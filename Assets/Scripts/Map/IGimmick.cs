using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGimmick
{

    public void InitGimmick();//초기 생성시
    public void ResetGimmick();//의도적 초기화 시
    public void PlayGimmick();//기믹 실행시
}
