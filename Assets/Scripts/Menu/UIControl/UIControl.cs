﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// 선택창 커서 조작 스크립트
/// PlayerSelectCursor프리팹에서 사용
/// </summary>
/// 
public class UIControl : MonoBehaviour
{
    [Header ("디버그용 인스펙터 출력")]
    [Space(2)]
    [SerializeField] Vector2 Moveval;
    [SerializeField] LobbyCursor Lobby_Cursor;
    //[SerializeField] UICursor UI_Cursor;
    [SerializeField] bool MoveFlag;
    [SerializeField] bool SelectFlag;
    [SerializeField] bool AllReadyFlag;
    //[SerializeField] bool GameStartFlag;
    [SerializeField] bool Disconnectflg;
    [SerializeField] public UnityEngine.Events.UnityAction PressLeft;
    [SerializeField] public UnityEngine.Events.UnityAction PressRight;
    [SerializeField] public UnityEngine.Events.UnityAction PressA;
    [SerializeField] public UnityEngine.Events.UnityAction PressB;
    [SerializeField] public UnityEngine.Events.UnityAction PressX;
    [SerializeField] public UnityEngine.Events.UnityAction PressY;
    public Vector2 MOVEVALUE
    {
        get { return Moveval; }
    }
    public bool MOVEFLAG
    {
        get { return MoveFlag; }
    }
    public bool SELECTFLAG
    {
        get { return SelectFlag; }
    }
    public bool ALLREADYFLAG
    {
        get { return AllReadyFlag; }
    }
    //public bool STARTFLAG
    //{
    //    get { return GameStartFlag; }
    //}
    public bool DISCONNECT
    {
        get { return Disconnectflg; }
    }
    //#region 프로퍼티
    //public int SelectIndex
    //{
    //    get
    //    {
    //        return PlayerSelectNum;
    //    }
    //    set
    //    {
    //        PlayerSelectNum = value;
    //    }
    //}
    //public GameObject CursorLinkedObject
    //{
    //    get
    //    {
    //        return PSCObj;
    //    }
    //    set
    //    {
    //        PSCObj = value;
    //    }
    //}//로비에서만 동작하는 부분
    //public SelectData SelectedData
    //{
    //    get
    //    {
    //        return Sd;
    //    }
    //    set
    //    {
    //        Sd = value;
    //    }
    //}
    //#endregion

    // Start is called before the first frame update
    //#region 유니티 기본 메세지 이벤트들
    //private void Awake()
    //{
    //    Backflg = false;
    //    inputmgr = GameObject.Find("InputMgr").GetComponent<LobbyManager>();
    //    Rtr = GetComponent<RectTransform>();

    //    APS = GameObject.Find("PlayerSelectObj").GetComponent<AddPlayerScript>();//로비씬에서만 사용
    //    TextureCams = GameObject.Find("TextureRenderCams");
    //}
    //private void OnEnable()
    //{
    //    SetSCDescription(PSCObj.GetComponent<UICursor>().sd);
    //}
    //void Start()
    //{
    //    GHit = transform.root.GetComponent<GraphicRaycaster>();
    //    Sd = PSCObj.GetComponent<UICursor>().sd;

    //    SetSCDescription(Sd);
    //    var togglesobj = transform.parent.parent.GetChild(0);
    //    toggles = new Toggle[togglesobj.childCount];
    //    for (int i = 0; i < togglesobj.childCount; i++)
    //    {
    //        toggles[i] = togglesobj.GetChild(i).GetComponent<Toggle>();
    //    }
    //    Limitx = (toggles.Length - 1) / 2 * 120f;
    //    var startposx = (((toggles.Length + 1) / 2) - 1) * 120;
    //    Rtr.anchoredPosition3D = new Vector3(-startposx, 120, 0);

    //    string str = "Ch_roundFlask";
    //    TextureCams.GetComponent<TextureRenderer>().InitTexture(Sd.GetIndex(), str);
    //    SelectedModelNum = GetStringToInt.GetModel(str);
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    if (Disconnectflg)
    //    {

    //        gameObject.SetActive(false);
    //        Disconnectflg = false;
    //    }
    //}

    //private void OnDisable()
    //{
    //    PSCObj.SetActive(false);
    //}
    //#endregion
    //public void SetSCDescription(SelectData sd)
    //{
    //    if (sd.GetDVName() != null)
    //    {
    //        transform.GetChild(0).GetComponent<Image>().material = sd.GetMat();
    //        transform.GetChild(0).GetChild(0).GetComponent<Text>().color = sd.GetMat().color;
    //        transform.GetChild(0).GetChild(0).GetComponent<Text>().text = PSCObj.transform.GetChild(2).GetChild(0).GetComponent<Text>().text;
    //    }
    //}
    private void Start()
    {
        if (GetComponent<LobbyCursor>())
        {
            Lobby_Cursor = GetComponent<LobbyCursor>();
        }
        //if (GetComponent<UICursor>())
        //{
        //    UI_Cursor = GetComponent<UICursor>();
        //}

    }
    public void Move(InputAction.CallbackContext ctx)
    {
        if (!SelectFlag)
        {
            if (ctx.phase == InputActionPhase.Started)
            {
                /*Moveval = ctx.ReadValue<Vector2>();
                Moveval = new Vector3(Mathf.RoundToInt(Moveval.x), Mathf.RoundToInt(Moveval.y), 0);
                MoveFlag = true;
                Debug.Log(Moveval);*/
                if(ctx.ReadValue<Vector2>().x < 0)
                {
                    if (PressLeft != null)
                    {
                        PressLeft.Invoke();
                    }
                }
                else if(ctx.ReadValue<Vector2>().x > 0)
                {
                    if(PressRight != null)
                    {
                        PressRight.Invoke();
                    }
                }
            }
        }

    }
    public void Select(InputAction.CallbackContext ctx)
    {
        if (PressA != null)
        {
            if (ctx.phase == InputActionPhase.Started)
            {
                PressA.Invoke();
            }
        }
    }

    public void Ready(InputAction.CallbackContext ctx)
    {
        
        if (PressX != null)
        {
            if (ctx.phase == InputActionPhase.Started)
            {
                PressX.Invoke();
            }
        }
    }

    public void Back(InputAction.CallbackContext ctx)
    {
        
        if (PressB != null)
        {
            if (PressB.Method.Name == "MainMenu_btnEventExit")
            {
                if (ctx.phase == InputActionPhase.Performed)
                {
                    PressB.Invoke();
                }
            }
            else
            {
                if (ctx.phase == InputActionPhase.Started)
                {
                    PressB.Invoke();
                }
                
            }
            
        }
    }
    public void PressYBtn(InputAction.CallbackContext ctx)
    {
        if (PressY != null)
        {
            if (ctx.phase == InputActionPhase.Started)
            {
                PressY.Invoke();
            }
        }
    }
    public void Menu(InputAction.CallbackContext ctx)
    {
        if (Lobby_Cursor != null)
        {
            if (Lobby_Cursor.sd.GetIndex() == 0)
            {
                if (Lobby_Cursor.LobbyMGR.ReadyAll())
                {
                    if (ctx.action.phase == InputActionPhase.Started)
                    {
                        Lobby_Cursor.LobbyMGR.GameStartflg = true;
                    }
                }
            }
        }
    }
    public void DeviceLostEvt(PlayerInput input)
    {
        Disconnectflg = true;

    }

    public void Change_Disconnectflg(bool flg = false)
    {
        Disconnectflg = flg;
    }
    public void Change_AllReadyFlag(bool flg)
    {
        AllReadyFlag = flg;
    }
    public void ResetSelectFlag()
    {
        SelectFlag = false;
    }
    public void Change_MoveFlag()
    {
        MoveFlag = false;
    }
    //public void Change_GameStartFlag(bool flg = false)
    //{
    //    GameStartFlag = flg;
    //}
    //RaycastResult GetRaycastResult(GraphicRaycaster _GHit, string str)
    //{
    //    RaycastResult RayResult = new RaycastResult();
    //    var ped = new PointerEventData(null);
    //    ped.position = transform.position;
    //    List<RaycastResult> results = new List<RaycastResult>();
    //    _GHit.Raycast(ped, results);
    //    if (results.Count <= 0) { return RayResult; }
    //    RayResult = results.Find(x => x.gameObject.name == str);
    //    return RayResult;
    //}

    //void ToggleCheck(Toggle[] _toggles) {
    //    for(int i = 0; i < _toggles.Length; i++)
    //    {
    //        if (_toggles[i].isOn)
    //        {
    //            SelectedModelNum = i;
    //            break;
    //        }
    //    }
    //}



    //public void GetLastSDATA(string str)
    //{

    //    inputmgr.SData[Sd.GetIndex()].SetName(str/*GetModelName(SelectedModelNum)*/);
    //}
    //public void SetReady()
    //{
    //    //var num = psObj.transform.GetChild(psObj.transform.childCount - 1).GetComponent<PlayerSelect>().GetNum();
    //    inputmgr.SData[Sd.GetIndex()].SetReady();
    //}
    //public void ResetSelect()
    //{

    //}



    /*public void DeviceReainedEvt(PlayerInput input)
    {

    }*/
}
