using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Set : MonoBehaviour
{
    UserInputManager UserManger;
    Custom.Data.ButtonScript ButtonScript;
    UIControl Control;
    // Start is called before the first frame update
    private void Awake()
    {
        if(GameObject.Find("UIController") != gameObject)
        {
            DestroyUIController();
        }

    }
    void Start()
    {
        SetButtonScript(GameManager.GM.BTNMGR);
        UserManger = GameManager.GM.USERMGR;
        Control = GetComponent<UIControl>();
        UserManger.SetUIControl(0, Control);
        SetButtonAction(KeyA: "Start", KeyB: "Exit");
        DontDestroyOnLoad(gameObject);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetButtonScript(Custom.Data.ButtonScript buttonScript)
    {
        ButtonScript = buttonScript;
    }
    public void SetButtonAction(string KeyLeft = "", string KeyRight = "", string KeyA = "", string KeyB = "", string KeyX = "", string KeyY = "")
    {
        
        Control.PressLeft = ButtonScript.GetFunctions(KeyLeft);
        Control.PressRight = ButtonScript.GetFunctions(KeyRight);
        Control.PressA = ButtonScript.GetFunctions(KeyA);
        Control.PressB = ButtonScript.GetFunctions(KeyB);
        Control.PressX = ButtonScript.GetFunctions(KeyX);
        Control.PressY = ButtonScript.GetFunctions(KeyY);
        GameManager.GM.SetGuideText(GetGuidestring(KeyLeft, KeyA, KeyB, KeyX, KeyY), 80);
    }
    string GetGuidestring(string l, string a, string b, string x, string y)
    {
        string Result = "  ";
        if (a != "")
        {
            Result += "A : " + a.ToUpper() ;
            Result += "     ";
        }
        if (b != "")
        {
            Result += "B : " + b.ToUpper();
            Result += "     ";
        }
        if (x != "")
        {
            Result += "X : " + x.ToUpper();
            Result += "     ";
        }
        if (y != "")
        {
            Result += "Y : " + y.ToUpper();
            Result += "     ";
        }
        if (l != "")
        {
            Result += "<-, -> : " + "MODEL CHANGE";
            Result += "     ";
        }
        return Result;
    }
    public void DestroyUIController()
    {
        DestroyImmediate(gameObject, true);
    }
    
}
