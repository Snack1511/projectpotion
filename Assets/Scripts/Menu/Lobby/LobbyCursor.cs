﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System.Collections;
using Custom.Data;

public class LobbyCursor : MonoBehaviour
{
    public SelectData sd;
    public UserInputManager UserMgr;
    [SerializeField] InputDevice InputDv;
    [SerializeField] LobbyManager LobbyMgr;
    GameObject SelectedImage;
    //[SerializeField] GameObject UISelector;
    int num;
    Text playerInputIdx;
    Text InputDevice;

    public SelectData SelectedData
    {
        get
        {
            return sd;
        }
        set
        {
            sd = value;
        }
    }
    public GameObject Selectedimg
    {
        get
        {
            return SelectedImage;
        }
    }
    public LobbyManager LobbyMGR
    {
        get { return LobbyMgr; }
    }

    private void Awake()
    {
        Debug.Log("UICursorAwake");
        InitCursor();
    }
    private void OnEnable()
    {
        if (sd.GetDVName() != "")
        {
            Debug.Log(sd.GetDVName());
            SetSelectedCursorDescription(sd, UserMgr, SelectedImage);
        }
    }
    void Start()
    {
        SetSelectedCursorDescription(sd, UserMgr, SelectedImage);
        //SetSelectedCursorDescription(sd);

    }


    

    // Update is called once per frame
    void Update()
    {
        /*if (Disconnectflg)
        {
            //transform.parent.GetComponent<AddPlayerScript>().RemovePlayer(sd.GetIndex());
            Disconnectflg = false;
        }*/


    }
    private void OnDisable()
    {
        if (transform.parent.GetComponent<AddPlayerScript>() != null)
        {
            transform.parent.GetComponent<AddPlayerScript>().RemovePlayer(sd.GetIndex());
        }
    }

    //public void SetCursor(SelectData sd)
    //{
    //    GetComponent<UICursor_Cal>().SelectedData = sd;
    //    SetSelectedCursorDescription(sd);
        
    //    //num = sd.GetIndex();
    //    //InputDv = sd.GetInputDv();
        
    //}//로비씬에서만 동작

    public void SetSelectedCursorDescription(SelectData selectdata, UserInputManager userInputmgr, GameObject SelectedImg)
    {
        if (selectdata.GetDVName() != "")
        {
            sd = selectdata;
            UserMgr = userInputmgr;
            SelectedImage = SelectedImg;
            transform.GetChild(0).GetComponent<Image>().material = sd.GetMat();
            transform.GetChild(0).GetChild(0).GetComponent<Text>().color = sd.GetMat().color;
            transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "P" + (sd.GetIndex() + 1).ToString();
            UserMgr.SetUIControl(sd.GetIndex(),GetComponent<UIControl>());
        }
    }
    public void InitCursor()
    {
        LobbyMgr = GameObject.Find("LobbyMgr").GetComponent<LobbyManager>();
        //UISelector.GetComponent<UICursor_Cal>().CursorLinkedObject = gameObject;
        GetComponent<LobbyCursor_Cal>().SelectIndex = sd.GetIndex();//이거 확인해야함
        
    }

    //커서
    //public string GetScheme(string device)
    //{
    //    if(device == "XInputControllerWindows")
    //    {
    //        return "Xbox";
    //    }
    //    else if(device == "Keyboard")
    //    {
    //        return "Keyboard";
    //    }
    //    else if (device == "DualShock4GamepadHID")
    //    {
    //        return "PS4";
    //    }
    //    else
    //    {
    //        return "";
    //    }
    //}

    //public void SetObjInputPair()
    //{
    //    UIInput.SwitchCurrentControlScheme(InputDv);
    //    //UIInput.user.
    //}


    /*public int GetNum()
    {
        return num;
    }*/

    

}
