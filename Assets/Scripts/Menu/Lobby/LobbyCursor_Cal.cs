﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Custom.Func;
using Custom.Data;

/// <summary>
/// 선택창 커서 조작 스크립트
/// PlayerSelectCursor프리팹에서 사용
/// </summary>
/// 
public class LobbyCursor_Cal : MonoBehaviour
{
    [Header("디버그용 인스펙터 출력")]
    [Space(2)]
    [SerializeField] UIControl Control;
    [SerializeField] LobbyCursor Cursor;
    //[SerializeField] bool Backflg;
    [SerializeField] int SelectedModelNum;
    [SerializeField] int PlayerSelectNum;
    [SerializeField] float Limitx;
    [Space]
    [SerializeField] SelectData sd;
    [Space]
    [SerializeField] GameObject TextureCams;
    //[SerializeField] GameObject PlayerSelectCursorObject;
    [SerializeField] Toggle[] toggles;
    [SerializeField] GraphicRaycaster GHit;
    [SerializeField] RectTransform Rtr;
    [Space]
    //[SerializeField] AddPlayerScript APS;
    [SerializeField] LobbyManager LobbyMgr;
    [SerializeField] Transform ToggleObjTr;
    [SerializeField] int StartPosx;
    [SerializeField] string SelectedModelName;
    [SerializeField] PointerEventData RayCastTargetData;
    [SerializeField] RaycastResult RayResult;
    [SerializeField] bool ToggleOnFlag;
    #region 프로퍼티
    public int SelectIndex
    {
        get
        {
            return PlayerSelectNum;
        }
        set
        {
            PlayerSelectNum = value;
        }
    }
    //public GameObject CursorLinkedObject
    //{
    //    get
    //    {
    //        return PlayerSelectCursorObject;
    //    }
    //    set
    //    {
    //        PlayerSelectCursorObject = value;
    //    }
    //}//로비에서만 동작하는 부분
    public SelectData SelectedData
    {
        get
        {
            return sd;
        }
        set
        {
            sd = value;
        }
    }
    #endregion

    // Start is called before the first frame update
    #region 유니티 기본 메세지 이벤트들
    private void Awake()
    {
        //Backflg = false;
        Control = GetComponent<UIControl>();
        Cursor = GetComponent<LobbyCursor>();
        Rtr = GetComponent<RectTransform>();

        LobbyMgr = Cursor.LobbyMGR;
        //APS = GameObject.Find("PlayerSelectObj").GetComponent<AddPlayerScript>();//로비씬에서만 사용
        TextureCams = GameObject.Find("TextureRenderCams");
    }
    //private void OnEnable()
    //{
    //    SetSelectedCursorDescription(PlayerSelectCursorObject.GetComponent<UICursor>().sd);
    //}
    void Start()
    {
        GHit = transform.root.GetComponent<GraphicRaycaster>();
        sd = GetComponent<LobbyCursor>().sd;

        //SetSelectedCursorDescription(Sd);
        ToggleObjTr = transform.parent.parent.GetChild(0);
        toggles = new Toggle[ToggleObjTr.childCount];
        for (int i = 0; i < ToggleObjTr.childCount; i++)
        {
            toggles[i] = ToggleObjTr.GetChild(i).GetComponent<Toggle>();
        }
        Limitx = (toggles.Length - 1) / 2 * 120f;
        StartPosx = (((toggles.Length + 1) / 2) - 1) * 120;
        Rtr.anchoredPosition3D = new Vector3(-StartPosx, 120, 0);

        SelectedModelName = "Ch_roundFlask";
        TextureCams.GetComponent<TextureRenderer>().InitTexture(sd.GetIndex(), SelectedModelName);
        SelectedModelNum = GetStringToInt.GetModel(SelectedModelName);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Select();
        if (Control.DISCONNECT)
        {
            gameObject.SetActive(false);
            Control.Change_Disconnectflg();
        }
    }

    //private void OnDisable()
    //{
    //    PlayerSelectCursorObject.SetActive(false);
    //}
    #endregion
    
    public void Move()
    {//
        if (Control.MOVEFLAG)
        {
            Debug.Log("MoveStart");
            if (!LobbyMgr.SData[sd.GetIndex()].IsReady())
            {
                Rtr.anchoredPosition3D += (Vector3)(Control.MOVEVALUE * 240);
                if (Rtr.anchoredPosition3D.x <= -Limitx) Rtr.anchoredPosition3D = new Vector3(-Limitx, Rtr.anchoredPosition3D.y, Rtr.anchoredPosition3D.z);
                else if (Rtr.anchoredPosition3D.x >= Limitx) Rtr.anchoredPosition3D = new Vector3(Limitx, Rtr.anchoredPosition3D.y, Rtr.anchoredPosition3D.z);
                if (Rtr.anchoredPosition3D.y >= 120) Rtr.anchoredPosition3D = new Vector3(Rtr.anchoredPosition3D.x, 120, Rtr.anchoredPosition3D.z);
                else if (Rtr.anchoredPosition3D.y <= -120) Rtr.anchoredPosition3D = new Vector3(Rtr.anchoredPosition3D.x, -120, Rtr.anchoredPosition3D.z);

                SelectedModelName = GetRaycastResult(GHit, "Background").gameObject.transform.parent.name;
                TextureCams.GetComponent<TextureRenderer>().SetTextureObj(sd.GetIndex(), SelectedModelNum, SelectedModelName);
                SelectedModelNum = GetStringToInt.GetModel(SelectedModelName);
            }//로직 다시 짜야함
            Control.Change_MoveFlag();
        }
    }
    public void Select()
    {
        if (Control.SELECTFLAG)
        {
            if (!LobbyMgr.SData[sd.GetIndex()].IsReady())
            {
                if (!ToggleOnFlag)
                {
                    //SelectedModelName = GetRaycastResult(GHit, "Background").gameObject.transform.parent.name;
                    GetLastSDATA(SelectedModelName);
                    GetRaycastResult(GHit, "Background").gameObject.transform.parent.GetComponent<Toggle>().isOn = true;
                    ToggleOnFlag = true;

                }
                if (LobbyMgr.SData[sd.GetIndex()].GetName() != "")
                {
                    Cursor.Selectedimg.transform.GetChild(2).GetChild(2).gameObject.SetActive(true);
                }
                SetReady();
            }
            else
            {
                LobbyMgr.SData[sd.GetIndex()].SetReady(false);
                Cursor.Selectedimg.transform.GetChild(2).GetChild(2).gameObject.SetActive(false);
                GetRaycastResult(GHit, "Background").gameObject.transform.parent.GetComponent<Toggle>().isOn = false;
                ToggleOnFlag = false;
            }
            Control.ResetSelectFlag();
        }
    }
    RaycastResult GetRaycastResult(GraphicRaycaster _GHit, string str)
    {
        RayResult = new RaycastResult();
        RayCastTargetData = new PointerEventData(null);
        RayCastTargetData.position = transform.position;
        List<RaycastResult> results = new List<RaycastResult>();
        _GHit.Raycast(RayCastTargetData, results);
        if (results.Count <= 0) { return RayResult; }
        RayResult = results.Find(x => x.gameObject.name == str);
        return RayResult;
    }

    //void ToggleCheck(Toggle[] _toggles)
    //{
    //    for (int i = 0; i < _toggles.Length; i++)
    //    {
    //        if (_toggles[i].isOn)
    //        {
    //            SelectedModelNum = i;
    //            break;
    //        }
    //    }
    //}



    public void GetLastSDATA(string str)
    {

        LobbyMgr.SData[sd.GetIndex()].SetName(str/*GetModelName(SelectedModelNum)*/);
    }
    public void SetReady()
    {
        //var num = psObj.transform.GetChild(psObj.transform.childCount - 1).GetComponent<PlayerSelect>().GetNum();
        LobbyMgr.SData[sd.GetIndex()].SetReady();
    }
    
    public void ResetSelect()
    {

    }


    /*public void DeviceReainedEvt(PlayerInput input)
    {

    }*/
}
