using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using System.Collections;
using Custom.Data;

public class SelectImage : MonoBehaviour
{
    public SelectData sd;

    [SerializeField] InputDevice InputDv;
    [SerializeField] GameObject UISelectorPrefab;//커서 프리팹
    //[SerializeField] Transform CursorContainer;
    GameObject CursorObj;
    int num;
    Text playerInputIdx;
    Text InputDevice;
    //GameObject UIInput;

    private void Awake()
    {
        InitSelectImage();
    }
    private void OnEnable()
    {

        if (UISelectorPrefab.activeSelf)
        {
            UISelectorPrefab.SetActive(true);
            //SetUIInput();
        }
    }
    void Start()
    {
        
    }




    // Update is called once per frame
    void Update()
    {
        /*if (Disconnectflg)
        {
            //transform.parent.GetComponent<AddPlayerScript>().RemovePlayer(sd.GetIndex());
            Disconnectflg = false;
        }*/


    }
    private void OnDisable()
    {
        if (transform.parent.GetComponent<AddPlayerScript>() != null)
        {
            transform.parent.GetComponent<AddPlayerScript>().RemovePlayer(sd.GetIndex());
        }
        /*if (UIInput.gameObject.activeSelf)
        {
            UIInput.gameObject.SetActive(false);
        }*/
    }
    public void SetObjectDescription(SelectData _sd)
    {
        
        gameObject.name = "PlayerSelect" + _sd.GetIndex();
        gameObject.transform.GetChild(1).GetComponent<Image>().material = _sd.GetMat();
        //Debug.Log(gameObject.name);
        playerInputIdx = transform.GetChild(2).GetChild(0).GetComponent<Text>();
        InputDevice = transform.GetChild(2).GetChild(1).GetComponent<Text>();
        var num = _sd.GetIndex();
        num += 1;
        transform.GetChild(2).GetComponent<RawImage>().texture = Resources.Load<CustomRenderTexture>("Materials/Texture/Player" + num);
        playerInputIdx.text = "P" + num;//+1;
        InputDevice.text = _sd.GetDVName();
        SetSelectedData(_sd);
        //CursorObj.GetComponent<UICursor>().SetSelectedCursorDescription(_sd);
        //UIInput.GetComponent<UICursor_Cal>().SetSelectedCursorDescription(sd);

        //num = sd.GetIndex();
        //InputDv = sd.GetInputDv();

    }//로비씬에서만 동작

    public void InitSelectImage()
    {
        //CursorObj = Instantiate(UISelectorPrefab, CursorContainer);
        //Debug.Log("SelectImg.sd : " + sd.GetDVName());
        //UISelector.GetComponent<UICursor_Cal>().CursorLinkedObject = gameObject;
        //UISelector.GetComponent<UICursor_Cal>().SelectIndex = sd.GetIndex();//이거 확인해야함
        //UIInput = PlayerInput.Instantiate(UISelector, sd.GetIndex(), GetScheme(sd.GetDVName()), -1, sd.GetInputDv());//로비매니저에서 실행해야함
        //UISelector.transform.SetParent(GameObject.Find("ModelPanel").transform.GetChild(1));

    }
    public void SetSelectedData(SelectData _sd)
    {
        sd = _sd;
    }
    //public void SetCursorContainer(Transform _CursorContainer)
    //{
    //    if (_CursorContainer != null)
    //    {
    //        CursorContainer = _CursorContainer;
    //    }
    //}

    //public void SetUIInput()
    //{
    //    UIInput.SwitchCurrentActionMap("UISelector");
    //    UIInput.SwitchCurrentControlScheme(sd.GetInputDv());
    //    UISelector.name = .currentControlScheme + " " + UIInput.currentActionMap.name;
    //}
    //커서
    //public string GetScheme(string device)
    //{
    //    if (device == "XInputControllerWindows")
    //    {
    //        return "Xbox";
    //    }
    //    else if (device == "Keyboard")
    //    {
    //        return "Keyboard";
    //    }
    //    else if (device == "DualShock4GamepadHID")
    //    {
    //        return "PS4";
    //    }
    //    else
    //    {
    //        return "";
    //    }
    //}

    //public void SetObjInputPair()
    //{
    //    UIInput.SwitchCurrentControlScheme(InputDv);
    //    //UIInput.user.
    //}


    /*public int GetNum()
    {
        return num;
    }*/



}
