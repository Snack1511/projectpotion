﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using Custom.Data;
public class AddPlayerScript : MonoBehaviour
{
    [Header(" - Public")]
    [SerializeField] List<GameObject> SelectedImages;
    [SerializeField] List<GameObject> SelectedCursor;
    public GameObject PlayerSelectPrefab;
    public GameObject PlayerCursorPrefab;
    [Space(2)]
    [Header(" - Private")]
    [SerializeField] LobbyManager LobbyMgr;
    [SerializeField] Transform CursorContainer;
    [Space(2)]
    [Header("디버그용 인스펙터 출력")]
    [SerializeField] float padding = 30;
    [SerializeField] float sizex = 300;

    RectTransform RTr;
    // Start is called before the first frame update
    void Awake()
    {
        SelectedImages = new List<GameObject>();
        SelectedCursor = new List<GameObject>();
        RTr = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    

    public void AddPlayer(SelectData _sd, float size, int n = 0)
    {
        if (SelectedImages.Count > 0)
        {
            Vector3 LastPos;
            for (int i = 0; i < SelectedImages.Count; i++)
            {
                SelectedImages[i].GetComponent<RectTransform>().position += new Vector3(-(size*0.5f + padding), 0, 0);
            }
            LastPos = SelectedImages[SelectedImages.Count - 1].GetComponent<RectTransform>().position;
            if (n + 1 > SelectedImages.Count)
            {
                if (transform.childCount <= SelectedImages.Count)
                {
                    SelectedImages.Add(Instantiate(PlayerSelectPrefab, transform));
                    SelectedCursor.Add(Instantiate(PlayerCursorPrefab, CursorContainer));
                }
                else
                {
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        if (!transform.GetChild(i).gameObject.activeSelf)
                        {
                            //Debug.Log("bb " + i);
                            transform.GetChild(i).gameObject.SetActive(true);
                            SelectedImages.Add(transform.GetChild(i).gameObject);
                            break;

                        }
                    }
                }
            }
            SelectedImages[SelectedImages.Count - 1].GetComponent<RectTransform>().position = LastPos + new Vector3((size + padding*2), 0, 0);

        }
        else
        {
            SelectedImages.Add(Instantiate(PlayerSelectPrefab, transform));
            SelectedCursor.Add(Instantiate(PlayerCursorPrefab, CursorContainer));
        }
        //SelectedImages[SelectedImages.Count - 1].GetComponent<SelectImage>().sd = _sd;
        SelectedImages[SelectedImages.Count - 1].GetComponent<SelectImage>().SetObjectDescription(_sd);
        SelectedCursor[SelectedCursor.Count - 1].GetComponent<LobbyCursor>().SetSelectedCursorDescription(_sd, GameManager.GM.USERMGR, SelectedImages[SelectedImages.Count - 1]);
    }


    public void RemovePlayer(int n, float size = 420)
    {
        for (int i = 0; i < SelectedImages.Count; i++)
        {
            if (i <= n)
            {
                SelectedImages[i].GetComponent<RectTransform>().anchoredPosition3D += new Vector3(size*0.5f+padding, 0, 0);
            }
            else 
            {
                SelectedImages[i].GetComponent<RectTransform>().anchoredPosition3D += new Vector3(-(size * 0.5f + padding), 0, 0);
            }
            

        }
        Debug.Log(n);
        SelectedImages.RemoveAt(n);
        if (LobbyMgr != null)
        {
            UpdateplayerImg(n);
        }

    }

    public void UpdateplayerImg(int n)
    {
        
        LobbyMgr.RemoveData(n);
        int idx = 0;
        for (int i = 0; i < SelectedImages.Count; i++)
        {
            if (SelectedImages[i].activeSelf)
            {
                Debug.Log(SelectedImages[i] + " " + LobbyMgr.SData[idx]);
                var PlayerImgScript = SelectedImages[i].GetComponent<SelectImage>();
                PlayerImgScript.sd = LobbyMgr.SData[idx];
                PlayerImgScript.SetObjectDescription(LobbyMgr.SData[idx]);
                idx++;
            }
        }
    }

}
