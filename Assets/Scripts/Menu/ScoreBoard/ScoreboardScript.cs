using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class ScoreboardScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite[] sprites = new Sprite[3];
    public float Height = 100;
    public float padding = 30;
    public float PixelMultiple = 2.5f;
    public bool bPlayEnd;
    Vector3 HeightMoveValue;
    PauseManager PauseMgr;
    private void Awake()
    {
        
    }
    public void InitScoreboard(PauseManager Pause)
    {
        bPlayEnd = false;
        HeightMoveValue = new Vector3(0, Height + padding, 0);
        PauseMgr = Pause;
        for (int i = 1; i <= Pause.ROUNDMGR.PMGR.PlayerCount; i++)
        {
            InitScoreMeter(i);
        }//��Ŵ��������� �޾������
    }
    private void OnEnable()
    {
        
        if (PauseMgr != null && PauseMgr.ROUNDMGR.ReadRoundCheck)
        {
            for (int i = 1; i <= PauseMgr.ROUNDMGR.PMGR.PlayerCount; i++)
            {
                transform.GetChild(i).GetComponent<RectTransform>().GetChild(1).GetComponent<MedalAdd>().SetData(
                    PauseMgr.ROUNDMGR.RoundNum,
                    /*PauseMgr.ROUNDMGR.PMGR.ReadPlyerDatas()[i - 1].GetPlayerData().DEGREE,
                    PauseMgr.ROUNDMGR.PMGR.ReadPlyerDatas()[i - 1].GetPlayerData().POINT*/
                    //PauseMgr.ROUNDMGR.PMGR.ReadPlyerDataList()[i - 1].GetPlayerData().DEGREE,
                    PauseMgr.ROUNDMGR.PMGR.ReadPlyerDataList()[i - 1].GetPlayerData().POINT
                    );
                //transform.GetChild(i).GetComponent<RectTransform>().GetChild(1).GetComponent<MedalAdd>().idxmax = PauseMgr.ROUNDMGR.PMGR.ReadPlyerDatas()[i - 1].GetPlayerData().POINT;
                transform.GetChild(i).GetComponent<RectTransform>().GetChild(1).GetComponent<MedalAdd>().idxmax = PauseMgr.ROUNDMGR.PMGR.ReadPlyerDataList()[i - 1].GetPlayerData().POINT;
            }
        }
    }
    void Start()
    {
        
        //InitScoreMeter();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDisable()
    {
        bPlayEnd = false;
    }
    void InitScoreMeter(int ChildNum = 1)
    {
        var objrtr = transform.GetChild(ChildNum).GetComponent<RectTransform>();
        objrtr.anchoredPosition3D += (HeightMoveValue * 0.5f);
        objrtr.anchoredPosition3D -= (HeightMoveValue) * ChildNum;
        objrtr.gameObject.SetActive(true);
    }


    

    
    
}
