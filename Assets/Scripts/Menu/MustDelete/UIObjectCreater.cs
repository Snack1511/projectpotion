using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Data.UIData;
using UnityEngine.UI;
public class UIObjectCreater : MonoBehaviour
{
    [SerializeField] Canvas MainCanvas;
    [SerializeField] string ObjectName;
    [SerializeField] string GroupName;
    [SerializeField] GameObject BaseGameObject;
    [SerializeField] Font BaseFont;
    [SerializeField] Sprite DebugSprite;
    [SerializeField] onClickScript buttonScript;
    // Start is called before the first frame update
    void Start()
    {
        InitUI();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void InitUI()
    {
        CreateGroup(GroupName, UIManager.instance.UIDRAWDATA[GroupName], UIManager.instance.UIRECTDATA[GroupName]);
        //CreateButton(ObjectName, UIManager.instance.UIDRAWDATA[ObjectName], UIManager.instance.UIRECTDATA[ObjectName]);
    }
    void CreateGroup(string name, UIDrawData DrawData, UIRectData RectData, string Groupingbase = "", int index = 0)
    {
        float ratiovalue = UIManager.instance.GetRatioValue(index);
        Transform ParentsTr;
        Vector2 anchorset = Vector2.zero;
        if (Groupingbase != "")
        {
            ParentsTr = GameObject.Find(Groupingbase).transform;
            anchorset = Vector2.one * 0.5f;
        }
        else
        {
            ParentsTr = MainCanvas.transform;
            anchorset = Vector2.zero;
        }
        var obj = Instantiate<GameObject>(BaseGameObject, ParentsTr);
        obj.name = name;
        

        var rectTransform = obj.GetComponent<RectTransform>();
        rectTransform.anchorMin = anchorset;
        rectTransform.anchorMax = anchorset;
        rectTransform.sizeDelta = RectData.GetSize(index) * ratiovalue;
        rectTransform.anchoredPosition3D = RectData.GetPosition(index) * ratiovalue;


        

        string[] labels = DrawData.LABEL.Split('/');

        if (DrawData.GROUPINGNAME != "") {
            obj.AddComponent<VerticalLayoutGroup>();
            var verticalSetting = obj.GetComponent<VerticalLayoutGroup>();
            verticalSetting.childAlignment = TextAnchor.MiddleCenter;
            verticalSetting.childControlHeight = false;
            verticalSetting.childControlWidth = false;
            verticalSetting.childForceExpandHeight = false;
            verticalSetting.childForceExpandWidth = false;

            var UIRectdata = UIManager.instance.UIRECTDATA[DrawData.GROUPINGNAME];
            var UiDrawdata = UIManager.instance.UIDRAWDATA[DrawData.GROUPINGNAME];
            verticalSetting.spacing = UIRectdata.SPACE[index] * ratiovalue;

            if (labels[0] != "")
            {

                for (int i = 0; i < DrawData.CHILDCOUNT; i++)
                {
                    CreateButton(DrawData.GROUPINGNAME, obj, UiDrawdata, UIRectdata, labels[i].TrimEnd('\r'), index, ratiovalue);
                }
            }
            else
            {
                for (int i = 0; i < DrawData.CHILDCOUNT; i++)
                {
                    CreateGroup(DrawData.GROUPINGNAME, UiDrawdata, UIRectdata, name, index);
                }
            }
        }
    }

    void CreateButton(string name, GameObject parent, UIDrawData Data, UIRectData RectData, string Label, int index, float Ratiovalue)
    {
        Transform ParentsTr = parent.transform;
        Vector2 anchorset = Vector2.one * 0.5f;//따로 빼낼수 있음
         
        var obj = Instantiate<GameObject>(BaseGameObject, ParentsTr);
        obj.name = name;
        obj.AddComponent<Image>();
        obj.AddComponent<Button>();

        var rectTransform = obj.GetComponent<RectTransform>();
        rectTransform.anchorMin = anchorset;
        rectTransform.anchorMax = anchorset;
        rectTransform.sizeDelta = RectData.GetSize(index) * Ratiovalue;
        rectTransform.anchoredPosition3D = RectData.GetPosition(index) * Ratiovalue;

        var ImageSetting = obj.GetComponent<Image>();
        if (Data.IMAGE != "")
        {

        }
        else
        {
            ImageSetting.sprite = DebugSprite;
            ImageSetting.type = Image.Type.Sliced;
            var str = Instantiate<GameObject>(BaseGameObject);
            str.transform.SetParent(obj.transform);
            var strRectTr = str.GetComponent<RectTransform>();
            strRectTr.anchorMin = Vector2.zero;
            strRectTr.anchorMax = Vector2.one;
            strRectTr.offsetMin = Vector2.zero;
            strRectTr.offsetMax = Vector2.zero;

            str.AddComponent<Text>().text = Label;
            var strText = str.GetComponent<Text>();
            strText.font = BaseFont;
            strText.color = Color.black;
            strText.alignment = TextAnchor.MiddleCenter;
            strText.resizeTextForBestFit = true;
        }
        Debug.Log("Label : " + Label);
        var ButtonSetting = obj.GetComponent<Button>();
        ButtonSetting.onClick.AddListener(buttonScript.GetFunctions(Label));

    }
    void DebugFunc()
    {

    }
}
