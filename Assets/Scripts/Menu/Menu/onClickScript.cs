﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class onClickScript : MonoBehaviour, Custom.Data.ButtonScript
{
    public static onClickScript instance;
    public Dictionary<string, List<UnityAction>> Functions = new Dictionary<string, List<UnityAction>>();
    [SerializeField] GameObject MainGroup, MultiGroup, OnlineGroup, OnlineSetting, Inputfield, BackButton;
    [SerializeField] UI_Set UISet;
    GameManager GameMgr;
    private void Awake()
    {
        instance = this;
        MainGroup.SetActive(true);
        MultiGroup.SetActive(false);
        OnlineGroup.SetActive(false);
        OnlineSetting.SetActive(false);
        BackButton.SetActive(false);
        


    }
    private void Start()
    {
        //UISet.SetButtonAction(KeyA: "Start", KeyB: "Exit");
        GameMgr = GameManager.GM;
        if(UISet == null)
        {
            UISet = GameMgr.USERMGR.UISet;
            UISet.SetButtonScript(this);
            UISet.SetButtonAction(KeyA: "Start", KeyB: "Exit");
        }
    }
    public void MainMenu_btnEventStart() {
        Debug.Log("Start");
        SoundManager.instance.PlayFXSound("Click");
        GameManager.GM.CreateSceneMgr();
        BackButton.SetActive(true);
        MainGroup.SetActive(false);
        MultiGroup.SetActive(true);
        OnlineGroup.SetActive(false);
        UISet.SetButtonAction(KeyB: "Back", KeyX:"Local", KeyY:"Online");
    }
    public void MainMenu_EventStartToBack()
    {
        Debug.Log("MainMenu_EventStartToBack");
        SoundManager.instance.PlayFXSound("Click");
        if (MultiGroup.activeSelf)
        {
            Debug.Log("MultiGroup.activeSelf");
            MultiGroup.SetActive(false);
            UISet.SetButtonAction(KeyA: "Start", KeyB:"Exit");
            MainGroup.SetActive(true);
            BackButton.SetActive(false);
        }
        else if (OnlineGroup.activeSelf)
        {
            Debug.Log("OnlineGroup.activeSelf");
            OnlineGroup.SetActive(false);
            UISet.SetButtonAction(KeyB: "Back", KeyX:"Local", KeyY:"Online");
            MultiGroup.SetActive(true);
        }
        else if (OnlineSetting.activeSelf)
        {
            Debug.Log("OnlineSetting.activeSelf");
            OnlineSetting.SetActive(false);
            UISet.SetButtonAction(KeyB: "Back", KeyX:"Host", KeyY:"Guest");
            OnlineGroup.SetActive(true);
        }
        else
        {
            Debug.Log("MainButton");
        }
    }
    public void MainMenu_btnEventExit() {
#if UNITY_EDITOR
        Debug.Log("Exit");
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }


    public void MultiSelect_btnEventLocal() {
        Debug.Log("Local is not ready");
        //GameManager.GM.SCENEMGR.SetLocal();
        
    }
    public void MultiSelect_btnEventOnline() {
        
        Debug.Log("Online");
        SoundManager.instance.PlayFXSound("Click");
        GameManager.GM.SCENEMGR.SetOnline();
        
        BackButton.SetActive(true);
        MultiGroup.SetActive(false);
        OnlineGroup.SetActive(true);
        UISet.SetButtonAction(KeyB: "Back", KeyX: "Host", KeyY: "Guest");
    }


    public void OnlineSelect_btnEventHost()
    {
        Debug.Log("Host");
        SoundManager.instance.PlayFXSound("Click");
        string RoomCode = "";
        RoomCode = Custom.Func.GenericFuncs.NumGeneric();
        GameManager.GM.SCENEMGR.SetHost(RoomCode, GameManager.GM.Version);
    }
    public void OnlineSelect_btnEventGuest()
    {
        Debug.Log("Guest");
        SoundManager.instance.PlayFXSound("Click");
        BackButton.SetActive(true);
        OnlineGroup.SetActive(false);
        OnlineSetting.SetActive(true);
        UISet.SetButtonAction(KeyA: "Submit", KeyB: "Back", KeyX: "Code");
    }


    public void SubmitEnterCode()
    {
        
        Debug.Log("Submit");
        SoundManager.instance.PlayFXSound("Click");
        GameManager.GM.SCENEMGR.SetGuest(Inputfield.GetComponent<UnityEngine.UI.InputField>().text, GameManager.GM.Version);
    }
    public void SelectCode()
    {
        Debug.Log("SelectCode");
        SoundManager.instance.PlayFXSound("Click");
        Inputfield.GetComponent<UnityEngine.UI.InputField>().Select();

    }
    public UnityAction GetFunctions(string key)
    {
        UnityAction action = null;
        //Debug.Log(action.Method.Name);
        switch (key)
        {
            case "Start":
                action = MainMenu_btnEventStart;
                break;
            case "Exit":
                action = MainMenu_btnEventExit;
                break;
            case "Local":
                action = MultiSelect_btnEventLocal;
                break;
            case "Online":
                action = MultiSelect_btnEventOnline;
                break;
            case "Host":
                action = OnlineSelect_btnEventHost;
                break;
            case "Guest":
                action = OnlineSelect_btnEventGuest;
                break;
            case "Code":
                action = SelectCode;
                break;
            case "Submit":
                action = SubmitEnterCode;
                break;
            case "Back":
                action = MainMenu_EventStartToBack;
                break;
            default:
                action = ReplaceNull;
                break;
        }
        return action;
    }
    public void DebugGenerate()
    {
        string str;
        str = Custom.Func.GenericFuncs.NumGeneric();
        GameObject.Find("GeneratorDebug").GetComponent<UnityEngine.UI.Text>().text = str;
    }
    void ReplaceNull()
    {
        Debug.Log("Empty Action");
    }
    public void SetUIControl(UI_Set uiset)
    {
        UISet = uiset;
    }
}
