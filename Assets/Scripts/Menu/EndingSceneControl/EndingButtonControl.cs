using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EndingButtonControl : MonoBehaviour, Custom.Data.ButtonScript
{
    [SerializeField] GameObject PadGuide;
    [SerializeField] Text CountDown;
    private void Awake()
    {
        GameManager.GM.USERMGR.SetUIControl(0, GameObject.Find("UIController").GetComponent<UIControl>());
        GameManager.GM.ActivePadGuide(PadGuide);
        GameManager.GM.USERMGR.UISet.SetButtonScript(GetComponent<EndingButtonControl>());
        GameManager.GM.USERMGR.UISet.SetButtonAction(KeyA: "Skip");
        StartCoroutine("SkipCount");
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public UnityEngine.Events.UnityAction GetFunctions(string key)
    {
        UnityEngine.Events.UnityAction action = null;
        //Debug.Log(action.Method.Name);
        switch (key)
        {
            case "Skip":
                action = ClickSkipBtn;
                break;
            default:
                action = ReplaceNull;
                break;
        }
        return action;
    }

    public void ClickSkipBtn()
    {
        GameManager.GM.SCENEMGR.SetMain();
    }
    void ReplaceNull()
    {
        Debug.Log("Empty Action");
    }
    IEnumerator SkipCount()
    {
        int time = 0;
        while(time < 5)
        {
            CountDown.text = "CountDown : " + (5 - time).ToString();
            time++;
            yield return new WaitForSecondsRealtime(1);
        }
        ClickSkipBtn();
    }
}
