using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinnerGenerator : MonoBehaviour
{
    [SerializeField]
    GameObject WinnerPos;
    GameObject WinnerObj;

    Material Mat_Liquid;
    (int, string) WinnerData;
    private void Awake()
    {
        WinnerData = GameObject.Find("EndingControlObj").GetComponent<EndingControl>().WinnerData;
        WinnerObj = Instantiate(Resources.Load<GameObject>("Objects/" + WinnerData.Item2), WinnerPos.transform);
        WinnerObj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material =
            new Material(Resources.Load<Material>("Materials/Player_Mat"));//Body->Flask->Bottle
        //불러온 파일의 유리부분에 유리 마테리얼 할당
        WinnerObj.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().material =
            new Material(Resources.Load<Material>("Materials/Cap_Mat"));

        Mat_Liquid = new Material(Resources.Load<Material>("Materials/Liquid_Mat"));
        //액체 로드 후 마테리얼 할당
        Mat_Liquid.SetFloat("_FillAmount", 0.2f);
        Color[] Colors = new Color[3];
        Colors = Custom.Func.ColorFunction.SetColor(WinnerData.Item1);

        Mat_Liquid.SetColor("_Tint", Colors[2]);

        WinnerObj.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<MeshRenderer>().material = Mat_Liquid;
        var FootMat = new Material(Resources.Load<Material>("Materials/PlayerMat_Colored"));
        FootMat.SetColor("_BaseColor", Colors[1]);
        FootMat.SetColor("_ColorDim", Colors[2]);
        var HandMat = new Material(Resources.Load<Material>("Materials/PlayerMat_Colored"));
        HandMat.SetColor("_BaseColor", Colors[0]);
        HandMat.SetColor("_ColorDim", Colors[2]);
        WinnerObj.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = HandMat;
        WinnerObj.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = FootMat;
        WinnerObj.transform.GetChild(3).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = HandMat;
        WinnerObj.transform.GetChild(4).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = FootMat;
        WinnerObj.transform.Rotate(new Vector3(0, -1, 0), 90);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
