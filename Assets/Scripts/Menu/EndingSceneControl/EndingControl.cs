using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndingControl : MonoBehaviour
{
    string strModelName;
    int iWinnerNum;
    PlayersManager PlayerMgr;
    RoundManager RoundMgr;
    PauseManager PauseMgr;
    RoomManager RoomMgr;
    PhotonSyncScript PhotonMgr;
    MapManager MapMgr;


    public (int, string)WinnerData{
        get
        {
            return (iWinnerNum, strModelName);
        }
    }
    // Start is called before the first frame update
    private void Awake()
    {
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetWinnerData((int, string)WinnerData)
    {
        iWinnerNum = WinnerData.Item1;
        strModelName = WinnerData.Item2;
    }
    public void SetManagers(PlayersManager pmgr, RoundManager rmgr, PauseManager pumgr, RoomManager RoMgr, MapManager MMgr)
    {
        PlayerMgr = pmgr;
        RoundMgr = rmgr;
        PauseMgr = pumgr;
        RoomMgr = RoMgr;
        MapMgr = MMgr;
    }
    public void SetManagers(PlayersManager pmgr, RoundManager rmgr, PauseManager pumgr, RoomManager RoMgr, PhotonSyncScript PhoMgr, MapManager MMgr)
    {
        PlayerMgr = pmgr;
        RoundMgr = rmgr;
        PauseMgr = pumgr;
        RoomMgr = RoMgr;
        PhotonMgr = PhoMgr;
        MapMgr = MMgr;
    }
    public void DestroyManagers()
    {
        GameManager.GM.SetDestroyManagers(this);
        PhotonMgr.DisconnectPhoton();
        Destroy(PauseMgr.gameObject);
        Destroy(PlayerMgr.gameObject);
        Destroy(RoundMgr.gameObject);
        Destroy(RoomMgr.gameObject);
        //Destroy(PhotonMgr.gameObject);
        Destroy(MapMgr.gameObject);
    }
}
