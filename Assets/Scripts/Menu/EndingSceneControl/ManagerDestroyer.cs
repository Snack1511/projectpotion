using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerDestroyer : MonoBehaviour
{
    private void Awake()
    {
        GameObject.Find("EndingControlObj").GetComponent<EndingControl>().DestroyManagers();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
