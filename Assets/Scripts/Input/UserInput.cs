﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using Custom.Data;

/// <summary>
/// 유저 인풋으로 모든 씬에서 조작 확인
/// 각 씬의 상태에 따라 인풋시스템의 액션맵 변경
/// 각 씬에서 조작되는 오브젝트들은 각자의 컨트롤 스크립트가 필요
/// 상태조건 -> UI, 플레이
/// </summary>
public class UserInput : MonoBehaviour
{
    UnityEvent MenuUIInput;
    UnityEvent PauseMenuInput;
    UnityEvent PlayInput;

    UIControl UIControlScript;
    PlayerControl PlayerControlScript;
    UserInputManager UserInputMgr;
    PlayerInput P_Input;
    private bool bSubmitflg;
    // Start is called before the first frame update
    //public float ThrowPower { private set; get; }
    public bool SubmitFlag_Debug
    {
        get { return bSubmitflg; }
    }
    
    private void Awake()
    {
        

    }
    private void OnEnable()
    {

    }
    void Start()
    {
        InitInput();
    }

    
    private void OnDisable()
    {
    }

    //=====UserCustomMathod
    void InitInput()
    {
        UserInputMgr = transform.parent.GetComponent<UserInputManager>();
        UserInputMgr.ActionmapChangeEvent.AddListener(ChangeActionMap);
        P_Input = GetComponent<PlayerInput>();
        //if (Gamepad.all.Count < 1)
        //{
        //    device = Keyboard.current;
        //}
        //else
        //{
        //    device = Gamepad.all[0];
        //}
        //P_Input.SwitchCurrentControlScheme(device);

        //if (Gamepad.all.Count > 0)
        //{
        //    P_Input.SwitchCurrentControlScheme(Gamepad.all[UserInputMgr.GAMEPADINDEX]);
        //    UserInputMgr.GamePadAssignAction.Invoke();
        //}
        //else
        //{
        //    if (!UserInputMgr.KEYBOARDASSIGN)
        //    {
        //        P_Input.SwitchCurrentControlScheme(Keyboard.current);
        //        UserInputMgr.KeyboardAssignAction.Invoke();
        //    }
        //}

    }
    #region 공통
    public void Move(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        Debug.Log("Move");
#endif
        if (P_Input.currentActionMap.name == "UISelector")
        {
            if (UIControlScript)
            {
                UIControlScript.Move(ctx);
            }
        }
        
        if(P_Input.currentActionMap.name == "Player")
        {
            if (PlayerControlScript)
            {
                PlayerControlScript.Move(ctx);
            }
        }

    }
    public void Menu(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        Debug.Log("Menu");
#endif
        if (P_Input.currentActionMap.name == "UISelector")
        {
            if (UIControlScript)
            {
                UIControlScript.Menu(ctx);
            }
        }

        if (P_Input.currentActionMap.name == "Player")
        {
            if (PlayerControlScript)
            {
                PlayerControlScript.Menu(ctx);
            }
        }
    }
    #endregion

    #region 게임조작

    public void Attack(InputAction.CallbackContext ctx) {
#if UNITY_EDITOR
        Debug.Log("Attack");
#endif
        if (PlayerControlScript)
        {
            PlayerControlScript.Attack(ctx);
        }
    }

    public void Dash(InputAction.CallbackContext ctx) {
#if UNITY_EDITOR
        Debug.Log("Dash");
#endif
        if (PlayerControlScript)
        {
            PlayerControlScript.Dash(ctx);
        }
    }

    public void Throw(InputAction.CallbackContext ctx) {
#if UNITY_EDITOR
        Debug.Log("Throw");
#endif
        if (PlayerControlScript)
        {
            PlayerControlScript.Throw(ctx);
        }
    }
    #endregion

    #region UI
    public void Submit(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        //Debug.Log("Submit Phase" + ctx.phase);
        
#endif
        if (UIControlScript)
        {
            UIControlScript.Select(ctx);
        }
    }
    public void Back(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        //Debug.Log("Back");
        //Debug.Log("Back Phase" + ctx.phase);
#endif
        if (UIControlScript)
        {
            UIControlScript.Back(ctx);
        }
        /*else if(UserInputMgr.BTNMGR)
        {
            UserInputMgr.BTNMGR.MainMenu_EventStartToBack();
        }*/
        //Debug.Log("BackEnd");
    }
    public void PressWest(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        Debug.Log("West");
#endif
        if (UIControlScript)
        {
            UIControlScript.Ready(ctx);
        }
        /*else if(UserInputMgr.BTNMGR)
        {
            UserInputMgr.BTNMGR.MainMenu_EventStartToBack();
        }*/
    }
    public void PressNorth(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        Debug.Log("North");
#endif
        if (UIControlScript)
        {
            UIControlScript.PressYBtn(ctx);
        }
        /*else if(UserInputMgr.BTNMGR)
        {
            UserInputMgr.BTNMGR.MainMenu_EventStartToBack();
        }*/
    }
    public void DeviceLost(PlayerInput input)
    {
#if UNITY_EDITOR
        Debug.Log("DeviceLost, UI조작시만 동작하도록 조건을 넣어줘야함");
#endif
        if (P_Input.currentActionMap.name == "UISelector")
        {
            if (UIControlScript)
            {
                UIControlScript.DeviceLostEvt(input);
            }
        }
    }
    #endregion

    #region 미사용
    public void Join(InputAction.CallbackContext ctx)
    {
#if UNITY_EDITOR
        Debug.Log(ctx.ReadValueAsButton());//bool
#endif
    }


    public void DeviceRegained()
    {

    }
    public void ControlChanged(PlayerInput input)
    {
        
    }
    #endregion
    public void SetInputDevice() {
        if (P_Input.currentControlScheme == null)
        {
            //InputSystem.devices
            P_Input.SwitchCurrentControlScheme(InputSystem.GetDevice("Keyboard"), InputSystem.GetDevice("Mouse"));
            
            //P_Input.SwitchCurrentControlScheme(InputSystem.GetDevice("XInputControllerWindows1"));
        }
    }
    public void ChangeActionMap(ActionMaps Maps)
    {
        switch (Maps)
        {
            case ActionMaps.UI:
                P_Input.SwitchCurrentActionMap("UISelector");
                break;
            case ActionMaps.GAMEPLAY:
                P_Input.SwitchCurrentActionMap("Player");
                break;
        }
        
    }

    public void SetUIControl(UIControl inputBase)
    {
        UIControlScript = inputBase;
    }
    public void SetPlayerControl(PlayerControl inputBase)
    {
        PlayerControlScript = inputBase;
    }
    void SelectCallingEvent(InputAction.CallbackContext ctx)
    {
        /*if ()
        {
            MenuUIInput.Invoke();
        }
        else if ()
        {
            PauseMenuInput.Invoke();

        }
        else
        {

            PlayInput.Invoke();
        }*/
    }//상황별 불러올 이벤트 변경
}
