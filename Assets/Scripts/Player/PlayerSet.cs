﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class PlayerSet : MonoBehaviour
{
    GameObject Text_3d;
    //렌더링 관련
    public PlayerInput P_input;
    string currentscheme;
    public string ModelName;
    public string ObjectName;
    public int ColorNum;
    int iPlayerIndex;
    private int iPlayerKill;
    Transform Tr;
    //조작관련

    CPlayer playerData;
    //게임데이터 관련
    Player_Cal Cal;
    PlayerControl Con;
    PlayerRender Render;

    bool bCallMenu;
    bool bisReady;
    PhotonView PV;
    Transform Parent;
    Vector3 InitPos;
    public bool CALLMENU
    {
        get { return bCallMenu; }
        set { bCallMenu = value; }
    }
    public int PlayerIndex
    {
        get { return iPlayerIndex; }
    }
    public bool ReadyFlag
    {
        get { return bisReady; }
    }
    public PhotonView photonview
    {
        get { return PV; }
    }
    public int PlayerKill
    {
        get { return iPlayerKill; }
    }
    RoundManager RoundMgr;
    public Rigidbody rigid;
    void Awake() {
        //Render = GetComponent<MeshRenderer>();
        //PlayerMat = new Material(Shader.Find("Standard"));
        StartCoroutine("WaitPlayerManger", "PlayerMgr");
        if (TryGetComponent<PhotonView>(out PhotonView pv))
        {
            PV = pv;
            
        }
        
        Con = gameObject.GetComponent<PlayerControl>();
        Cal = GetComponent<Player_Cal>();
        Render = GetComponentInChildren<PlayerRender>();
        rigid = GetComponent<Rigidbody>();
        
        playerData = new CPlayer();
        playerData = CPlayer.DEBUG;
        Text_3d = GetComponent<Transform>().GetChild(0).gameObject;
        P_input = GetComponent<PlayerInput>();
        Tr = GetComponent<Transform>();
        //PlayerInit();
    }
    IEnumerator WaitPlayerManger(string name)
    {
        while (!GameObject.Find(name))
        {
            Debug.Log("Wait"+name);
            yield return new WaitForEndOfFrame();
        }
        Debug.Log("Find " + GameObject.Find(name));
        Parent = GameObject.Find(name).transform;
        Debug.Log("Parent is "+ Parent);
        RoundMgr = GameObject.Find("RoundMgr").GetComponent<RoundManager>();
        transform.SetParent(Parent);
        Parent.GetComponent<PlayersManager>().AddPlayerDatas(this.GetComponent<PlayerSet>());

    }
    private void OnEnable()
    {
        
    }
    void Start() {
        Debug.Log(ModelName +" "+"Reset");
        PlayerInit();
        //CallFunc_ResetData();
        ResetData();
        //SetRender(currentscheme);
    }
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        if (GameManager.GM.GamePauseflg != bCallMenu)
        {
            bCallMenu = GameManager.GM.GamePauseflg;
        }


        //PlayerDead();
    }
    
    private void OnDisable()
    {
        Debug.Log("Dead");
        //playerData.POINT = GameManager.GM.PMGR.SetLeavePlayer();//튜토리얼맵에서 넘어갈땐 안뜨게
        GameManager.GM.PMGR.SetLeavePlayer_ReturnVoid();//튜토리얼맵에서 넘어갈땐 안뜨게
        //playerData.CalDegree(GameManager.GM.ROUNDMGR.RoundNum);
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GROUND")
        {
            if (rigid.useGravity)
            {
                transform.position = new Vector3(transform.position.x, 1, transform.position.z);
                rigid.velocity = Vector3.zero;
                rigid.drag = 10;
                rigid.useGravity = false;
            }
        }

    }
    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "GROUND")
        {
            if (!rigid.useGravity)
            {
                Debug.Log("aa");
                rigid.useGravity = true;
                rigid.drag = 0;
            }
        }
        
    }

    void PlayerInit()
    {

        if (P_input != null)
        {
            currentscheme = P_input.currentControlScheme;
        }
        name = ObjectName;
        //playerData.SetName(name);
        Text_3d.GetComponent<TextMesh>().text = "P"+iPlayerIndex.ToString();


        //로드시 안불러와짐
        /*Con = gameObject.GetComponent<Control>();
        Cal = GetComponent<Player_Cal>();
        Render = GetComponentInChildren<PlayerRender>();
        rigid = GetComponent<Rigidbody>();*/
        //GetComponent<Transform>()position = ;
    }

    public void CallFunc_ResetData()
    {
        PV.RPC("ResetData", PhotonTargets.All);
    }
    [PunRPC]
    public void ResetData()
    {
        
        bCallMenu = GameManager.GM.GamePauseflg;
        //CallFunc_SetParam_iPlayerKill(0);
        rigid.useGravity = true;
        rigid.drag = 0;
        rigid.velocity = Vector3.zero;
        playerData.ResetHp();
        if(InitPos == Vector3.zero)
        {
            SetInitPos(Tr.position);
        }
        //CallFuncResetPosition();
        //ResetPosition();
        Con.ResetData();
        Cal.ResetData();
        Render.ResetData();
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);
        }
    }
    public void SetIndex(int idx)
    {
        iPlayerIndex = idx;
    }
 
    public CPlayer GetPlayerData() {
        return playerData;
    }

    public Transform GetTr() {
        return Tr;
    }
    public string GetcurrentSchreme() {
        return currentscheme;
    }
    public void SetParent(Transform tr)
    {
        Debug.Log("SetParent");
        Parent = tr;
    }
    public void SetInitPos(Vector3 v)
    {
        InitPos = v;
    }
    public void CallFunc_SetValues(string modelName, string objectName, int colorNum, int index)
    {
        PV.RPC("SetValues", PhotonTargets.All, modelName, objectName, colorNum, index);
    }
    [PunRPC]
    void SetValues(string modelName, string objectName, int colorNum, int index)
    {
        ModelName = modelName;
        ObjectName = objectName;
        ColorNum = colorNum;
        iPlayerIndex = index;
    }
    public void CallFunc_SetParent()
    {
        
        PV.RPC("SetParent", PhotonTargets.All);

    }
    [PunRPC]
    void SetParent()
    {
        transform.SetParent(Parent);
    }
    public void CallFunc_SetParam_bIsReady(bool flag)
    {
        PV.RPC("SetParam_bIsReady", PhotonTargets.All, flag);
    }
    [PunRPC]
    void SetParam_bIsReady(bool flag)
    {
        bisReady = flag;
    }
    public void CallFuncResetPosition()
    {
        PV.RPC("ResetPosition", PhotonTargets.All);
    }
    [PunRPC]
    public void ResetPosition()
    {
        Tr.position = InitPos;
    }
    /*public void CallFunc_SetParam_iPlayerKill(int n)
    {
        PV.RPC("SetParam_iPlayerKill", PhotonTargets.All, n);
    }
    [PunRPC]
    void SetParam_iPlayerKill(int n)
    {
        iPlayerKill = n;
    }*/
    /*void PlayerDead() {
        if (!gameObject.activeSelf)
        {
            GameManager.GM.SetLeavePlayer();
        }
    }*/




}
