using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Func;

public class ConvertingFbxObject : MonoBehaviour
{
    GameObject PlayerFbx;
    RuntimeAnimatorController FbxAnimController;
    Player_AnimControl AnimControl;
    Material Mat_Liquid;
    Transform Tr;
    Transform RightHandTransform;
    PhotonView PV;
    // Start is called before the first frame update
    public Transform RightHandTR
    {
        get
        {
            return RightHandTransform;
        }
    }
    public Player_AnimControl AnimCon
    {
        get
        {
            return AnimControl;
        }
    }
    public Material Liquid
    {
        get { return Mat_Liquid; }
    }
    private void Awake()
    {
        Tr = GetComponent<Transform>();
    }
    void Start()
    {
        
    }
    public void ResetConvertingObj()
    {
        Tr.localPosition = Vector3.zero;
        Tr.localRotation = new Quaternion(0, -0.7f, 0, 0.7f);
        //Tr.localRotation = new Quaternion(0, 0 , 0, 0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetFbx(string _str, int n)
    {
        //string _str = "Ch_roundFlask";
        PlayerFbx = Instantiate(Resources.Load<GameObject>("Objects/" + _str), Tr);
        FbxAnimController = Resources.Load<RuntimeAnimatorController>("Animators/Player_Animator");
        //Fbx파일 불러오기


        PlayerFbx.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material =
            new Material(Resources.Load<Material>("Materials/Player_Mat"));//Body->Flask->Bottle
        //불러온 파일의 유리부분에 유리 마테리얼 할당
        PlayerFbx.transform.GetChild(0).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().material =
            new Material(Resources.Load<Material>("Materials/Cap_Mat"));

        Mat_Liquid = new Material(Resources.Load<Material>("Materials/Liquid_Mat"));
        //액체 로드 후 마테리얼 할당
        Mat_Liquid.SetFloat("_FillAmount", 0.2f);
        //할당한 마테리얼의 FillAmount값 초기화
        Color[] Colors = new Color[3];
        Colors = ColorFunction.SetColor(n);

        Mat_Liquid.SetColor("_Tint", Colors[2]);
        //Mat_Liquid.color = Colors[2];
        PlayerFbx.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<MeshRenderer>().material = Mat_Liquid;
        //할당한 마테리얼을 Liquid오브젝트에 할당
        var FootMat = new Material(Resources.Load<Material>("Materials/PlayerMat_Colored"));
        FootMat.SetColor("_BaseColor", Colors[1]);
        FootMat.SetColor("_ColorDim", Colors[2]);
        var HandMat = new Material(Resources.Load<Material>("Materials/PlayerMat_Colored"));
        HandMat.SetColor("_BaseColor", Colors[0]);
        HandMat.SetColor("_ColorDim", Colors[2]);
        PlayerFbx.transform.GetChild(1).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = HandMat;
        PlayerFbx.transform.GetChild(2).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = FootMat;
        PlayerFbx.transform.GetChild(3).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = HandMat;
        PlayerFbx.transform.GetChild(4).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = FootMat;

        PlayerFbx.transform.GetChild(0).GetChild(0).GetChild(2).gameObject.AddComponent<Wobble>();//Body->Flask -> Liquid
                                                                                                  //출렁임 효과 위한 스크립트 등록



        PlayerFbx.GetComponent<Animator>().runtimeAnimatorController = FbxAnimController;
        //Resources.Load<RuntimeAnimatorController>("Animators/Player_Animator");
        //Debug.Log(Resources.Load<Material>("Materials/Player_Mat"));
        //애니메이션 추가   Ch_roundFlask_Animator
        //PlayerFbx.GetComponent<Animator>().cullingMode = AnimatorCullingMode.CullUpdateTransforms;
        //PlayerFbx.GetComponent<Animator>().applyRootMotion = true;
        PlayerFbx.AddComponent<Player_AnimControl>();
        //애니메이션 재생위한 스크립트 추가

        // 애니메이션 해제용 주석 -> 애니메이션 수정 완료 시 주석 제거

        //PlayerFbx.AddComponent<BoxCollider>().size = new Vector3(1.5f, 2.25f, 1.5f);
        //PlayerFbx.GetComponent<BoxCollider>().center = new Vector3(0, -0.2f, 0);
        //PlayerFbx.AddComponent<Rigidbody>();
        //PlayerFbx.GetComponent<Rigidbody>().constraints =
        //    (RigidbodyConstraints.FreezeAll - (int)RigidbodyConstraints.FreezePositionY);


        //Calculate.SetRightHandTr(PlayerFbx.transform.GetChild(3).GetChild(0).GetChild(0));
        RightHandTransform = PlayerFbx.transform.GetChild(3).GetChild(0).GetChild(0);
        //오른손 위치값 할당 -> R_Arm -> PlayerFbx.transform.GetChild(3).GetChild(0).GetChild(0)
        //ArmBon -> 팔 회전 -> PlayerFbx.transform.GetChild(3)
        //HandBon -> 손 회전 -> ArmBon.GetChild(0)
        //Arm -> 마테리얼 존재위치 -> HandBon.GetChild(0)

        RightHandTransform.gameObject.AddComponent<BoxCollider>().isTrigger = true;
        //오른손 박스콜라이더 설정 -> RHand

        //PlayerFbx.transform.GetChild(3).GetChild(0).GetChild(0).gameObject.tag = "FIST";
        //-> 충돌시 루트 오브젝트의 태그값을 읽음
        AnimControl = PlayerFbx.GetComponent<Player_AnimControl>();

        //SetFBXPhoton();

        //디버그용 주석
        //PhotonAnimatorView PAV = PlayerFbx.AddComponent<PhotonAnimatorView>();
        //PAV.SetLayerSynchronized(0, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsRun", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsDash", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsAttack", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsThrow", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsHit", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsDie", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("HItTrg", PhotonAnimatorView.ParameterType.Trigger, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("Blend", PhotonAnimatorView.ParameterType.Float, PhotonAnimatorView.SynchronizeType.Discrete);
        //PAV.SetParameterSynchronized("IsDeadAnimPlay", PhotonAnimatorView.ParameterType.Bool, PhotonAnimatorView.SynchronizeType.Discrete);
        //// PV.ObservedComponents.Add(RightHandTransform.GetComponent<BoxCollider>());
        //PV.ObservedComponents.Add(PAV);

        //PlayerFbx.transform.GetChild(0).GetChild(3).GetChild(1).gameObject.GetComponent<BoxCollider>().enabled = false;

        //PlayerFbx.GetComponent<Transform>().rotation = Quaternion.Euler(0, -90, 0);

        //Calculate.ResetData();
        //PlayerFbx.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.AddComponent<BoxCollider>();
        //PlayerFbx.transform.GetChild(4).GetChild(0).GetChild(0).gameObject.AddComponent<BoxCollider>();
        //불러온 Fbx파일 회전
        ResetConvertingObj();
    }
    void SetFBXPhoton()
    {
        PV = PlayerFbx.AddComponent<PhotonView>();
        PV.viewID = transform.parent.GetComponent<PlayerRender>().photonView.viewID + 1;
        PV.synchronization = ViewSynchronization.UnreliableOnChange;
        PV.ObservedComponents = new List<Component>();
    }
}
