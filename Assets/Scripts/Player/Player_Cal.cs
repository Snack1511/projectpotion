﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Player_Cal
 * 유니티 내에서의 수치 계산 및 물리효과적용에만 사용
 */

public class Player_Cal : MonoBehaviour
{
   
    public bool bHitflg { private set; get; }
    public bool bDieflg { set; get; }
    public bool bDeadAnimEnd { set; get; }
    public bool bInvincibilityflg { private set; get; }
    bool bKillflg;
    
    public bool bIsTouchItem { get; set; }
    public bool bThrowflg { get; set; }
    public bool bThrowControlAnim { get; set; }
    public bool bAttackControlAnim { get; set; }
    bool bCollisionflg;
    bool bMapOutflg;
    bool KillselfCalflag;
    bool bResetflg;
    public Vector3 ToWeaponAttDir { get; set; }
    public Transform RightHandTr { private set; get; }
    public float fAttactedDmg { set; get; }
    public float fKnockbackforce { set; get; }
    public float Speed;
    public float fDashforce = 3f;
    public PlayerSet SetPlayer;
    public WeaponType WT;
    public Player_AnimControl AnimControl;
    //float fMoveThrough;

    Transform Tr;
    Rigidbody Rigid;
    PlayerSet AttByEnemy;
    CWeaponData WeaponData;
    PlayerControl control;
    GameObject Slots;
    Vector3 MoveDir;
    Vector3 AttactedDir;//없어도 될거 같음
    //차후 수정 필요
    // Start is called before the first frame update

    public bool KillSelfFlag
    {
        get { return KillSelfFlag; }
    }
    private void Awake()
    {
        Init();
    }
    void Start()
    {
        
        ResetData();
    }
    void FixedUpdate() {
        if (!bMapOutflg)
        {
            Cal_Dash();
        }
        //대시
    }
    // Update is called once per frame
    void Update()
    {
        //if(transform.position.y <= -1f)
        //{
        //    bDieflg = true;
        //}
        if (!bResetflg)
        {
            if (!bDieflg)
            {

                Cal_Attacked();// 피격

                SetDieflg(AttByEnemy);

                Cal_Throw();//투척

                Cal_Move(bCollisionflg);//이동


            }//사망 확인
            else
            {
                State_Dead();
            }
            Cal_Point(ref bKillflg, IsKillSelf(bDieflg, AttByEnemy));
        }
        //포인트 계산
        //SetEnemy();//필요없는 연산같음
    }


    private void OnTriggerEnter(Collider other)
    {
        /*if(other.tag == "GROUND")
        {
            if (bMapOutflg)
            {
                bMapOutflg = false;
            }
        }*/
        if (other.tag == "WEAPON")
        {
            SetHitflg(true);//sword -> blade
            AttactedDir = other.transform.parent.position;//없어도 될거 같음

        }
        else if(other.tag == "FIST")
        {
            SetEnemy(other.transform.parent.parent.parent.parent.parent.parent.name);
            SetHitflg(true);
            AttactedDir = other.transform.root.position;//없어도 될거 같음

        }
        else if(other.tag == "ITEM")
        {
            
            if (other.transform.parent == null)
            {
                CheckItem(other.gameObject);
            }
            else
            {
                if (!control.bHaveWeapon)
                {
                    CheckItem(other.transform.parent.gameObject);//sword -> blade
                }

            }//tag -> Melee Range를 세분화 시키면 더 직관적일거 같음
            
            
        }

    }//아이템 습득 및 무기 피격 구현
    
    private void OnTriggerStay(Collider other)
    {
        if (!control.bAttackflg)
        {
            if (other.tag == "Player"|| other.tag == "MAP")
            {
                Rigid.Sleep();
                bCollisionflg = true;
                if (control.bAnim_Dashflg)
                {
                    //control.bAnim_Dashflg = false;
                    control.CallFunc_SetParamAnimDash(false);
                }
                if (control.bDashflg)
                {
                    //control.bDashflg = false;
                    control.CallFunc_SetParamDash(false);
                }
                MoveDir = transform.position - other.transform.localPosition;
                MoveDir = new Vector3(MoveDir.x, 0, MoveDir.z);
                //Debug.Log("transform.position : " + transform.position);
                //Debug.Log("other.transform.position : " + other.transform.localPosition);
                //Debug.Log("MoveDir : " + MoveDir);
                MoveDir = (MoveDir + control.MoveVal).normalized;
            }
        }
    }
    int ExitCount = 0;
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "MAP" || other.tag == "Player")
        {
            bCollisionflg = false;
            
        }
        if(other.tag == "GROUND")
        {
            bMapOutflg = true;
            Debug.Log("OutGround");
            if (control.bAnim_Dashflg)
            {
                //control.bAnim_Dashflg = false;
                control.CallFunc_SetParamAnimDash(false);
            }
            if (control.bDashflg)
            {
                //control.bDashflg = false;
                control.CallFunc_SetParamDash(false);
            }
            Rigid.velocity = Vector3.zero;
            Rigid.Sleep();

        }
    }
    
    void Init() {
        SetPlayer = GetComponent<PlayerSet>();
        control = GetComponent<PlayerControl>();
        Rigid = GetComponent<Rigidbody>();
        Tr = SetPlayer.GetTr();
        Speed = SetPlayer.GetPlayerData().SPD;
        fDashforce = SetPlayer.GetPlayerData().DSH;
        Slots = Tr.GetChild(2).GetChild(1).gameObject;
        bResetflg = false;


    }
    //초기화 -> 게임시작시 동작

    public void ResetData() {
        bResetflg = true;
        Debug.Log(gameObject.name + "Player_Cal.Reset");
        bHitflg = false;
        bDieflg = false;
        Debug.Log("Tr.position.y : " + Tr.position.y);
        //if(Tr!= null && Tr.position.y<= -1f)
        //{
        //    SetPlayer.ResetPosition();
        //}
        bDeadAnimEnd = false;
        bKillflg = false;
        bInvincibilityflg = false;
        bIsTouchItem = false;
        bThrowflg = false;
        bThrowControlAnim = false;
        AttByEnemy = null;
        bCollisionflg = false;
        bMapOutflg = false;
        KillselfCalflag = false;
        WT = WeaponType.PUNCH;
        if (RightHandTr != null)
        {
            RightHandTr.tag = RightHandTr.parent.tag;
        }
        bResetflg = false;
        control.SetPauseflg(false);
    }
    //데이터 리셋 -> 라운드 변화시 동작
    //나중에 만들 GameMgr 구현시 함께 사용

    void Cal_Move(bool flg) {
        if (!control.bDashflg && !control.bAnim_Dashflg)
        {
            if (!control.bAnim_Throwflg && !control.bAnim_Attflg)
            {
                if (flg)
                {
                    Tr.Translate(MoveDir.normalized * Speed * Time.deltaTime);
                }
                else
                {
                    Tr.Translate(control.MoveVal.normalized * Speed * Time.deltaTime);
                }
            }

        }
    }

    /*
   void Cal_Attack(GameObject obj) {
        if (control.Attackflg)
        {
            if (!control.haveWeapon)
            {

                RightHandTr.tag = "FIST";
            }
        }

    }*/

    void Cal_Dash() {
        if (!bCollisionflg)
        {
            if (control.bDashflg)
            {
                Rigid.AddForce(control.DashDir * fDashforce, ForceMode.VelocityChange);
            }
            /*else
            {
                //bDashEndflg = true;
            }*/
            Rigid.AddForceAtPosition(Vector3.zero, Tr.position, ForceMode.VelocityChange);
            
        }
        else
        {
            Rigid.velocity = Vector3.zero;
        }
    }
    //Dash키 입력시 활성화

    void Cal_Throw() {
        if (!control.bHaveWeapon && RightHandTr.childCount != 0)
        {
            if (bThrowControlAnim)
            {
                Debug.Log("Cal_Throw");
                WT = WeaponType.PUNCH;
                ToWeaponAttDir = control.AimDir;
                bThrowflg = true;
                SetPlayer.GetPlayerData().ResetAttackValue();
                Slots.GetComponent<SlotScript>().ReleaseSlot();
            }
        }
        if (bThrowControlAnim)
        {
            // bThrowControlAnim = false;
            CallFunc_ThrowControlAnim(false);
        }


    }
    //투척
    void Cal_Attacked() {
        if (bHitflg && !bInvincibilityflg)
        {
            if (WeaponData != null)
            {
                if (WeaponData.OWNER != gameObject.name)
                {
                    Move_KnockBack(AttByEnemy, WeaponData);
                    State_Stun(WeaponData.STUNTIME);
                    State_Invicibility(SetPlayer.GetPlayerData().INVIN);
                    Cal_Dmg(WeaponData.DMG);
                }
            }
            else
            {
                Move_KnockBack(AttByEnemy);
                State_Stun(AttByEnemy.GetPlayerData().STUNTIME);
                State_Invicibility(SetPlayer.GetPlayerData().INVIN);
                Cal_Dmg(AttByEnemy.GetPlayerData().DMG);
            }
            SoundManager.instance.PlayFXSound("Hit");
        }
    }
    //피격

    void Move_KnockBack(PlayerSet Enemy, CWeaponData WD = null)
    {
        float Knockforce = Enemy.GetPlayerData().KNOCKFORCE;
        Vector3 KnockDir;
        if (WD != null) {
            Knockforce = WD.KNOCKFORCE;
        }
        KnockDir = new Vector3((Tr.position.x - Enemy.GetTr().position.x), 0f, (Tr.position.z - Enemy.GetTr().position.z)).normalized;
        Rigid.AddForceAtPosition(KnockDir * Knockforce, Tr.position, ForceMode.VelocityChange);
    }
    //충돌 시 활성화

    void State_Stun(float _Time) {
        if (!bDieflg)
        {
            control.SetPauseflg(true);
            StartCoroutine("DelayAndChangePause", _Time);
        }
    }
    //상태이상 : 스턴 >> CPlayer의 fStuntime 변수사용
    IEnumerator DelayAndChangePause(float time) {
        yield return new WaitForSeconds(time);
        control.SetPauseflg(false);
    }
    //time 만큼시간 이후 SetPauseflg false

    void State_Invicibility(float _Time) {
        
        StartCoroutine("SetInvincivbility", _Time);
    }
    //상태이상 : 무적
    IEnumerator SetInvincivbility(float _Time) {
        if (!bInvincibilityflg)
        {
            bInvincibilityflg = true;
            yield return new WaitForSeconds(_Time);
            bInvincibilityflg = false;
        }
    }
    //bInvincibilityflg 값 변화

    void State_Dead() {
        if (bDeadAnimEnd)
        {
            StartCoroutine("RemainDead");
        }
    }
    //상태이상 : 스턴

    void SetDieflg(PlayerSet Enemy) {
        if (SetPlayer.GetPlayerData().HP <= 0f || SetPlayer.GetTr().position.y <= -1f) {
            if (Enemy != null) {
                Enemy.GetComponent<Player_Cal>().bKillflg = true;
            }
            Debug.Log("SetPlayer.GetPlayerData().HP : " + SetPlayer.GetPlayerData().HP);
            Debug.Log(" SetPlayer.GetTr().position.y : " + SetPlayer.GetTr().position.y);
            bDieflg = true; 
        }
        else bDieflg = false;
    }
    //PlayerSet안의 플레이어 데이터의Hp가 0일경우 동작
    //상대 오브젝트의 Killflg를 대신 동작시켜줌

    IEnumerator RemainDead()
    {
        
        yield return new WaitForSeconds(3f);
        control.SetPauseflg(true);
        SetPlayer.CallFuncResetPosition();
        gameObject.SetActive(false);
        transform.GetChild(1).GetChild(0).localPosition = new Vector3(0, 1, 0);
    }

    public void Cal_Point(ref bool IsKill, bool IsKillself)
    {
        if (IsKill) SetPlayer.GetPlayerData().POINT += 1;
        if (IsKillself && !KillselfCalflag)
        {
            GameManager.GM.PMGR.GetPointAlivePlayer(SetPlayer);
            KillselfCalflag = true;
        }
        IsKill = false;
    }
    //상대 플레이어 오브젝트가 변경한 Killflg가 변경시 점수 적용
    //아래의 IsKillSelf와 연동해 자살일 경우 포인트 감소

    bool IsKillSelf(bool Dieflg, PlayerSet Enemy) {
        if (Dieflg && Enemy == null) return true;
        else return false;
    }
    //포인트 계산시 매개변수로 쓰임

    void CheckItem(GameObject obj)
    {
        
        if (!control.bHaveWeapon&&RightHandTr.childCount == 0)
        {
            RightHandTr.tag = RightHandTr.parent.tag;
            //bIsTouchItem = true;
            CallFunc_IsTouchItem(true);
            WeaponData = obj.GetComponent<Melee>().GetWeapondata();
            Slots.GetComponent<SlotScript>().FillSlot(obj.GetComponent<Melee>().GetWeapondata().IMG);
            Slots.SetActive(true);
            //control.bHaveWeapon = true;
            control.CallFunc_SetParamHaveWeapon(true);
        }

    }
    //무기 소지 상태 변화 함수 -> 현재 소지 무기가 없을 경우 동작

    public void SetHitflg(bool flg) {
        if (bHitflg != flg)
        {
            if (flg)
            {
                if (!bInvincibilityflg)//SetInvincivbility:IEnumerator 에서 변경
                {
                    AnimControl.Play_HIt();
                    //bHitflg = flg;
                    CallFunc_Hitflg(flg);
                }
            }
            else
            {
                //bHitflg = flg;
                CallFunc_Hitflg(flg);
            }
        }
    }//충돌 처리부분에서 사용
    
    public void Cal_Dmg(float _Dmg)
    {
        Debug.Log(gameObject.name + "HP : " + SetPlayer.GetPlayerData().HP + " AttackedDmg : " + _Dmg);
        SetPlayer.GetPlayerData().HP -= _Dmg;

        CallFunc_HP(SetPlayer.GetPlayerData().HP);
        SetHitflg(false);
    }//받은 Dmg에 따른 Hp계산

    public void SetEnemy(string Enemy = null, bool flg = false, CWeaponData WD = null) {
        
        if (flg)
        {
            WeaponData = WD;
            AttByEnemy = GameObject.Find(WD.OWNER).GetComponent<PlayerSet>();
        }
        else
        {
            WeaponData = null;
            AttByEnemy = GameObject.Find(Enemy).GetComponent<PlayerSet>();
        }
    }//상대의 무기데이터를 받아 동작


    public void SetRightHandTr(Transform tr) {
        RightHandTr = tr;
    }
    //RightHandTr함수를 외부에서 등록하는 함수
    public void CallFunc_ThrowControlAnim(bool flag)
    {
        Debug.Log("CallFunc_ThrowControlAnim");
        SetPlayer.photonview.RPC("SetParam_ThrowControlAnim", PhotonTargets.All, flag);
    }
    [PunRPC]
    void SetParam_ThrowControlAnim(bool flag)
    {
        Debug.Log("SetParam_ThrowControlAnim");
        bThrowControlAnim = flag;
    }
    public void CallFunc_IsTouchItem(bool flag)
    {
        Debug.Log("CallFunc_IsTouchItem");
        SetPlayer.photonview.RPC("SetParam_IsTouchItem", PhotonTargets.All, flag);
    }
    [PunRPC]
    void SetParam_IsTouchItem(bool flag)
    {
        Debug.Log("SetParam_IsTouchItem");
        bIsTouchItem = flag;
    }

    public void CallFunc_HP(float value)
    {
        Debug.Log("CallFunc_HP");
        Debug.Log("HP : "+ value);
        SetPlayer.photonview.RPC("SetParam_HP", PhotonTargets.Others, value);
    }
    [PunRPC]
    void SetParam_HP(float value)
    {
        Debug.Log("RPC SetParam_HP");
        if (value >= SetPlayer.GetPlayerData().HP)
        {
            Debug.Log(value+"(value)" + " >= "+ SetPlayer.GetPlayerData().HP+"(SetPlayer.GetPlayerData().HP)");
            SetPlayer.GetPlayerData().HP = value;
        }
    }

    public void CallFunc_Hitflg(bool flag)
    {
        Debug.Log("CallFunc_Hitflg, Hitflg : "+flag);
        SetPlayer.photonview.RPC("SetParam_bHitflg_ToMaster", PhotonTargets.MasterClient, flag);
    }
    [PunRPC]
    void SetParam_bHitflg_ToMaster(bool flag)
    {
        if (bHitflg != flag)
        {
            Debug.Log("this is MasterClient");
            bHitflg = flag;
            SetPlayer.photonview.RPC("SetParam_bHitflg", PhotonTargets.Others, flag);
        }
    }
    [PunRPC]
    void SetParam_bHitflg(bool flag)
    {
        if (bHitflg != flag)
        {
            bHitflg = flag;
        }
    }


}
