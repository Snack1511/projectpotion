using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
public class PlayerControl : MonoBehaviour
{
    PlayerSet script;
    //Transform tr;
    //InputPlayer Player_Input;
    //PlayerInput P_Input;
    //bool Alterflg;
    public float fAimVal { private set; get; }
    public bool bAttackflg { private set; get; }
    public bool bDashflg { set; get; }
    //public bool ThrowChargeflg { private set; get; }
    public bool bPauseflg { private set; get; }
    public Vector3 MoveVal { private set; get; }
    public Vector3 AimDir { private set; get; }
    public Vector3 DashDir { private set; get; }
    //public float Speed;
    // Start is called before the first frame update
    //public float ThrowPower { private set; get; }



    public bool bHaveWeapon;
    public bool bHavePassive;
    public bool bHaveEffect;

    public bool bAnim_Moveflg;
    public bool bAnim_Attflg;
    public bool bAnim_Dashflg;
    public bool bAnim_Throwflg;

    
    private void Awake()
    {
        //InitInput();
        ResetData();
    }
    private void OnEnable()
    {
        //Player_Input.Enable();

    }
    void Start()
    {
        script = GetComponent<PlayerSet>();

    }


    private void OnDisable()
    {
        //Player_Input.Disable();
    }

    //=====UserCustomMathod
    //void InitInput()
    //{
    //    Player_Input = new InputPlayer();

    //    P_Input = GetComponent<PlayerInput>();
    //    if (P_Input.currentControlScheme == null)
    //    {
    //        P_Input.SwitchCurrentControlScheme(InputSystem.GetDevice("Keyboard"));
    //        //P_Input.SwitchCurrentControlScheme(InputSystem.GetDevice("XInputControllerWindows1"));
    //    }
        
    //}

    public void ResetData()
    {
        MoveVal = Vector3.zero;
        fAimVal = 0;
        AimDir = Vector3.zero;
        bAttackflg = false;
        bDashflg = false;

        bHaveWeapon = false;
        bHavePassive = false;
        bHaveEffect = false;

        bAnim_Moveflg = false;
        bAnim_Attflg = false;
        bAnim_Dashflg = false;
        bAnim_Throwflg = false;

        //CallFunc_SetParamMove(false);
        //CallFunc_SetParamAttack(false);
        //CallFunc_SetParamDash(false);
        //CallFunc_SetParamThrow(false);
    }
    public void Move(InputAction.CallbackContext ctx)
    {
        if (!bPauseflg && !script.CALLMENU)
        {

            //bAnim_Moveflg = true;
            CallFunc_SetParamMove(true);
            MoveVal = new Vector3(ctx.ReadValue<Vector2>().x, 0, ctx.ReadValue<Vector2>().y);
            if (ctx.ReadValue<Vector2>() != new Vector2(0, 0))
            {
                if (!bAnim_Throwflg && !bAnim_Dashflg && !bAnim_Attflg)
                {
                    fAimVal = Mathf.Atan2(ctx.ReadValue<Vector2>().y, ctx.ReadValue<Vector2>().x) * Mathf.Rad2Deg;
                    fAimVal += 90;
                    //AimDir = new Vector3(ctx.ReadValue<Vector2>().x, 0, ctx.ReadValue<Vector2>().y).normalized;
                    CallFunc_SetParameter_AimDir(new Vector3(ctx.ReadValue<Vector2>().x, 0, ctx.ReadValue<Vector2>().y).normalized);
                }
            }
            else
            {
                //bAnim_Moveflg = false;
                CallFunc_SetParamMove(false);
            }
        }
    }//다른 애니메이션 재생 중 입력 불가

    public void Join(InputAction.CallbackContext ctx)
    {
        Debug.Log(ctx.ReadValueAsButton());//bool
    }


    public void Attack(InputAction.CallbackContext ctx)
    {
        if (!bPauseflg && !script.CALLMENU)
        {
            //bAnim_Attflg = true;
            CallFunc_SetParamAttack(true);
            bAttackflg = ctx.ReadValueAsButton();
        }
    }

    public void Dash(InputAction.CallbackContext ctx)
    {
        if (!bPauseflg && !script.CALLMENU)
        {
            //Debug.Log("Dash" + " " + ctx.phase);
            DashDir = AimDir;
            //bDashflg = true;
            //bAnim_Dashflg = true;
            CallFunc_SetParamDash(true);
            CallFunc_SetParamAnimDash(true);
        }
        else
        {
            bDashflg = false;
        }
    }

    public void Throw(InputAction.CallbackContext ctx)
    {
        if (!bPauseflg && !script.CALLMENU)
        {
            if (bHaveWeapon)
            {
                //bAnim_Throwflg = true;
                CallFunc_SetParamThrow(true);
                //bHaveWeapon = false;
                CallFunc_SetParamHaveWeapon(false);

            }

        }
    }
    public void Menu(InputAction.CallbackContext ctx)
    {
        if (script.CALLMENU)
        {
            GameManager.GM.GamePauseflg = false;

        }
        else
        {
            GameManager.GM.GamePauseflg = true;
        }
    }
    public void DeviceLost()
    {
        Debug.Log("DeviceLost");
        Time.timeScale = 0f;
    }
    public void DeviceRegained()
    {
        Debug.Log("DeviceRegained");
        Time.timeScale = 1f;
    }
    public void ControlChanged()
    {

    }
    
    //public void SetInputDevice()
    //{
    //    if (P_Input.currentControlScheme == null)
    //    {
    //        //InputSystem.devices
    //        P_Input.SwitchCurrentControlScheme(InputSystem.GetDevice("Keyboard"), InputSystem.GetDevice("Mouse"));
    //        //P_Input.SwitchCurrentControlScheme(InputSystem.GetDevice("XInputControllerWindows1"));
    //    }
    //}

    public void SetPauseflg(bool _flg)
    {
        bPauseflg = _flg;
    }


    public void CallFunc_SetParamMove(bool flg)
    {
        script.photonview.RPC("SetParameter_AnimMove", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamThrow(bool flg)
    {
        script.photonview.RPC("SetParameter_AnimThrow", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamAttack(bool flg)
    {
        script.photonview.RPC("SetParameter_AnimAttack", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamAnimDash(bool flg)
    {
        script.photonview.RPC("SetParameter_AnimDash", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamDash(bool flg)
    {
        script.photonview.RPC("SetParameter_Dash", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamHaveWeapon(bool flg)
    {
        script.photonview.RPC("SetParameter_HaveWeapon", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamHavePassive(bool flg)
    {
        script.photonview.RPC("SetParameter_HavePassive", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParamHaveEffect(bool flg)
    {
        script.photonview.RPC("SetParameterHaveEffect", PhotonTargets.All, flg);
    }
    public void CallFunc_SetParameter_AimDir(Vector3 dir)
    {
        script.photonview.RPC("SetParameter_AimDir", PhotonTargets.All, dir);
    }
    [PunRPC]
    void SetParameter_AnimMove(bool flag)
    {
        bAnim_Moveflg = flag;
    }
    [PunRPC]
    void SetParameter_AnimThrow(bool flag)
    {
        bAnim_Throwflg = flag;
    }
    [PunRPC]
    void SetParameter_AnimAttack(bool flag)
    {
        bAnim_Attflg = flag;
    }
    [PunRPC]
    void SetParameter_AnimDash(bool flag)
    {
        bAnim_Dashflg = flag;
    }
    [PunRPC]
    void SetParameter_Dash(bool flag)
    {
        bDashflg = flag;
    }
    [PunRPC]
    void SetParameter_HaveWeapon(bool flag)
    {
        bHaveWeapon = flag;
    }
    [PunRPC]
    void SetParameter_HavePassive(bool flag)
    {
        bHavePassive = flag;
    }
    [PunRPC]
    void SetParameterHaveEffect(bool flag)
    {

        bHaveEffect = flag;
    }
    [PunRPC]
    void SetParameter_AimDir(Vector3 dir)
    {
        AimDir = dir;
    }
}
