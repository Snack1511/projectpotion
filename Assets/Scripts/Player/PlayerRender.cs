﻿using UnityEngine;
//using UnityEditor.Animations;
using UnityEngine.UI;
using PhotonHashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerRender : Photon.PunBehaviour
{
    Transform Tr;
    Slider Hpbar;
    GameObject ConvertedGameObject;
    GameObject PlayerFbx;
    Material Mat_Liquid, Mat_PlayerColor;
    MeshRenderer MRender;//플레이어별 색 할당용...?
    string MeshName, ColorName, Parent;
    //public bool CaculateHit;

    PlayerSet script;
    PlayerControl control;
    Player_Cal Calculate;
    RuntimeAnimatorController FbxAnimController;
    public bool bHitflg;
    bool bDieflg;
    PhotonView PV;
    PhotonHashtable Properties;
    float PlayerHp;
    // Start is called before the first frame update
    private void Awake()
    {
        
        //StartCoroutine("StartCheck");
        if (TryGetComponent<PhotonView>(out PhotonView pv))
        {
            PV = pv;
        }
        Tr = GetComponent<Transform>();
        Hpbar = Tr.parent.GetChild(2).GetChild(0).GetComponent<Slider>();
        AttachScript();
        //SetFbx("fbx이름을 변수로 받음");
    }

    void Start()
    {
        Debug.Log("RenderStart");
        //SetFbx(script.ModelName, script.ColorNum);
        SetCharacter(script.ModelName, script.ColorNum);
        name = transform.parent.name + "_Render";
        Parent = gameObject.transform.parent.name;
        ResetData();
        //AttachScript();
        //SetFbx("fbx이름을 변수로 받음");
        //SetRender(script.GetcurrentSchreme());
    }

    // Update is called once per frame
    void Update()
    {
        if (!Calculate.bDieflg)
        {
            /*if (!control.Anim_Throwflg)//!control.ThrowChargeflg)
            {
                Tr.rotation = Quaternion.AngleAxis(control.AimVal, Vector3.down);
                
            }*/
            if (PV != null)
            {
                if (PV.isMine)
                {
                    Tr.rotation = Quaternion.AngleAxis(control.fAimVal, Vector3.down);
                }
            }
            else
            {
                Tr.rotation = Quaternion.AngleAxis(control.fAimVal, Vector3.down);
            }
            

            
        }
        


    }
    private void LateUpdate()
    {
        if (script.GetPlayerData().HP < PlayerHp)//bHitflg가 Player_Cal내에서 동작을 처리하고 끝나버림
        {                     //가장 간단하게는 SetHitflg를 Update내에서 따로 처리하면 될거 같음
            Debug.Log(transform.parent.name + "Ouch!!!");
            if (!bDieflg)
            {
                MatCal(script.GetPlayerData().HP);
            }
        }
    }

    public void ResetData()
    {
        bHitflg = false;
        bDieflg = false;
        Hpbar.value = 0.9f;
        //PlayerHp = script.GetPlayerData().HP;
        MatCal(script.GetPlayerData().HP);
        //transform.rotation = new Quaternion(0, 0, 0, 1);
        if (ConvertedGameObject != null)
        {
            Debug.LogWarning("Call ResetRotate");
            ConvertedGameObject.GetComponent<ConvertingFbxObject>().ResetConvertingObj();
        }
        else
        {
        }
        
    }
    void AttachScript() {
        script = transform.parent.gameObject.GetComponent<PlayerSet>();
        control = transform.parent.gameObject.GetComponent<PlayerControl>();
        Calculate = transform.parent.gameObject.GetComponent<Player_Cal>();
    }

    void SetCharacter(string _str, int n)
    {
        Debug.Log("Set ConvertObj");
        if (_str != null)
        {
            ConvertedGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/ConvertingFbxObject"), Tr);
            var Convertingscript = ConvertedGameObject.GetComponent<ConvertingFbxObject>();
            Convertingscript.SetFbx(_str, n);
            Mat_Liquid = Convertingscript.Liquid;
            Calculate.SetRightHandTr(Convertingscript.RightHandTR);
            Calculate.AnimControl = Convertingscript.AnimCon;
            PlayerFbx = ConvertedGameObject.transform.GetChild(0).gameObject;
        }
    }

    void MatCal(float value)
    {
        if (Mat_Liquid != null)
        {
            Mat_Liquid.SetFloat("_FillAmount", 0.8f - ((value) * 0.2f));
            //Mat_Liquid.SetFloat("_FillAmount", Mat_Liquid.GetFloat("_FillAmount") + 0.2f);
            PlayerFbx.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<MeshRenderer>().material = Mat_Liquid;
            Hpbar.value = 0.3f * value;

            PlayerHp = value;
            bHitflg = false;
            if (Calculate.bDieflg)
            {
                bDieflg = true;
            }
        }
    }

    void MatCal2()
    {
        Debug.Log("Shrink");
        Mat_Liquid.SetFloat("_FillAmount", Mat_Liquid.GetFloat("_FillAmount") + 0.2f);
        PlayerFbx.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<MeshRenderer>().material = Mat_Liquid;
        Hpbar.value -= 0.3f;

        bHitflg = false;
        if (Calculate.bDieflg)
        {
            bDieflg = true;
        }
    }//Run on Ontrigger
}
