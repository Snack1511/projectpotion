using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileUndrow : MonoBehaviour
{
    [SerializeField] GameObject HostButton;
    [SerializeField] GameObject GuestButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
#if UNITY_ANDROID
        Debug.Log("Android");
        OnDrawButton(false);
#else
        OnDrawButton(true);
#endif

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnDrawButton(bool bothflg)
    {
        if (bothflg)
        {
            HostButton.SetActive(bothflg);
            GuestButton.SetActive(bothflg);
            TransformButton(HostButton, new Vector2(-300, 0));
            TransformButton(GuestButton, new Vector2(300, 0));
        }
        else
        {
            HostButton.SetActive(bothflg);
            GuestButton.SetActive(!bothflg);
            TransformButton(GuestButton, Vector2.zero);
        }
    }

    void TransformButton(GameObject obj, Vector2 pos)
    {
        obj.GetComponent<RectTransform>().anchoredPosition = pos;
    }
}
