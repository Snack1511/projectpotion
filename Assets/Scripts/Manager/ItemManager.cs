using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    [SerializeField] public GameObject MeleeItem;
    List<GameObject> ItemList = new List<GameObject>();
    int PlayerCount;
    public const float Dist = 8;
    Vector3[] pos = {
        new Vector3(-Dist, 2.5f, 0), new Vector3(Dist, 2.5f, 0), new Vector3(0, 2.5f, -Dist*0.75f), new Vector3(0, 2.5f, Dist*0.75f)
    };
    float dist;
    public void SetMgr(int PlayerCnt)
    {
        PlayerCount = PlayerCnt;
    }
    public void ResetItemManager()
    {
        Debug.Log("ResetItemMgr");
        for (int i = 0; i < PlayerCount; i++)
        {
            if (ItemList.Count < PlayerCount)
            {
                ItemList.Add(Instantiate(MeleeItem, transform));
            }
            ItemList[i].GetComponent<Melee>().SetItemManager(this.gameObject);
            ItemList[i].GetComponent<Melee>().ResetWeapon();
            ItemList[i].GetComponent<Melee>().ResetWeaponPos(pos[i]);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
