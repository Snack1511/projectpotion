using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Data.UIData;
using Custom.Func;
public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    //[SerializeField] ScreenData screenData;
    [SerializeField] List<DisplayData> displayData = new List<DisplayData>();
    [SerializeField] Dictionary<string, UIRectData> ObjectData = new Dictionary<string, UIRectData>();
    [SerializeField] Dictionary<string, UIDrawData> UIDraw = new Dictionary<string, UIDrawData>();
    public List<DisplayData> DISPLAYDATA
    {
        get { return displayData; }
    }

    public Dictionary<string, UIRectData> UIRECTDATA
    {
        get { return ObjectData; }
    }
    public Dictionary<string, UIDrawData> UIDRAWDATA
    {
        get { return UIDraw; }
    }
    public float GetRatioValue(int index)
    {
        return displayData[index].RATIOVALUE;
    }
    private void Awake()
    {
        InitUIManager();
        instance = this;//.gameObject.GetComponent<UIManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        //InitUIManager();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void InitUIManager()
    {
        displayData = DataReadFunction.ReadResolution(Application.dataPath);
        ObjectData = DataReadFunction.ReadObjectData(Application.dataPath);
        UIDraw = DataReadFunction.ReadUIDrawData(Application.dataPath);
#if UNITY_STANDALONE_WIN
        
#if UNITY_EDITOR
        foreach (string str in ObjectData.Keys)
        {
            Debug.Log(str);
            Debug.Log(ObjectData[str]);
        }
        Debug.Log(ObjectData["MenuPanel"].DebugValues(0));
        Debug.Log(displayData[0].DebugValues());
        Debug.Log(UIDraw["MenuPanel"].DebugValues());
#endif

        //var Readdata = DataReadFunction.ReadFile(Application.dataPath, "ResolutionSetting");
        //screenData = new ScreenData(Readdata[0]["OS"], Readdata[0]["DisplayWidthRatio"], Readdata[0]["DisplayHeightRatio"], Readdata[0]["DisplayWidth"], Readdata[0]["DisplayHeight"]);
        //UIData.Add("Key", Readdata[i]["Key"]);
        //Screen.SetResolution();
#elif UNITY_ANDROID
        //Screen.SetResolution();
        //Screen.orientation = ScreenOrientation.Landscape;
        //UIData.Add("Key", Readdata[i]["Key"]);
#endif

    }
    
}
