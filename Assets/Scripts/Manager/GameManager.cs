﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Custom.Func;
using Custom.Data;
//using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;
    public bool GamePauseflg;
    [SerializeField] private GameObject Userobj;
    [SerializeField] private GameObject VirtualPad;
    [SerializeField] private GameObject PlayerPrefab;
    [SerializeField] private GameObject PadGuide;
    [SerializeField] private GameObject UIControl;
    [SerializeField] onClickScript ButtonMgr;
    [SerializeField] string gameVersion;
    [SerializeField] int EndTotalScore;

    //public static SelectData[] Selected = new SelectData[4];//플레이어 정보 담는 데이터
    UIManager UIMgr;
    UserInputManager UserMgr;
    SceneManager SceneMgr;
    RoomManager RoomMgr;
    List<GameObject> ResetList = new List<GameObject>();

    public SceneManager SCENEMGR
    {
        get {
            
            return SceneMgr;
        }
        set
        {
            SceneMgr = value;
        }
    }
    public SelectData[] Selected
    {
        get { return SCENEMGR.Selected; }
    }
    public RoundManager ROUNDMGR
    {
        get { return SceneMgr.ROUNDMGR; }
        
    }
    public PlayersManager PMGR
    {
        get {
            Debug.Log(gameObject + ", " + SceneMgr);
            return SceneMgr.PMGR; }
    }
    public UserInputManager USERMGR
    {
        get { return UserMgr; }
    }
    public onClickScript BTNMGR
    {
        get { return ButtonMgr; }
    }

    public RoomManager ROOMMGR
    {
        get { return RoomMgr; }
    }
    public string Version
    {
        get { return gameVersion; }
    }
    public GameObject VIRTUALPAD
    {
        get { return VirtualPad; }
    }
    public int TotalScore
    {
        get { return EndTotalScore; }
    }
    //public bool GamePauseflg;//중지플레그
    //public bool Tutorialchkflg;//튜토리얼 확인 플레그
    //GameObject MenuPanel;
    //GameObject ScoreboardPanel;

    //List<PlayerInput> P_Input;
    // Start is called before the first frame update

    private void Awake()
    {
        Debug.Log("GameMgr Awake");
        
        InitGameMgr();
        SoundManager.instance.PlayBGMSound("MenuBGM");
    }
    void Start()
    {
        //ResetGameMgr();
        //RoundMgr.RoundStart();
        //StartCoroutine("ResetValues", RoomMgr.ROUNDMGR);
        //이거도 룸메니저에서 돌리는거 생각해 봐야함

    }
    // Update is called once per frame
    void Update()
    {
        
        
    }
    /*void LateUpdate()
    {

        //RoundMgr.RoundEndCheck();
        if (GamePauseflg)
        {
            StartCoroutine("MenuOpen");
        }
    }*/

    public void CreateSceneMgr()
    {
        
        SceneMgr = GenericFuncs.InitMgr<SceneManager>("SceneMgr").GetComponent<SceneManager>();
        Debug.Log(gameObject + ", "+SceneMgr);
        //RoomMgr.SetManager(Selected);
    }
    public void CreateRoomMgr()
    {
        RoomMgr = GenericFuncs.InitMgr<RoomManager>("RoomMgr").GetComponent<RoomManager>();
    }
    

    void InitGameMgr()
    {
        if (GM == null)
        {
            GM = this;
            DontDestroyOnLoad(gameObject);
        }
        if (GM != this)
        {
            GM.SetButtonMgr(ButtonMgr);
            GM.SetPadGuide(PadGuide);
            GM.ActivePadGuide(GM.USERMGR.PadFlag);
            Destroy(gameObject);
        }

        GamePauseflg = true;
        /*if (!Selected[0].IsActive())
        {
            Selected[0] = new SelectData(null, 0, "Keyboard", InputSystem.devices[0], "Ch_roundFlask");
            Selected[1] = new SelectData(null, 1, "XInputControllerWindows", InputSystem.devices[2], "Ch_roundFlask");//디버깅용
        }*///Input for Debug -> 라운드매니저에서 받아야 할듯

        //GamePauseflg = true;
        //Tutorialchkflg = false;
        //MenuPanel = transform.GetChild(0).gameObject;

        /*RoomMgr = GenericFuncs.InitMgr<RoomManager>("RoomMgr").GetComponent<RoomManager>();
        RoomMgr.SetManager(Selected);*/

        /*for (int i = 0; i < ROUNDMGR.PMGR.PlayerCount; i++)
        {
            DontDestroyOnLoad(ROUNDMGR.PMGR.InputPlayers[i].gameObject);
        }*/
        //UIMgr = GenericFuncs.InitMgr<UIManager>("UIMgr").GetComponent<UIManager>();
        if (GameObject.Find("UserMgr") == null)
        {
            UserMgr = GenericFuncs.InitMgr<UserInputManager>("UserMgr").GetComponent<UserInputManager>();
            ResetGameMgr();
        }
    }
    void ResetGameMgr()
    {
        Debug.Log("ResetGM");
        
        //RoomMgr.ResetRoomMgr();
        UserMgr.ResetManager(Userobj);
    }
    public void ActivePadGuide(bool flg)
    {
        Debug.Log("PadTrue");
        
        if (PadGuide.activeSelf != flg)
        {
            PadGuide.SetActive(flg);
        }
    }
    public void ActivePadGuide(GameObject obj)
    {
        Debug.Log("PadTrue");
        if(PadGuide == null)
        {
            SetPadGuide(obj);
        }
        if (PadGuide.activeSelf != UserMgr.PadFlag)
        {
            PadGuide.SetActive(UserMgr.PadFlag);
        }
    }
    public void SetGuideText(string str, int fontsize)
    {
        PadGuide.GetComponentInChildren<UnityEngine.UI.Text>().text = str;
        PadGuide.GetComponentInChildren<UnityEngine.UI.Text>().fontSize = fontsize;
    }
    void SetPadGuide(GameObject obj)
    {
        PadGuide = obj;
    }
    void SetButtonMgr(onClickScript script)
    {
        ButtonMgr = script;
    }
    PhotonSetupScript PhotonMgr;
    EndingControl EndControl;
    public void SetDestroyManagers(PhotonSetupScript PhoMgr, EndingControl End)
    {
        PhotonMgr = PhoMgr;
        EndControl = End;
    }
    public void SetDestroyManagers(EndingControl End)
    {
        //PhotonMgr = PhoMgr;
        EndControl = End;
    }
    public void DestroyManagers()
    {
        if (PhotonMgr != null)
        {
            Destroy(PhotonMgr.gameObject);
        }
        if (EndControl != null)
        {
            Destroy(EndControl.gameObject);
        }
        if(SceneMgr != null)
        {
            Destroy(SceneMgr.gameObject);
        }
        GamePauseflg = true;
    }
    /*IEnumerator ResetValues(RoundManager RMGR)
    {
        while (RMGR.Gamestartflg)
        {
            if (RMGR.bCallResetGameMgr)
            {
                ResetGameMgr();
            }
            else yield return new WaitForEndOfFrame();
        }
    }*///RoundMgr의 bCallResetGameMgr변수를 받아서 원하는 타이밍에 변수를 초기화시키는 함수

    /*IEnumerator MenuOpen() {

        MenuPanel.SetActive(true);
        Time.timeScale = 0f;
        while (ROUNDMGR.PMGR.InputNextButton() && GamePauseflg)
        {
            yield return new WaitForEndOfFrame();
        }
        GamePauseflg = false;
        if (!GamePauseflg)
        {
            if (!Tutorialchkflg) Tutorialchkflg = true;
            Time.timeScale = 1f;
            MenuPanel.SetActive(false);
        }
        
        
    }
    */
}