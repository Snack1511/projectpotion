using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Custom.Func;
using Custom.Data;
public class SceneManager : MonoBehaviour
{
    /*
     * 씬 조작 및 설정
     */
    // Start is called before the first frame update
    /*
     * usable values >>
     * 1. NetworkMgr
     * 2. InputMgr -> Lobby 진입 후
     * 3. RoundMgr -> Play 진입 후

     */
    /*
     * Game->Room->Round->Player
     */
    //★ : UIMgr만들기 전 까지만 유지

    public SelectData[] Selected = new SelectData[4];
    
    RoundManager RoundMgr;
    int iPlayerIndex;
    bool bTutorial;
    bool bOnline;
    bool bPlay;
    bool bMulti;
    
    public bool Tutorialchkflg { 
        get { return bTutorial; }
        set { bTutorial = value; }
    }//튜토리얼 확인 플레그
    public bool Onlineflg
    {
        get { return bOnline; }
        //set { bOnline = value; }
    }//온라인 확인 플레그
    public bool PlaySceneflg
    {
        get { return bPlay; }
        //set { bPlay = value; }
    }
    public bool GamePauseflg
    {
        get { return RoundMgr.PAUSEMGR.GamePauseflg; }
    }

    public RoundManager ROUNDMGR
    {
        get { return RoundMgr; }
    }
    public PlayersManager PMGR
    {
        get { return RoundMgr.PMGR; }
    }
    public int PlayerIdx
    {
        get { return iPlayerIndex; }
    }
    public int UserCount
    {
        get
        {
            int i = 0;
            for (int n = 0; n < Selected.Length; n++)
            {
                if (Selected[n].IsActive())
                {
                    i++;
                }
            }
            return i;
        }
    }
    void InitRoomMgr()
    {
        GameManager.GM.SCENEMGR = this;
        //플레이씬으로 넘어갈때 동작
        //RoundMgr = GenericFuncs.InitMgr<RoundManager>("RoundMgr").GetComponent<RoundManager>();
        ResetSceneMgr();

    }
    public void ResetSceneMgr()
    {
        iPlayerIndex = -1;
        bOnline = false;
        bPlay = false;
        Tutorialchkflg = false;
        bMulti = false;
        Selected = new SelectData[4];
        
    }

    void Awake()
    {
        InitRoomMgr();
        StartCoroutine("ChangeScene");
    }
    void Start()
    {
        //RoundMgr.RoundStart();
    }
    IEnumerator ChangeScene()
    {
        while (!bPlay)
        {
            yield return new WaitForEndOfFrame();
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/Maps/TutorialScene");
        while (GameObject.Find("ScoreboardCanvas") == null)
        {
            yield return new WaitForEndOfFrame();
        }
        
        CreateRoundMgr(bMulti);
    }
    private void OnLevelWasLoaded(int level)
    {
        if(level == 2)
        {
            Debug.Log("tutorial");
            Debug.Log(GameManager.GM.SCENEMGR);
            //CreateRoundMgr();
        }
    }
    void CreateRoundMgr(bool Multiflag = false)
    {
        Debug.Log("CRM");
        if (!Selected[0].IsActive())
        {
            Debug.Log("DebugSelected");
            Selected[0] = new SelectData(null, 0, "Keyboard", InputSystem.devices[0], "Ch_roundFlask");
            Selected[1] = new SelectData(null, 1, "XInputControllerWindows", InputSystem.devices[2], "Ch_roundFlask");//디버깅용
        }
        RoundMgr = GenericFuncs.InitMgr<RoundManager>("RoundMgr").GetComponent<RoundManager>();
        RoundMgr.SetManager(Selected : Selected , flag : Multiflag/*ScoreboardPanel,*/);
    }

    public void SetPlay(bool flg = true)
    {
        bPlay = flg;
        Debug.Log("bPlay : "+bPlay);
    }
    public void SetLocal(bool flg = true)
    {
        Debug.Log(GameManager.GM.SCENEMGR);
        bMulti = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/1.LocalRobbyScene");
        //Debug.Log("Create NetWorkMgr");
    }
    public void SetOnline(bool flg = true)
    {
        Debug.Log("Create RoomMgr");
        bMulti = true;
        GameManager.GM.CreateRoomMgr();
    }
    public void SetHost(string RoomCode, string Gameversion)
    {
        GameManager.GM.ROOMMGR.InitRoom(true, RoomCode, Gameversion);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/2.MultiplaymodeScene");
    }
    public void SetGuest(string RoomCode, string Gameversion)
    {
        GameManager.GM.ROOMMGR.InitRoom(false, RoomCode, Gameversion);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/2.MultiplaymodeScene");
    }
    public void SetMain()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/0.MainmenuScene");
        ResetSceneMgr();
        GameManager.GM.DestroyManagers();
    }
    public void SetWinner()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/WinnerScene");

    }

    public void SetPlayerIdx(int n)
    {
        iPlayerIndex = n;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
}
