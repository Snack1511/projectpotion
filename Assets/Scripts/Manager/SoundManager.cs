using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    GameObject BGMContainer;
    [SerializeField] AudioClip MenuBGM;
    [SerializeField] AudioClip SFXPunch;
    [SerializeField] AudioClip SFXSword;
    [SerializeField] AudioClip SFXHit;
    [SerializeField] AudioClip SFXClick;
    [SerializeField] AudioMixer Mixer;
    [SerializeField] AudioSource audioSource;
    Stack<AudioSource> Stack_FXSource = new Stack<AudioSource>();
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        if (instance != this)
        {
            DestroyImmediate(gameObject);
        }
        DontDestroyOnLoad(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        //PlaySound("MenuBGM");
        //PlaySound("Punch");
        //PlaySound("Sword");
        //PlaySound("Hit");
        //PlaySound("Click");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayBGMSound(string str)
    {
        if (BGMContainer == null) {
            BGMContainer = new GameObject("BGMContainer");
        }
        Play(str, Instantiate(audioSource.gameObject, BGMContainer.transform).GetComponent<AudioSource>());
    }
    public void PlayFXSound(string str)
    {
        if(Stack_FXSource.Count <= 0 || (Stack_FXSource.Peek() != null && Stack_FXSource.Peek().isPlaying))
        {
            Stack_FXSource.Push(Instantiate<GameObject>(audioSource.gameObject, transform).GetComponent<AudioSource>());
        }
        if (!Stack_FXSource.Peek().gameObject.activeSelf)
        {
            Stack_FXSource.Peek().gameObject.SetActive(true);
        }
        Play(str, Stack_FXSource.Peek());
        StartCoroutine("PlayCheck", Stack_FXSource.Peek());
    }
    IEnumerator PlayCheck(AudioSource src)
    {
        while (src != null && src.isPlaying)
        {
            yield return new WaitForEndOfFrame();
        }
        if (src != null)
        {
            src.gameObject.SetActive(false);
            StartCoroutine("CheckStack");
        }
    }
    IEnumerator CheckStack()
    {
        yield return new WaitForSecondsRealtime(3);
        if(Stack_FXSource.Count > 0)
        {
            for(int i = 0; i < transform.childCount; i++) 
            {
                Destroy(transform.GetChild(0).gameObject);
            }
        }
        GameObject SrcObj = Stack_FXSource.Pop().gameObject;
        Destroy(SrcObj);

    }
    public void Play(string str, AudioSource SoundSource)
    {
        //var SoundObj = Instantiate<GameObject>(audioSource.gameObject, transform);
        SoundSource.clip = GetSound(str);
        SoundSource.outputAudioMixerGroup = GetMixerGroup(str);
        SoundSource.loop = SetLoop(str);
        SoundSource.Play();
    }
    
    void ActiveSource(AudioSource Src)
    {
        if (!Src.isPlaying)
        {
            Src.gameObject.SetActive(false);
        }
    }
    AudioClip GetSound(string str) => str switch
    {
        "MenuBGM" => MenuBGM,
        "Punch" => SFXPunch,
        "Sword" => SFXSword,
        "Hit" => SFXHit,
        "Click" => SFXClick,
        _ => null,

    };
    AudioMixerGroup GetMixerGroup(string str) => str switch
    {
        "MenuBGM" => Mixer.FindMatchingGroups("BGM/MenuBGM")[0],
        "Punch" => Mixer.FindMatchingGroups("SoundFX/Punch")[0],
        "Sword" => Mixer.FindMatchingGroups("SoundFX/Sword")[0],
        "Hit" => Mixer.FindMatchingGroups("SoundFX/Hit")[0],
        "Click" => Mixer.FindMatchingGroups("SoundFX/Click")[0],
        _ => null,

    };
    bool SetLoop(string str)
    {
        if (str == "MenuBGM")
        {
            return true;
        }
        else return false;
    }
}
