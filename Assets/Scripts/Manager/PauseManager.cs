using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Custom.Data;
public class PauseManager : MonoBehaviour//기능이 애매함 -> 일시정지 매니저가 적당할듯
{
    /*
     * UI창 관리
     */
    //Game -> Room -> Round -> UI
    public bool GamePauseflg;//중지플레그
    bool GuidInputCheck;
    GameObject MenuPanel;
    GameObject ScoreboardPanel;
    GameObject ContinueBtn;
    SceneManager SceneMgr;
    RoundManager RoundMgr;
    public RoundManager ROUNDMGR
    {
        get { return RoundMgr; }
    }

    public void InitUIMgr()
    {
        GamePauseflg = GameManager.GM.GamePauseflg;//★
        GuidInputCheck = false;
    }
    public void ResetUIMgr()
    {
        MenuPanel.SetActive(!SceneMgr.Tutorialchkflg);
        ScoreboardPanel.SetActive(false);
    }
    public void SetManager()
    {
        SceneMgr = GameObject.Find("SceneMgr").GetComponent<SceneManager>();
        RoundMgr = GameObject.Find("RoundMgr").GetComponent<RoundManager>();
        MenuPanel = GameObject.Find("MenuCanvas");//★
        ContinueBtn = MenuPanel.transform.GetChild(1).GetChild(1).GetChild(2).gameObject;
        ContinueBtn.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(new UnityEngine.Events.UnityAction(ClickContinueButton));
        Debug.Log("MenuPanel");
        MenuPanel.transform.SetParent(transform);//★

        ScoreboardPanel = GameObject.Find("ScoreboardCanvas");//★
        ScoreboardPanel.transform.SetParent(transform);//★
        ScoreboardPanel.transform.GetChild(1).GetComponent<ScoreboardScript>().InitScoreboard(this);
    }
    private void Awake()
    {
        InitUIMgr();
        //SetManager();
    }
    // Start is called before the first frame update
    void Start()
    {
        //RoomMgr = GameObject.Find("RoomMgr").GetComponent<RoomManager>();
    }

    // Update is called once per frame
    void Update()
    {
        MenuOpenLoop();
    }
    IEnumerator MenuOpen()
    {
        Debug.Log("MenuOpenEnter");
        MenuPanel.SetActive(true);
        RoundMgr.USERMGR.ActionmapChangeEvent.Invoke(ActionMaps.UI);//조작변경
        if (Time.timeScale == 1f)
        {
            Time.timeScale = 0f;
        }
        Debug.Log("GamePauseflg : "+ GamePauseflg);
        //int debugcnt = 0;
        while (/*RoomMgr.ROUNDMGR.PMGR.InputNextButton() &&*/ GamePauseflg && !RoundMgr.PMGR.ReadyAllPlayer())
        {
            Debug.Log("MenuOpenLoop");
            //SetGuidInputCheck(RoundMgr.USERMGR.InputNextButton());
            Debug.Log("GuidInputCheck : " + GuidInputCheck);
            yield return new WaitForEndOfFrame();
        }
        if (!GamePauseflg)
        {
            Debug.Log("What The Fuck");
        }
        GamePauseflg = false;
        Debug.Log("MenuOpenLoopOut");
        if (!GamePauseflg)
        {
            if (!SceneMgr.Tutorialchkflg) SceneMgr.Tutorialchkflg = true;//주의
            GameManager.GM.GamePauseflg = GamePauseflg;
            if (Time.timeScale == 0f)
            {
                Time.timeScale = 1f;
            }
            MenuPanel.SetActive(false);
            RoundMgr.USERMGR.ActionmapChangeEvent.Invoke(ActionMaps.GAMEPLAY);//조작변경
        }


    }//★

    public void MenuOpenLoop()
    {
        if (MenuPanel != null && ScoreboardPanel != null && GameManager.GM.GamePauseflg)
        {
            GamePauseflg = GameManager.GM.GamePauseflg;
            Debug.Log("MenuPanelFind");
            StartCoroutine("MenuOpen");
        }//★
        
    }
    public void ClickContinueButton()
    {
        //SetGuidInputCheck(true);
        RoundMgr.PMGR.PlayerDataList[0].CallFunc_SetParam_bIsReady(true);
        ChangeContinueText("WAITING OTHER PLAYERS..");

    }
    void ChangeContinueText(string str)
    {
        ContinueBtn.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = str;
    }
    public void SetGuidInputCheck(bool flag)
    {
        GuidInputCheck = flag;
    }
    public void SetPause(bool flg)
    {
        GamePauseflg = flg;
    }
    public void ScoreboardCheck(bool flg)
    {
        if (flg)
        {
            Debug.Log("ScoreboardCheck On");
            Debug.Log("SCORECHECK True");
            RoundMgr.SCORECHECK = true;
            /*if (ScoreboardPanel.transform.GetChild(1).GetComponent<ScoreboardScript>().bPlayEnd)
            {
                Debug.Log("SCORECHECK True");
                RoundMgr.SCORECHECK = true;
            }
            else
            {
                Debug.Log("Click");
                ScoreboardPanel.transform.GetChild(1).GetComponent<ScoreboardScript>().bPlayEnd = true;
            }*/
        }//버튼 입력시 스코어 보드 비활성화
    }
    public void SwitchScorePanel(bool flg)
    {
        if (!flg)
        {
            Debug.Log("ScoreOn");
            if (!ScoreboardPanel.activeSelf) ScoreboardPanel.SetActive(true);
        }
        else
        {
            Debug.Log("ScoreOff");
            if (ScoreboardPanel.activeSelf) ScoreboardPanel.SetActive(false);
        }

    }
    public bool IsScoreOn()
    {
        return ScoreboardPanel.activeSelf;
    }
}
