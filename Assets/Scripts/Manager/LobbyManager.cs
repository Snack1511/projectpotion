﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

using Custom.Data;


public class LobbyManager : MonoBehaviour
{
    PlayerInput P_A, P_B;//ForDebug
    Text txt;
    public SelectData[] SData;
    [SerializeField] AddPlayerScript PlayerSelectObjPrefab;
    public int PlayerIndex;
    public bool GameStartflg;//SelectorControl에서 변경
    public Material[] ColorMats = new Material[4];
    InputDevice InputDv;
    string DeviceName;
    UserInputManager UserInputMgr;
    private void Awake()
    {
        GameStartflg = false;
        SData = new SelectData[4];
        for (int i = 0; i < SData.Length; i++)
        {
            SData[i].ResetData();
        }
        UserInputMgr = GameManager.GM.USERMGR;
    }
    // Start is called before the first frame update
    void Start()
    {
       StartCoroutine("ReadSetDevice");
    }

    // Update is called once per frame
    

    public void SetSelectMode() {
        Debug.Log("SelectMode");
    }
    public void SortSD()
    {
        for (int i = 1; i < SData.Length; i++)
        {
            if (SData[i].IsActive())
            {
                if (!SData[i - 1].IsActive())
                {
                    SData[i].SetPreviousData(i - 1, ColorMats[i - 1]);
                    SData[i - 1] = SData[i];
                    SData[i].ResetData();
                    i -= 1;
                }
            }
        }
    }
    public bool ReadyAll()
    {
        int cnt = 0;
        for (int i = 0; i < PlayerIndex; i++)
        {
            if (SData[i].IsActive())
            {
                cnt++;
                if (!SData[i].IsReady())
                {
                    return false;
                }
            }

        }
        if (cnt >= 2)
        {
            return true;
        }
        else return false;
    }

    public void RemoveData(int n)
    {
        PlayerIndex -= 1;
        SData[n].ResetData();
        SortSD();
    }
    void ForDebuginTutorial()
    {
        P_A = PlayerInput.Instantiate(Resources.Load<GameObject>("Prefabs/PlayerObj"), 0, "Xbox");
        P_A.GetComponent<Transform>().position = new Vector3(2, 0, 0);
        P_B = PlayerInput.Instantiate(Resources.Load<GameObject>("Prefabs/PlayerObj"), 0, "PS4");
        P_B.GetComponent<Transform>().position = new Vector3(-2, 0, 0);
    }
    void DisplayBindingText()
    {
        txt.text = "Need to Binding Gamepad" + P_A.name;
    }
    void SetSelectDevice(InputDevice device = null)
    {
        InputDv = device;
        DeviceName = device?.name;
    }
    bool SearchData(SelectData[] a, string name)
    {
        for (int i = 0; i < a.Length; i++)
        {
            if (a[i].GetDVName() == name)
            {
                return true;
            }

        }
        return false;
    }


    IEnumerator ReadSetDevice()
    {
        PlayerIndex = 0;

        var AllGamePad = Gamepad.all;
        var keyboard = Keyboard.current;
        
        for (int i = 0; i < AllGamePad.Count && i < 4; i++)
        {

            SetSelectDevice(AllGamePad[i]);


            if (DeviceName != "")
            {
                if (!SearchData(SData, DeviceName))
                {
                    SData[PlayerIndex] = new SelectData(ColorMats[PlayerIndex], PlayerIndex, DeviceName, InputDv);
                    UserInputMgr.CreateUserInput(UserInputMgr.USEROBJ, UserInputMgr.transform, PlayerIndex, InputDv);
                    PlayerSelectObjPrefab.AddPlayer(SData[PlayerIndex], 420, PlayerIndex);
                    PlayerIndex++;
                }
                SetSelectDevice();
            }
            //Debug.Log(i);
        }
        if (PlayerIndex < 4)
        {
            SetSelectDevice(keyboard);

            if (DeviceName != "")
            {
                if (!SearchData(SData, DeviceName))
                {
                    SData[PlayerIndex] = new SelectData(ColorMats[PlayerIndex], PlayerIndex, DeviceName, InputDv);
                    UserInputMgr.CreateUserInput(UserInputMgr.USEROBJ, UserInputMgr.transform, PlayerIndex, InputDv);
                    PlayerSelectObjPrefab.AddPlayer(SData[PlayerIndex], 420, PlayerIndex);
                    PlayerIndex++;
                }
                SetSelectDevice();
            }
        }
        

        while (!GameStartflg)
        {
            if (AllGamePad.Count != PlayerIndex-1)
            {
                for (int i = 0; i < AllGamePad.Count && i < 4; i++)
                {
                    SetSelectDevice(AllGamePad[i]);

                    if (DeviceName != "")
                    {
                        if (!SearchData(SData, DeviceName))
                        {
                            SData[PlayerIndex] = new SelectData(ColorMats[PlayerIndex], PlayerIndex, DeviceName, InputDv);
                            PlayerSelectObjPrefab.AddPlayer(SData[PlayerIndex], 420, PlayerIndex);
                            PlayerIndex++;
                        }
                        SetSelectDevice();
                    }
                }
            }
            yield return null;
        }
        for (int i = 0; i < SData.Length; i++)
        {
            GameManager.GM.SCENEMGR.Selected[i].SetData(SData[i]);
        }
        GameManager.GM.SCENEMGR.SetPlay();
    }

    
}


/*
    PA Device입력
    현재 입력중인 Device확인
    DeviceInfo ControlA에 전송
    PB Device입력
    현재 입력중인 Device확인
    DeviceInfo ControlB에 전송
 */
