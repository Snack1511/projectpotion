using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Custom.Data;

/*
 * 플레이어 생성 및 읽기 전용
 */
public class PlayersManager : Photon.PunBehaviour
{
    [SerializeField] GameObject PlayerPrefab;
    [SerializeField] string PhotonPlayerPrefabName;
    public const float Dist = 10;//플레이어 스폰 위치정보
    public int Leaveplayer;//남은 플레이어 수
    public int PlayerCount = 0;//전체 플레이어 수
    public PlayerSet[] PlayerDatas;//플레이어들의 데이터 읽기용
    public List<PlayerSet> PlayerDataList = new List<PlayerSet>();
    // -> ControlMgr로 뺄 수 있을듯
    public GameObject Winner;//승자 저장용
    UserInputManager UserMgr;
    int TotalScore;
    int ClientIndex;
    //private PlayerInput[] InputPlayers;//플레이어들의 조작관련 읽기용 -> UserInput사용시 여기서 필요 읎음
    Vector3[] pos = {
        new Vector3(-Dist, 3, 0), new Vector3(Dist, 3, 0), new Vector3(0, 3, -Dist*0.75f), new Vector3(0, 3, Dist*0.75f)
    };//사전생성위치
    public UserInputManager USERMGR
    {
        get { return UserMgr; }
    }
    public (int,string) WinnerData
    {
        get { 
            if(Winner != null)
            {
                return (Winner.GetComponent<PlayerSet>().PlayerIndex, Winner.GetComponent<PlayerSet>().ModelName);
            }
            else
            {
                return (0, "");
            }
        }
    }
    //List<Player_Cal> PlayersCalculates = new List<Player_Cal>();
    /*public PlayerManager(SelectData[] Selected)
    {
        InputPlayers = new PlayerInput[4];
        PlayerDatas = new PlayerSet[4];
        for (int i = 0; i < Selected.Length; i++)
        {
            if (Selected[i].IsActive())
            {
                InputPlayers[i] = PlayerInput.Instantiate(Resources.Load<GameObject>("Prefabs/PlayerObj"), Selected[i].GetIndex(), GetScheme.GetSchemeDatas(Selected[i].GetDVName()), -1, Selected[i].GetInputDv());
                InputPlayers[i].GetComponent<PlayerSet>().ModelName = Selected[i].GetName();
                InputPlayers[i].GetComponent<PlayerSet>().ColorNum = Selected[i].GetIndex();
                PlayerDatas[i] = InputPlayers[i].GetComponent<PlayerSet>();
                //PlayersCalculates.Add(InputPlayers[i].GetComponent<Player_Cal>());

                PlayerCount++;

            }
        }
    }
    */
    void InitPlayerMgr()
    {
        PlayerPrefab = Resources.Load<GameObject>("Prefabs/PlayerObj");
        PhotonPlayerPrefabName = "Prefabs/PhotonPlayerObj";
        UserMgr = GameManager.GM.USERMGR;
        TotalScore = GameManager.GM.TotalScore;
        ClientIndex = GameManager.GM.SCENEMGR.PlayerIdx;
        //PlayerCount = ClientIndex;
        //InputPlayers = new PlayerInput[4];
        PlayerDatas = new PlayerSet[4];
        PlayerDataList = new List<PlayerSet>();
    }
    public void SetManager(SelectData[] Selected, bool MultiFlg = false)
    {
        for (int i = 0; i < Selected.Length; i++)
        {
            if (Selected[i].IsActive())
            {
                Debug.Log(i);

                if (ClientIndex != i)
                {
                    PlayerCount++;
                    continue;
                }

                //if(GameObject.Find("PhotonNetworkManager").GetComponent<PhotonSetupScript>()?.PlayerNumber != i)
                //{
                //    PlayerCount++;
                //    continue;
                //}

                //InputPlayers[i] = PlayerInput.Instantiate(Resources.Load<GameObject>("Prefabs/PlayerObj"), Selected[i].GetIndex(), GetScheme.GetSchemeDatas(Selected[i].GetDVName()), -1, Selected[i].GetInputDv());
                //InputPlayers[i].GetComponent<PlayerSet>().ModelName = Selected[i].GetName();
                //InputPlayers[i].GetComponent<PlayerSet>().ColorNum = Selected[i].GetIndex();
                if (MultiFlg)
                {
                    PlayerDatas[i] = PhotonNetwork.Instantiate(PhotonPlayerPrefabName, Vector3.zero, Quaternion.identity, 0).GetComponent<PlayerSet>();

                    //PhotonView PV = PlayerDatas[i].gameObject.AddComponent<PhotonView>();
                    //PhotonPlayerSet PVSet = PlayerDatas[i].gameObject.AddComponent<PhotonPlayerSet>();
                    //PV.viewID = GameObject.Find("PhotonNetworkManager").GetComponent<PhotonSetupScript>().ViewID;
                    //PV.viewID = PhotonNetwork.AllocateViewID();
                    //Debug.Log("ViewID : " + PV.viewID);
                    //PV.synchronization = ViewSynchronization.UnreliableOnChange;
                    //PV.ObservedComponents = new List<Component>();

                    //PhotonTransformView PTV = PV.gameObject.AddComponent<PhotonTransformView>();
                    //PTV.m_PositionModel.SynchronizeEnabled = true;
                    //PTV.m_RotationModel.SynchronizeEnabled = true;

                    //PV.ObservedComponents.Add(PTV);

                    //GameObject obj = PhotonNetwork.Instantiate(PhotonPlayerPrefabName, Vector3.zero, Quaternion.identity, 0);
                    //PlayerDatas[i] = obj.GetComponent<PlayerSet>();
                    //PhotonView PV = PlayerDatas[i].gameObject.AddComponent<PhotonView>();
                    //PhotonTransformView PTV = PV.gameObject.AddComponent<PhotonTransformView>();
                    //PTV.m_PositionModel.SynchronizeEnabled = true;
                    //PTV.m_RotationModel.SynchronizeEnabled = true;
                    //PV.synchronization = ViewSynchronization.UnreliableOnChange;
                    //PV.ObservedComponents = new List<Component>();
                    //PV.ObservedComponents.Add(PTV);
                    //PlayerDatas[i].SetParent(transform);
                    PlayerDatas[i].P_input = UserMgr.Users[/*Selected[i].GetIndex()*/0].GetComponent<PlayerInput>();
                    UserMgr.SetPlayerControl(/*Selected[i].GetIndex()*/0, PlayerDatas[i].GetComponent<PlayerControl>());
                    PlayerDatas[i].CallFunc_SetValues(Selected[i].GetName(), "Player " + Selected[i].GetIndex().ToString() + " GameObject", Selected[i].GetIndex(), Selected[i].GetIndex());
                    //PlayerDatas[i].CallFunc_SetParent();
                }
                else
                {
                    PlayerDatas[i] = Instantiate(PlayerPrefab, transform).GetComponent<PlayerSet>();
                    PlayerDatas[i].P_input = UserMgr.Users[/*Selected[i].GetIndex()*/i].GetComponent<PlayerInput>();
                    UserMgr.SetPlayerControl(/*Selected[i].GetIndex()*/i, PlayerDatas[i].GetComponent<PlayerControl>());
                    PlayerDatas[i].ModelName = Selected[i].GetName();
                    PlayerDatas[i].ObjectName = "Player "+ Selected[i].GetIndex().ToString() +" GameObject";
                    PlayerDatas[i].ColorNum = Selected[i].GetIndex();
                    PlayerDatas[i].SetIndex(Selected[i].GetIndex());

                }
                //if (MultiFlg)
                //{
                //    PhotonPlayerSet PVSet = PlayerDatas[i].gameObject.GetComponent<PhotonPlayerSet>();
                //    PVSet.SpawnPlayerEverywhere();

                //    break;
                //}
                //if (MultiFlg)
                //{
                //    PhotonView PV = PlayerDatas[i].gameObject.GetComponent<PhotonView>();
                //    PhotonPlayerSet PVSet = PlayerDatas[i].gameObject.AddComponent<PhotonPlayerSet>();

                //    PVSet.SpawnPlayerEverywhere(PV.viewID);
                //    break;
                //}
                //if (MultiFlg)
                //{
                //    GameObject obj = PlayerDatas[i].gameObject;
                //    Component[] views = (Component[])PlayerDatas[i].gameObject.GetPhotonViewsInChildren();
                //    int[] viewIDs = new int[views.Length];
                //    for (int j = 0; j < viewIDs.Length; j++)
                //    {
                //        //Debug.Log("Instantiate prefabName: " + prefabName + " player.ID: " + player.ID);
                //        viewIDs[j] = PhotonNetwork.AllocateViewID();
                //    }


                //    // Send to others, create info
                //    PhotonHashtable instantiateEvent = PhotonNetwork.networkingPeer.SendInstantiate(obj.gameObject.name.Trim("(Clone)".ToCharArray()), obj.transform.position, obj.transform.rotation, 0, viewIDs, null, false);

                //    // Instantiate the GO locally (but the same way as if it was done via event). This will also cache the instantiationId
                //    PhotonNetwork.networkingPeer.DoInstantiate(instantiateEvent, PhotonNetwork.networkingPeer.LocalPlayer, obj.gameObject);
                //    //PV.ObservedComponents.Add(PV.gameObject.transform);

                //}
                //PlayerDatas[i] = InputPlayers[i].GetComponent<PlayerSet>();
                //PlayerDatas[i].GetComponent<Transform>().SetParent(transform);
                //PlayersCalculates.Add(InputPlayers[i].GetComponent<Player_Cal>());

                PlayerCount++;

            }
        }
    }


    private void Awake()
    {
        InitPlayerMgr();
    }
    public void ResetPlayerMgr()
    {
        for (int i = 0; i < PlayerCount; i++)
        {
            //Debug.Log(PlayersCalculates[i]);
            /*if (PlayersCalculates[i] != null)
            {
                PlayersCalculates[i].gameObject.transform.position = pos[i];
                PlayersCalculates[i].GetComponent<PlayerSet>().ResetData();
                PlayersCalculates[i].gameObject.SetActive(true);
                
            }*/
            if (PlayerDatas[i] != null)
            {
                PlayerDatas[i].gameObject.transform.position = pos[PlayerDatas[i].PlayerIndex];
                //PlayerDatas[i].GetComponent<PlayerSet>().ResetData();
                PlayerDatas[i].GetComponent<PlayerSet>().CallFunc_ResetData();
                //PlayerDatas[i].gameObject.SetActive(true);
            }
        }
        Leaveplayer = 0;
    }//플레이어변수들 초기화
    /*
     if (PlayerDatas[i] == null)
            {
                for (int j = 0; j < PlayerDataList.Count; j++)
                {
                    if (PlayerDataList[j].PlayerIndex == i)
                    {
                        PlayerDatas[i] = PlayerDataList[j];
                        break;
                    }
                }
            }
     */
    public void GivePointToPlayer(int RoundNum)
    {
        for (int i = 0; i < PlayerCount; i++)
        {
            /*if (PlayersCalculates[i].gameObject.activeSelf)
            {
                PlayerDatas[i].GetPlayerData().POINT = 1;
                PlayerDatas[i].GetPlayerData().CalTotalPoint(RoundNum);
            }*/
            if (PlayerDatas[i] != null)
            {
                if (PlayerDatas[i].gameObject.activeSelf)
                {
                    PlayerDatas[i].GetPlayerData().POINT += 1;
                    //PlayerDatas[i].GetPlayerData().CalDegree(RoundNum);
                }
            }
        }
    }//플레이어변수에 점수할당

    public bool GetWinner()
    {
        for (int i = 0; i < PlayerCount; i++)
        {
            if (PlayerDataList[i] != null)
            {
                if (/*PlayerDatas[i] != null && */PlayerDataList[i].GetPlayerData().POINT >= TotalScore)
                {
                    Winner = PlayerDataList[i].gameObject;
                    return false;
                }
            }
        }
        return true;
    }//승자설정

    //public bool InputNextButton()
    //{
    //    if (InputPlayers[0].currentControlScheme == "Keyboard")
    //    {
    //        if (InputPlayers[0].actions["Attack"].phase == InputActionPhase.Waiting)
    //        {
    //            return true;
    //        }
    //    }
    //    else
    //    {
    //        if (InputPlayers[0].actions["Dash"].phase == InputActionPhase.Waiting)
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //}//플레이어확인버튼 입력 -> UserInput을 만들경우 여기서는 필요가 읎음

    public bool InputAnyButton()
    {
        if ((Gamepad.current.IsPressed() && Gamepad.current.wasUpdatedThisFrame)
            || (Keyboard.current.IsPressed() && Keyboard.current.wasUpdatedThisFrame))
        {
            return true;
        }
        else return false;
    }

    public bool IsLeaveBiggerThanPlayer()
    {
        
        if (Leaveplayer >= PlayerCount - 1) return true;
        else return false;
    }

    public PlayerSet[] ReadPlyerDatas()
    {
        return PlayerDatas;
    }
    public PlayerSet[] ReadPlyerDataList()
    {
        return PlayerDataList.ToArray();
    }
    public int SetLeavePlayer()
    {
        Leaveplayer++;
        return PlayerCount - (Leaveplayer - 1);
    }
    public void SetLeavePlayer_ReturnVoid()
    {
        
        Leaveplayer++;
        //return PlayerCount - (Leaveplayer - 1);
    }
    
    public void GetPointAlivePlayer(PlayerSet playerSet)
    {
        for (int i = 0; i < PlayerDataList.Count; i++)
        {
            if (PlayerDataList[i] != playerSet)
            {
                PlayerDataList[i].GetPlayerData().POINT += 1;
            }
        }
    }
    public void SetGamePause()
    {
        for (int i = 0; i < PlayerDatas.Length; i++) {
            if(PlayerDatas[i].CALLMENU != GameManager.GM.GamePauseflg)
            {
                PlayerDatas[i].CALLMENU = GameManager.GM.GamePauseflg;
            }
        }
    }
    public GameObject FindPlayer(GameObject obj)
    {
        for(int i = 0; i < PlayerDataList.Count; i++)
        {
            if (transform.GetChild(i) != null)
            {
                if (transform.GetChild(i).GetChild(1).GetChild(0).GetChild(0).gameObject == obj)
                {
                    return transform.GetChild(i).gameObject;
                }
                else
                {
                    Debug.Log("Can't Found " + transform.GetChild(i) .name+"."+ obj.name);
                }
            }

        }
        Debug.Log("Can't find gameObject");
        return null;
    }
    public void AddPlayerDatas(PlayerSet data)
    {
        PlayerDataList.Add(data);
        if(PlayerDatas[data.PlayerIndex] == null)
        {
            PlayerDatas[data.PlayerIndex] = data;
        }
    }
    public bool ReadyAllPlayer()
    {
        for(int i = 0; i < PlayerDataList.Count; i++)
        {
            if (!PlayerDataList[i].ReadyFlag)
            {
                return false;
            }
        }
        Debug.Log("playerListCount : " + PlayerDataList.Count);
        return true;
    }
}
