using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    string RoomCode;
    bool bHost;
    RoomOptions InitOption = new RoomOptions { MaxPlayers = 4};
    [SerializeField] string GameVersion;
    public (string, string, bool) EnterInfo
    {
        get { return (RoomCode, GameVersion, bHost); }
    }
    // Start is called before the first frame update
    void Start()
    {
        //PhotonNetwork.ConnectUsingSettings(GameVersion);
        /*if (bHost)
        {
            PhotonNetwork.CreateRoom(RoomCode, InitOption, null);
        }*/
    }

    /*public override void onConnect()
    {
        Debug.Log("Connect");
        PhotonNetwork.JoinOrCreateRoom("ROOM", new RoomOptions { MaxPlayers = 4, }, null);
    }*/

    // Update is called once per frame

    public void InitRoom(bool isHost, string roomCode, string version)
    {
        RoomCode = roomCode;
        bHost = isHost;
        GameVersion = version;
    }
}
