using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlurManager : MonoBehaviour
{
    public Camera blurCam;
    public Material BlurMat;

    void Start()
    {
        if(blurCam.targetTexture != null)
        {
            blurCam.targetTexture.Release();
        }
        blurCam.targetTexture = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32, 1);
        BlurMat.SetTexture("_RenTex", blurCam.targetTexture);
    }

}
