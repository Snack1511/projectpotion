using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Custom.Data;
/// <summary>
/// 유저 인풋 컨트롤러만 생성 삭제만 관리
/// </summary>
public class UserInputManager : MonoBehaviour
{
    
    public UnityEvent<ActionMaps> ActionmapChangeEvent = new UnityEvent<ActionMaps>();
    public List<GameObject> Users = new List<GameObject>();
    GameObject UserObj;
    UI_Set uiset;
    bool bIsPad;
    public GameObject USEROBJ
    {
        get { return UserObj; }
    }
    public onClickScript BTNMGR
    {
        get { return GameManager.GM.BTNMGR; }
    }
    public bool PadFlag
    {
        get { return bIsPad; }
    }
    public UI_Set UISet
    {
        get { return uiset; }
    }
    public void CreateUserInput(GameObject obj, Transform parent = null, int index = 0, InputDevice Device = null)
    {
        parent = transform;
        if (parent.childCount <= Users.Count && Users.Count < 4 )
        {
            if (Users.Count < 1)
            {
                //if (Device == null)
                //{
                //    if (Gamepad.all.Count < 1)
                //    {
                //        Device = Keyboard.current;
                //    }
                //    else
                //    {
                //        Device = Gamepad.all[0];
                //    }
                //}
                //var UserObj = PlayerInput.Instantiate(obj, index, GetScheme(Device.name), -1, Device);
                //Users.Add(UserObj.gameObject);
                //Users[Users.Count - 1].transform.SetParent(parent);
                CreateUser(obj, parent, index, Device);
            }
            else
            {
                if(Users.Count <= index)
                {
                    CreateUser(obj, parent, index, Device);
                }
            }
            //Instantiate(Users[Users.Count - 1] , parent);
        }
        else
        {
            foreach (GameObject a in Users)
            {
                if (!a.activeSelf)
                {
                    a.SetActive(true);
                    break;
                }
            }
        }

    }
    void CreateUser(GameObject obj, Transform parent, int index, InputDevice Device)
    {
        if (Device == null)
        {
            if (Gamepad.all.Count > 0)
            {
                Device = Gamepad.all[0];
                bIsPad = true;
                
            }
            else
            {
                
                Device = Keyboard.current;
                bIsPad = false;
            }
        }
        var UserObj = PlayerInput.Instantiate(obj, index, GetScheme(Device.name), -1, Device);
        Users.Add(UserObj.gameObject);
        Users[Users.Count - 1].transform.SetParent(parent);
    }
    public void ResetManager(GameObject obj)
    {
        if (!UserObj)
        {
            UserObj = obj;
        }
        RemoveUserInput();
        CreateUserInput(obj, transform);
        GameManager.GM.ActivePadGuide(bIsPad);

    }
    public void ChangeActionMap(ActionMaps Maps)
    {
        ActionmapChangeEvent.Invoke(Maps);
    }//액션맵 변화 타이밍에 호출 ex) 게임 진입시, 일시정지 호출시

    public void SetUIControl(int index, UIControl UIControlScriptUIControlScript)
    {
        Users[index].GetComponent<UserInput>().SetUIControl(UIControlScriptUIControlScript);
        uiset = UIControlScriptUIControlScript.GetComponent<UI_Set>();
    }
    public void SetPlayerControl(int index, PlayerControl PlayerControlScript)
    {
        Users[index].GetComponent<UserInput>().SetPlayerControl(PlayerControlScript);
    }
    void RemoveUserInput()
    {
        foreach(GameObject a in Users)
        {
            a.SetActive(false);
        }
    }

    public bool InputNextButton()
    {
        //if(Users[0].GetComponent<UserInput>)
        if (Users[0].GetComponent<PlayerInput>().currentControlScheme == "Keyboard")
        {
            if (Users[0].GetComponent<PlayerInput>().actions["Attack"].phase == InputActionPhase.Started 
                || Users[0].GetComponent<PlayerInput>().actions["Select"].phase == InputActionPhase.Started)
            {
                Debug.Log("InputCheck");
                return true;
            }
            else
            {
                Debug.Log("InputWaiting");
                return false;
            }
        }
        else
        {
            if (Users[0].GetComponent<PlayerInput>().actions["Dash"].phase == InputActionPhase.Started
                || Users[0].GetComponent<PlayerInput>().actions["Select"].phase == InputActionPhase.Started)
            {
                Debug.Log("InputCheck");
                return true;
            }
            else
            {
                Debug.Log("InputWaiting");
                return false;
            }
        }
        
    }
    public InputDevice GetUserInputDevice(int userIndex = 0) 
    {
        string devicename = Users[userIndex].GetComponent<PlayerInput>().currentControlScheme;
        return InputSystem.GetDevice(devicename);
    }
    public void CreateVirtualPad()
    {
#if UNITY_ANDROID
        Instantiate(GameManager.GM.VIRTUALPAD, Vector3.zero, Quaternion.identity);
        //var UICam = Instantiate(GameManager.GM.VIRTUALPAD, Vector3.zero, Quaternion.identity).GetComponent<Canvas>();
        //UICam.worldCamera = Camera.main.transform.GetChild(0).GetComponent<Camera>();
        //UICam.planeDistance = 1;

#endif
    }
    string GetScheme(string device)
    {
        if (device == "XInputControllerWindows")
        {
            return "Xbox";
        }
        else if (device == "Keyboard")
        {
            return "Keyboard";
        }
        else if (device == "DualShock4GamepadHID")
        {
            return "PS4";
        }
        else
        {
            return "";
        }
    }
}
