﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.InputSystem;


namespace Custom
{
    namespace Func
    {
        public class GetStringToInt {
        public static int GetModel(string str) => str switch
            {
                "Ch_roundFlask" => 0,
                "Ch_erlenmeyerFlask" => 1,
                "Ch_IsoscelesFlask" => 2,
                "Ch_pumpkinFlask" => 3,
                _ => -1,
            };
        }
        public class GetIntToString
        {
            public static string GetModel(int index) => index switch
            {
                0 => "Ch_roundFlask",
                1 => "Ch_erlenmeyerFlask",
                2 => "Ch_IsoscelesFlask",
                3 => "Ch_pumpkinFlask",
                _ => "None",
            };
        }
        public class GenericFuncs
        {
            public static GameObject InitMgr<T>(string _name) where T : class, new()
            {
                var a = new T();
                a.GetType();
                GameObject Mgrobj = GameObject.Find(_name);
                if (Mgrobj == null)
                {
                    Mgrobj = new GameObject();
                    Mgrobj.name = _name;
                    Mgrobj.AddComponent(a.GetType());
                    Object.DontDestroyOnLoad(Mgrobj);
                }
                return Mgrobj;
            }
            public static string NumGeneric()
            {
                int n;
                 n = Random.Range((int)0, (int)9999);
                return n.ToString("D4");
            }
        }
        public class GetScheme
    {
        public static string GetSchemeDatas(string device)
        {
            if (device == "XInputControllerWindows")
            {
                return "Xbox";
            }
            else if (device == "Keyboard")
            {
                return "Keyboard";
            }
            else if (device == "DualShock4GamepadHID")
            {
                return "PS4";
            }
            else
            {
                return "";
            }
        }
    }
        public class ColorFunction {
        public static Color[] SetColor(int _Colornum)
        {
            Color[] Cols = new Color[3];
            switch (_Colornum)
            {
                case 0:
                    Cols[0] = Hexadecimal("FC8370");//손
                    Cols[1] = Hexadecimal("FB6D51");//발
                    Cols[2] = Hexadecimal("E8563F");//Liquid
                    break;

                case 1:
                    Cols[0] = Hexadecimal("62DDBD");
                    Cols[1] = Hexadecimal("46CEAD");
                    Cols[2] = Hexadecimal("35BB9B");
                    break;

                case 2:
                    Cols[0] = Hexadecimal("FCD277");
                    Cols[1] = Hexadecimal("FECD57");
                    Cols[2] = Hexadecimal("F5BA45");
                    break;

                case 3:
                    Cols[0] = Hexadecimal("B3A5EF");
                    Cols[1] = Hexadecimal("AC92EA");
                    Cols[2] = Hexadecimal("967ADA");
                    break;
            }
            Debug.Log(Cols[0] + Cols[1] + Cols[2]);
            return Cols;

        }

        public static Color Hexadecimal(string Colorcode)
        {
            //string[] strsplit = ;

            byte br = System.Convert.ToByte(Colorcode.Substring(0, 2), 16);
            byte bg = System.Convert.ToByte(Colorcode.Substring(2, 2), 16);
            byte bb = System.Convert.ToByte(Colorcode.Substring(4, 2), 16);
            byte ba = 255;

            Color32 GetCol = new Color32(br, bg, bb, ba);

            return GetCol;
        }
        }
        public class DataReadFunction
        {
            public static Dictionary<string, Data.UIData.UIRectData> ReadObjectData(string path)
            {
                path = path + "/UserSetting/ObjectData.csv";
                
                //해상도별 요소의 크기
                //Dictionary<string, List<Data.UIData.UIObjectData>> Datas = new Dictionary<string, List<Data.UIData.UIObjectData>>();
                Dictionary<string, Data.UIData.UIRectData> Components = new Dictionary<string, Data.UIData.UIRectData>();
                //요소 집합
                //dic->dic->list
                if (File.Exists(path))
                {
                    StreamReader reader = new StreamReader(path);
                    string[] datas = reader.ReadToEnd().Split('\n');//문서 전체
                    Data.UIData.UIRectData ComponentData = new Data.UIData.UIRectData();
                    List<float> values = new List<float>();
                    string key = "";
                    for (int i = 1; i <  datas.Length; i++)
                    {
                        string[] datavalues = datas[i].Split(',');
                        
                        if (datavalues[0] == "Name")
                        {

                            Debug.Log(datavalues[1]);
                            key = datavalues[1].TrimEnd('\r');
                            continue;
                        }
                        else if (datavalues[0] == "EOD")
                        {
                            Components.Add(key, ComponentData);
                            ComponentData = new Data.UIData.UIRectData();
                        }
                        else if(datavalues[0] == "")
                        {
                            break;
                        }
                        else
                        {
                            string ComponentKey = datavalues[0];
                            for(int j = 1; j < datavalues.Length; j++)
                            {
                                ComponentData.SetValues(ComponentKey, float.Parse(datavalues[j]));
                            }
                            values.Clear();
                        }
                    }
                    reader.Close();
                }
                else
                {
                    Debug.LogError("File don't exist");
                }
                return Components;
            }
            public static List<Data.UIData.DisplayData> ReadResolution(string path)
            {
                path = path + "/UserSetting/Resolution.csv";
                
                List<Data.UIData.DisplayData> DataString = new List<Data.UIData.DisplayData>();
                if (File.Exists(path))
                {
                    //Debug.Log("FileExist");
                    StreamReader reader = new StreamReader(path);
                    var FirstLine = reader.ReadLine().Split(',');
                    string[] datas = reader.ReadToEnd().Split('\n');
                    foreach (string a in datas)
                    {
                        var OtherLine = a.Split(',');
                        if(OtherLine[0] != "EOD")
                        {
                            Dictionary<string, string> data = new Dictionary<string, string>();
                            for(int i = 1; i < OtherLine.Length; i++)
                            {
                                data.Add(FirstLine[i], OtherLine[i]);
                            }
                            Data.UIData.DisplayData ResolutionData = new Data.UIData.DisplayData();
                            ResolutionData.SetResolutionData(data);
                            DataString.Add(ResolutionData);
                        }
                        else
                        {
                            break;
                        }
                    }
                    reader.Close();
                }
                else
                {
                    Debug.LogError("File don't exist");
                }
                return DataString;
            }
            public static Dictionary<string, Data.UIData.UIDrawData> ReadUIDrawData(string path)
            {
                path = path + "/UserSetting/ObjectDraw.csv";

                Dictionary<string, Data.UIData.UIDrawData> DataString = new Dictionary<string, Data.UIData.UIDrawData>();
                if (File.Exists(path))
                {
                    //Debug.Log("FileExist");
                    StreamReader reader = new StreamReader(path);
                    var FirstLine = reader.ReadLine().Split(',');
                    string[] datas = reader.ReadToEnd().Split('\n');
                    foreach (string a in datas)
                    {
                        var OtherLine = a.Split(',');
                        if (OtherLine[0] != "EOD")
                        {
                            Dictionary<string, string> data = new Dictionary<string, string>();
                            
                            for (int i = 1; i < OtherLine.Length; i++)
                            {
                                data.Add(FirstLine[i], OtherLine[i]);
                            }
                            
                            Data.UIData.UIDrawData DrawData = new Data.UIData.UIDrawData();
                            DrawData.SetDrawData(data);
                            DataString.Add(OtherLine[0], DrawData);
                        }
                        else
                        {
                            break;
                        }
                    }
                    reader.Close();
                }
                else
                {
                    Debug.LogError("File don't exist");
                }
                return DataString;
            }
            //public static List<Dictionary<string, int>> ReadFile(string path, string FileName) {
            //    path = path + "/UserSetting/" + FileName + ".csv";
            //    List<Dictionary<string, int>> DataString = new List<Dictionary<string, int>>();
            //    if (File.Exists(path))
            //    {
            //        //Debug.Log("FileExist");
            //        StreamReader reader = new StreamReader(path);
            //        var FirstLine = reader.ReadLine().Split(',');

            //        foreach(string a in reader.ReadToEnd().Split('\n'))
            //        {
            //            Dictionary<string, int> DataValue = new Dictionary<string, int>();
            //            var OtherLine = a.Split(',');
            //            if (OtherLine[0] != "")
            //            {
            //                for (int i = 0; i < FirstLine.Length; i++)
            //                {
            //                    DataValue.Add(FirstLine[i], int.Parse(OtherLine[i]));
            //                }
            //                DataString.Add(DataValue);
            //            }
            //        }
            //        reader.Close();
            //    }
            //    else
            //    {
            //        Debug.LogError("File don't exist");
            //    }
            //    return DataString;
            //}
            
        }
    }

    namespace Data
    {
        public enum ActionMaps
        {
            UI,
            GAMEPLAY,
        }
        [System.Serializable]
        public class SelectData
        {

            [SerializeField] bool Readyflg;
            [SerializeField] bool Activeflg;
            [SerializeField] int Index;
            [SerializeField] string charactername;
            [SerializeField] string device;
            [SerializeField] Material Mat;
            [SerializeField] InputDevice InputDv;
            //delegate bool SetReadyflg(bool value);

            public SelectData(Material _Mat = null, int idx = -1, string _device = "", InputDevice _InputDv = null, string name = "")
            {
                Index = idx;
                Mat = _Mat;
                charactername = name;
                device = _device;
                InputDv = _InputDv;
                Readyflg = false;
                Activeflg = true;
            }

            public string GetDVName()
            {
                return device;
            }
            public string GetIndexTostr()
            {
                return Index.ToString();
            }
            public int GetIndex()
            {
                return Index;
            }
            public InputDevice GetInputDv()
            {
                return InputDv;
            }
            public string GetName()
            {
                return charactername;
            }
            public Material GetMat()
            {
                return Mat;
            }
            public bool IsReady()
            {
                return Readyflg;
            }
            public bool IsActive()
            {
                return Activeflg;
            }
            public void SetName(string value)
            {
                this.charactername = value;
            }
            public void SetReady(bool value = true)
            {
                this.Readyflg = value;
            }
            public void SetActive(bool value = true)
            {
                this.Activeflg = value;
            }
            public void ResetData()
            {
                Index = 0;
                Mat = null;
                charactername = "";
                device = "";
                InputDv = null;
                Readyflg = false;
                Activeflg = false;
            }

            public string PrintForDebug()
            {
                string str;
                str = Index + " " + device + " " + Mat + " " + charactername;
                return str;
            }

            public void SetData(SelectData sd)
            {
                Index = sd.Index;
                Mat = sd.Mat;//c;
                charactername = sd.charactername;
                device = sd.device;
                InputDv = sd.InputDv;
                Readyflg = sd.Readyflg;
                Activeflg = sd.Activeflg;
            }
            public void SetData(int PlayerNumber, int ModelNumber, bool ReadyFlag)
            {
                this.Index = PlayerNumber;
                SetName(Func.GetIntToString.GetModel(ModelNumber));
                SetReady(ReadyFlag);
            }

            public void SetPreviousData(int n, Material _Mat)
            {
                this.Index = n;
                this.Mat = _Mat;
            }

            
        };
        /*
             Keyboard
             XInputControllerWindows
             Dual4Shock
         */
        namespace UIData
        {
            
            [System.Serializable]
            public struct DisplayData {
                float RatioWidth;
                float RatioHeight;
                int DisplayWidth;
                int DisplayHeight;
                float RatioValue;

                public float RATIOVALUE
                {
                    get { return RatioValue; }
                }

                public void SetResolutionData(Dictionary<string, string> data)
                {
                    
                    RatioWidth = float.Parse(data["RatioWidth"]);
                    RatioHeight = float.Parse(data["RatioHeight"]);
                    DisplayWidth = int.Parse(data["DisplayWidth"]);
                    DisplayHeight = int.Parse(data["DisplayHeight"]);
                    RatioValue = float.Parse(data["RatioValue"]);
                }
                public string DebugValues()
                {
                    string str = "";
                    str += RatioWidth.ToString() + " ";
                    str += RatioHeight.ToString() + " ";
                    str += DisplayWidth.ToString() + " ";
                    str += DisplayHeight.ToString() + " ";
                    str += RatioValue.ToString();
                    return str;
                }

            }
            public class UIRectData
            {
                List<float> SizeX;
                List<float> SizeY;
                List<float> PositionX;
                List<float> PositionY;
                List<float> Space;
                
                public UIRectData()
                {
                    SizeX = new List<float>();
                    SizeY = new List<float>();
                    PositionX = new List<float>();
                    PositionY = new List<float>();
                    Space = new List<float>();
                }
                public List<float> WIDTH
                {
                    get { return SizeX; }
                }
                public List<float> HEIGHT
                {
                    get { return SizeY; }
                }
                public List<float> POSITIONX
                {
                    get { return PositionX; }
                }
                public List<float> POSITIONY
                {
                    get { return PositionY; }
                }
                public List<float> SPACE
                {
                    get { return Space; }
                }

                public void SetValues(string key, float values)
                {
                    switch (key)
                    {
                        case "SizeX":
                            SizeX.Add(values);
                            break;
                        case "SizeY":
                            SizeY.Add(values);
                            break;
                        case "PositionX":
                            PositionX.Add(values);
                            break;
                        case "PositionY":
                            PositionY.Add(values);
                            break;
                        case "Space":
                            Space.Add(values);
                            break;
                    }
                }

                public string DebugValues(int index)
                {
                    string str = "";
                    str += SizeX[index].ToString() + " ";
                    str += SizeY[index].ToString() + " ";
                    str += PositionX[index].ToString() + " ";
                    str += PositionY[index].ToString() + " ";
                    str += Space[index].ToString();
                    return str;
                }
                public Vector2 GetSize(int index)
                {
                    return new Vector2(SizeX[index], SizeY[index]);
                }
                public Vector2 GetPosition(int index)
                {
                    return new Vector2(PositionX[index], PositionY[index]);
                }
            }   
            public struct UIDrawData {
                string Image;
                string GroupingObject;
                int ChildCount;
                string ButtonLabel;
                public string IMAGE
                {
                    get { return Image; }
                }
                public string GROUPINGNAME
                {
                    get { return GroupingObject; }
                }
                public int CHILDCOUNT
                {
                    get { return ChildCount; }
                }
                public string LABEL
                {
                    get { return ButtonLabel; }
                }
                public void SetDrawData(Dictionary<string, string> data)
                {
                    
                    Image = data["Image"];
                    GroupingObject = data["GroupingObject"];
                    ChildCount = int.Parse(data["ChildCount"]);
                    ButtonLabel = data["ButtonLabel"];
                }
                public string DebugValues()
                {
                    string str = "";
                    str += Image+" ";
                    str += GroupingObject + " ";
                    str += ChildCount.ToString();
                    return str;
                }
            }
            struct ButtonData{
                int BaseButtonWidthRatio;
                int BaseButtonHeightRatio;
                int OneByOneMultiple;
                int BlankRatio;
            }

            
        }
        public interface ButtonScript
        {
            public UnityEngine.Events.UnityAction GetFunctions(string key);
        }
    }
    


}