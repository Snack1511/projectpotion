using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Custom.Data;
public class PhotonSetupScript : Photon.PunBehaviour
{
    [SerializeField] Vector3 HalfSize = new Vector3(210, 210, 0);
    [SerializeField] Vector3 HalfPosition = new Vector3(210, 0, 0);
    [SerializeField] GameObject ButtonObj;
    [SerializeField] GameObject PhotonCanvas;
    [SerializeField] GameObject SelectedModels;
    [SerializeField] GameObject RoomCodePanel;
    [SerializeField] GameObject PadGuide;
    [SerializeField] Camera TextureCam;
    [SerializeField] GameObject[] DummyModels = new GameObject[3];
    [SerializeField] RenderTexture[] PlayerRenderImage = new RenderTexture[4];
    [SerializeField] Color[] TextureColor;
    [SerializeField] Material CapMat;
    [SerializeField] Material BottleMat;
    [SerializeField] Text CountDownStr;
    [Header("- 디버그용 SerializeField")]
    [SerializeField] string RoomCode;
    [SerializeField] string Version;
    [SerializeField] bool Host;
    [SerializeField] int iPlayerNumber;
    [SerializeField] GameObject CharacterViewPanel;
    [SerializeField] RectTransform CharacterViewRect;
    [SerializeField] PhotonPlayer PhotonPlayerThisClient;
    [SerializeField] int PhotonViewIDTisClient;
    [SerializeField] List<SelectData> SData = new List<SelectData>();//하나만 있어도 됨 대신 공유할 데이터(ex : 플레이어 넘버, 플레이어모델넘버)의 경우 동기화 필요
    [SerializeField] List<PhotonImageScript> ImageScriptList = new List<PhotonImageScript>();
    [SerializeField] bool ChangeFlag = false;
    public RenderTexture[] RenderImage
    {
        get { return PlayerRenderImage; }
    }
    public Transform CharacterViewPanelTransform
    {
        get { return PhotonCanvas.transform; }
    }
    public Transform PhotonCanvasTransform
    {
        get { return PhotonCanvas.transform; }
    }
    public GameObject[] Dummy
    {
        get { return DummyModels; }
    }
    public int PlayerNumber
    {
        get { return iPlayerNumber; }
    }
    public int ViewID
    {
        get { return PhotonViewIDTisClient; }
    }
    //[SerializeField] List<GameObject> Player;
    Vector3 increase = new Vector3(400, 0, 0);
    RoomManager RoomMgr;

    private void Awake()
    {
        //if (CountDownStr.gameObject.activeSelf)
        //{
        //    CountDownStr.gameObject.SetActive(false);
        //}

        //DontDestroyOnLoad(gameObject);
        iPlayerNumber = 0;
        RoomMgr = GameObject.Find("RoomMgr").GetComponent<RoomManager>();
        GameManager.GM.ActivePadGuide(PadGuide);
        GameManager.GM.USERMGR.UISet.SetButtonScript(ButtonObj.GetComponent<MultiButtonScript>());
        GameManager.GM.USERMGR.UISet.SetButtonAction(KeyLeft: "Left", KeyRight: "Right", KeyA: "Ready", KeyB:"Back");
        //Player = new List<GameObject>();
        //GetComponent<PhotonView>().ObservedComponents = new List<Component>();

    }
    // Start is called before the first frame update
    void Start()
    {

        PhotonNetwork.automaticallySyncScene = true;
        ConnectRoom(RoomMgr.EnterInfo);
        StartCoroutine("ChangeRoutine");
    }
    public void ConnectRoom((string, string, bool ) _EnterInfo)
    {
        Debug.Log("RoomCode : " + _EnterInfo.Item1 + ", Version : " + _EnterInfo.Item2 + ", IsHost : " + _EnterInfo.Item3);
        RoomCode = _EnterInfo.Item1;
        Version = _EnterInfo.Item2;
        Host = _EnterInfo.Item3;
        /*if (Host)
        {
            PhotonNetwork.automaticallySyncScene = true;
        }*/
        PhotonNetwork.ConnectUsingSettings(Version);
        //GetComponent<PhotonView>().ObservedComponents.Add(Player.GetComponent<Image>());
        //PhotonNetwork.ConnectUsingSettings(GameManager.GM.Version);
    }
    bool ReadyAllPlayer()
    {
        if (ImageScriptList != null && ImageScriptList.Count > 0)
        {
            for (int i = 0; i < ImageScriptList.Count; i++)
            {
                if (!ImageScriptList[i].ReadyFlag)
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }
    IEnumerator ChangeRoutine()
    {
        Debug.Log("ChangeRoutine");
        while (Host)
        {
            if(ReadyAllPlayer() != ChangeFlag)
            {
                Debug.Log("ReadyAllPlayer is true");
                StartCoroutine("ChangePlayScene", 3);
                ChangeFlag = true;
            }
            yield return new WaitForEndOfFrame();
        }
    }
    public void ClickDebugSceneChangeButton()
    {
        //DebugChangeScene();
        if (Host)
        {
            StartCoroutine("ChangePlayScene", 3);
        }
        
    }
    IEnumerator ChangePlayScene(int MaxTime = 3)
    {
        int TimeInterval = 0;
        string Debugstr;
        while (TimeInterval < MaxTime)
        {
            if (!ReadyAllPlayer())
            {
                StopCoroutine("ChangePlayScene");
                ChangeFlag = false;
            }
            Debugstr = (MaxTime - TimeInterval).ToString();
            GetComponent<PhotonView>().RPC("SetCountDownStr", PhotonTargets.All, Debugstr);
            yield return new WaitForSeconds(1);
            TimeInterval += 1;
        }
        Debug.Log("StartGame");
        GetComponent<PhotonView>().RPC("DebugChangeScene", PhotonTargets.All);
        //DebugChangeScene();
    }
    [PunRPC]
    void DebugChangeScene()
    {

        Debug.Log("StartGameChangeScene");
        for (int i = 0; i < SData.Count; i++)
        {
            GameManager.GM.SCENEMGR.Selected[i].SetData(SData[i]);

        }//이부분은 굳이 없어도 될듯함
        GameManager.GM.SCENEMGR.SetPlayerIdx(PlayerNumber);
        GameManager.GM.SCENEMGR.SetPlay();
        
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log("ConnectToMaster");
        if (Host)
        {
            Debug.Log("Create Room");
            PhotonNetwork.CreateRoom(RoomCode, new RoomOptions { MaxPlayers = 4 }, null);
            //PhotonNetwork.JoinRoom(RoomCode);
        }
        else
        {
            Debug.Log("Join Room");
            PhotonNetwork.JoinRoom(RoomCode);

        }
        
    }
    public override void OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("JoinFailed");
        GameManager.GM.SCENEMGR.SetMain();
    }
    public override void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        Debug.Log("JoinFailed");
        GameManager.GM.SCENEMGR.SetMain();
    }
    public override void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        Debug.Log("OnMasterClientSwitched");
        Debug.Log(newMasterClient.NickName);
        base.OnMasterClientSwitched(newMasterClient);
    }
    
    public override void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        if (PhotonPlayerThisClient.ID < otherPlayer.ID)
        {
            CharacterViewRect.anchoredPosition3D += HalfPosition;
        }
        else
        {
            CharacterViewRect.anchoredPosition3D -= HalfPosition;
            iPlayerNumber -= 1;
            CharacterViewPanel.GetComponent<PhotonImageScript>().CallFunc_SetPlayerNumber(iPlayerNumber);
        }
    }
    //public override void OnDis
    public override void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");
        RoomCodePanel.transform.GetChild(0).GetComponent<Text>().text = "RoomCode : " + RoomCode;
        int i = 0;
        iPlayerNumber = PhotonNetwork.playerList.Length - 1;
        while (i < iPlayerNumber)
        {
            SData.Add(new SelectData(idx: i));
            SetDummyTexture(i);
            Debug.Log(PhotonCanvas.transform.childCount);
            ImageScriptList.Add(null);
            Debug.Log("ImageScriptList Count : "+ImageScriptList.Count);
            i++;
        }
        AddPlayer(iPlayerNumber);
        SetModelColor(iPlayerNumber, SelectedModels);
        CharacterViewPanel.GetComponent<PhotonImageScript>().CallFunc_SetObjParent();
        Debug.Log("Player Count : " + PhotonNetwork.playerList.Length);
        Debug.Log("OnJoinedRoom PhotonCanvasChild Count : " + PhotonCanvas.transform.childCount);
    }//서버 접속시 호출
    public override void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        Debug.Log("OnPhotonPlayerConnected PhotonCanvasChild Count : " + PhotonCanvas.transform.childCount);
        int n = PhotonNetwork.playerList.Length - 1;
        Debug.Log("PhotonNetwork.playerList.Length : " + PhotonNetwork.playerList.Length);
        Debug.Log("n : " + n);
        CharacterViewPanel.GetComponent<PhotonImageScript>().CallFunc_SetObjParent();
        SetDummyTexture(n);
        CharacterViewPanel.GetComponent<PhotonImageScript>().CallFunc_SetPlayerNumber(iPlayerNumber);
        Debug.Log("OnPhotonPlayerConnected");
        CharacterViewRect.anchoredPosition3D -= HalfPosition;
        SData.Add(new SelectData(idx: n));
        CharacterViewPanel.GetComponent<PhotonImageScript>().CallFunc_SyncSelected();



    }//현재 클라이언트가 접속 한 후 다른 클라이언트들 이 접속될때 호출
    public override void OnLeftRoom()
    {
        Debug.Log("LeftRoom");
        Destroy(RoomMgr.gameObject);
    }
    public override void OnDisconnectedFromPhoton()
    {
        Debug.Log("DisconnectPhoton");
        DestroyImmediate(gameObject, true);
    }

    
    [PunRPC]
    void SetCountDownStr(string str)
    {
        //if (!CountDownStr.gameObject.activeSelf)
        //{
        //    CountDownStr.gameObject.SetActive(true);
        //}
        CountDownStr.text = str;
    }

    void AddPlayer(int PlayerIndex)
    {
        SData.Add(new SelectData(idx: PlayerIndex, _InputDv: GameManager.GM.USERMGR.GetUserInputDevice()));
        CharacterViewPanel = PhotonNetwork.Instantiate("Prefabs/PlayerSelect", Vector3.zero, Quaternion.identity, 0);
        CharacterViewPanel.transform.SetParent(PhotonCanvas.transform);
        CharacterViewRect = CharacterViewPanel.GetComponent<RectTransform>();
        TextureCam.targetTexture = PlayerRenderImage[PlayerIndex];
        CharacterViewRect.localScale = Vector3.one;
        CharacterViewRect.sizeDelta = (HalfSize * 2);
        Debug.Log("Count of Players in Room" + PlayerIndex);
        CharacterViewRect.anchoredPosition3D = (HalfPosition * PlayerIndex);
        ButtonObj.GetComponent<MultiButtonScript>().SetConnectObj(CharacterViewPanel, PlayerRenderImage[PlayerIndex], this/*PhotonCanvas.transform.GetChild(index).gameObject*/);
        //ImageScriptList.Add(CharacterViewPanel.GetComponent<PhotonImageScript>());
        ImageScriptList.Add(CharacterViewPanel.GetComponent<PhotonImageScript>());


        CharacterViewPanel.GetComponent<PhotonImageScript>().CallFunc_SetPlayerNumber(PlayerIndex);
        PhotonViewIDTisClient = CharacterViewPanel.GetComponent<PhotonImageScript>().photonView.viewID;
        PhotonPlayerThisClient = CharacterViewPanel.GetComponent<PhotonImageScript>().photonView.owner;
        for (int i = 0; i < ImageScriptList.Count; i++)
        {
            if (ImageScriptList[i] != null)
            {
                Debug.Log(ImageScriptList[i].gameObject.name);
            }
            else
            {
                Debug.Log("ImageScriptList[" + i + "] is null");
            }
        }
        //var obj = PhotonNetwork.Instantiate("DebugTest/Panel", Vector3.zero, Quaternion.identity, 0);
        //obj.transform.SetParent(GameObject.Find("PhotonCanvas").GetComponent<RectTransform>());
        //obj.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
        //obj.GetComponent<RectTransform>().anchoredPosition3D += increase * (PhotonNetwork.countOfPlayers - 1);
        //Player.Add(PhotonNetwork.Instantiate("DebugTest/Panel", Vector3.zero, Quaternion.identity, 0));
        //Player[Player.Count - 1].transform.SetParent(Panel.transform);
        //Player[Player.Count - 1].GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
        //Player[Player.Count - 1].GetComponent<RectTransform>().anchoredPosition3D += increase * (Player.Count - 1);
        //GetComponent<PhotonView>().ObservedComponents.Add(Player[Player.Count - 1].GetComponent<Image>());
    }
    // Update is called once per frame

    void SetDummyTexture(int index)
    {
        Debug.Log("SetDummyTexture index is " + index);
        DummyModels[index].transform.GetChild(DummyModels[index].transform.childCount - 1).GetComponent<Camera>().targetTexture = PlayerRenderImage[index];
        InitTexture(index, DummyModels[index]);
        //여기서 마테리얼도 설정
    }
    void SetModelColor(int index, GameObject obj)
    {
        TextureColor = Custom.Func.ColorFunction.SetColor(index);
        InitTexture(index, obj);
    }
    void InitTexture(int n, GameObject DummyModels)
    {
        Material Mat_Liquid = new Material(Resources.Load<Material>("Materials/Liquid_Mat"));
        Material FootMat = new Material(Resources.Load<Material>("Materials/PlayerMat_Colored"));
        Material HandMat = new Material(Resources.Load<Material>("Materials/PlayerMat_Colored"));
        Color[] Colors = Custom.Func.ColorFunction.SetColor(n);
        Mat_Liquid.SetColor("_Tint", Colors[2]);
        FootMat.SetColor("_BaseColor", Colors[1]);
        FootMat.SetColor("_ColorDim", Colors[2]);
        HandMat.SetColor("_BaseColor", Colors[0]);
        HandMat.SetColor("_ColorDim", Colors[2]);
        for (int i = 0; i < DummyModels.transform.childCount-1; i++)
        {
            DummyModels.transform.GetChild(i).GetChild(0).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = BottleMat;
            DummyModels.transform.GetChild(i).GetChild(0).GetChild(0).GetChild(1).GetComponent<MeshRenderer>().material = CapMat;
            DummyModels.transform.GetChild(i).GetChild(0).GetChild(0).GetChild(2).GetComponent<MeshRenderer>().material = Mat_Liquid;
            DummyModels.transform.GetChild(i).GetChild(1).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = HandMat;
            DummyModels.transform.GetChild(i).GetChild(2).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = FootMat;
            DummyModels.transform.GetChild(i).GetChild(3).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = HandMat;
            DummyModels.transform.GetChild(i).GetChild(4).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material = FootMat;
        }
    }
    
    public void UpdateImageScript(int PlayerCount)
    {
        Debug.Log("UpdateImageScript PlayerCount" + PlayerCount);
        if (PlayerCount >= ImageScriptList.Count - 1)
        {
            Debug.Log("PlayerCount >= ImageScriptList.Count - 1 is True");
            ImageScriptList.Add(PhotonCanvas.transform.GetChild(PlayerCount).GetComponent<PhotonImageScript>());
        }
        else
        {
            Debug.Log("PlayerCount >= ImageScriptList.Count - 1 is false");
            //ImageScriptList.RemoveAt(PlayerCount);
            ImageScriptList[PlayerCount] = PhotonCanvas.transform.GetChild(PlayerCount+1).GetComponent<PhotonImageScript>();
        }
    }

    public void UpdateSelectData(int PlayerIndex, int ModelNumber, bool Readyflg)
    {
        //ImageScriptList[PlayerIndex].ModelNumber
        Debug.Log("Call UpdateSelectData");
        SData[PlayerIndex].SetName(Custom.Func.GetIntToString.GetModel(ModelNumber));
        SData[PlayerIndex].SetReady(Readyflg);
        Debug.Log("PlayerIndex : " + PlayerIndex + ", ModelNumber : " + ModelNumber);
        Debug.Log("Player "+PlayerIndex+"ModelName : " + SData[PlayerIndex].GetName());
    }
    
    public void BackToStart()
    {
        GameManager.GM.SCENEMGR.SetMain();
        PhotonNetwork.Disconnect();
        
    }
    
    void Update()
    {
        //Log.text = PhotonNetwork.countOfPlayers.ToString();
    }
}
