using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MultiButtonScript : MonoBehaviour, Custom.Data.ButtonScript
{
    PhotonView PV;
    PhotonImageScript imgScript;
    PhotonSetupScript SetupScript;
    GameObject ConnectedImg;
    [SerializeField] Transform PlayerModelsTransform;
    [SerializeField] Transform DummyModelsTransform;
    [SerializeField] Text ModelNumberText;
    int iModelindex;
    private void Awake()
    {
        iModelindex = 0;
        InitModel(iModelindex);
    }
    
    public void SetConnectObj(GameObject obj, RenderTexture Render, PhotonSetupScript _SetupScript)
    {
        SetupScript = _SetupScript;
        ConnectedImg = obj;
        imgScript = ConnectedImg.GetComponent<PhotonImageScript>();
        PV = imgScript.IMGPhotonView;
        ConnectedImg.transform.GetChild(2).GetComponent<RawImage>().texture = Render;
        //imgScript.CallFunc_SetPlayerNumber(index);//������...
        //imgScript.CallFunc_SetReadyFlg(false);
        //imgScript.CallFunc_SetSelectData();

    }
    public void ClickReadyBtn()
    {
        SoundManager.instance.PlayFXSound("Click");
        if (!imgScript.ReadyFlag)
        {
            if (PV.isMine)
            {
                Debug.Log("Click");
                Debug.Log("Click" + gameObject.name);
                //StartCoroutine("WaitTime");
                imgScript.CallFunc_SetReadyFlg(true);
                imgScript.CallFunc_SetSelectData();
                //PV.RPC("ButtonClick", PhotonTargets.All);
            }
        }
        else
        {
            imgScript.CallFunc_SetReadyFlg(false);

        }

    }
    public void ClickLeftBtn()
    {
        Debug.Log("LeftClick");
        SoundManager.instance.PlayFXSound("Click");
        if (!imgScript.ReadyFlag)
        {
            if (iModelindex > 0)
            {
                iModelindex--;
                if (PV.isMine)
                {
                    if (PlayerModelsTransform.GetChild(iModelindex + 1).gameObject.activeSelf)
                    {
                        PlayerModelsTransform.GetChild(iModelindex + 1).gameObject.SetActive(false);
                    }
                    PlayerModelsTransform.GetChild(iModelindex).gameObject.SetActive(true);
                    imgScript.CallFunc_SetOtherClientsDummyModelNum(iModelindex, iModelindex + 1);
                    imgScript.CallFunc_SetImageData(iModelindex);
                    imgScript.CallFunc_SetSelectData();
                }
                else
                {

                }
            }
            ModelNumberText.text = iModelindex.ToString();

        }

    }
    public void ClickRightBtn()
    {
        Debug.Log("RightClick");
        SoundManager.instance.PlayFXSound("Click");
        if (!imgScript.ReadyFlag)
        {
            if (iModelindex < PlayerModelsTransform.childCount - 2)
            {
                iModelindex++;
                if (PV.isMine)
                {
                    if (PlayerModelsTransform.GetChild(iModelindex - 1).gameObject.activeSelf)
                    {
                        PlayerModelsTransform.GetChild(iModelindex - 1).gameObject.SetActive(false);
                    }
                    PlayerModelsTransform.GetChild(iModelindex).gameObject.SetActive(true);
                    imgScript.CallFunc_SetOtherClientsDummyModelNum(iModelindex, iModelindex - 1);
                    imgScript.CallFunc_SetImageData(iModelindex);
                    imgScript.CallFunc_SetSelectData();
                }
                else
                {

                }
            }
            ModelNumberText.text = iModelindex.ToString();
        }
    }
    IEnumerator WaitTime()
    {
        Debug.Log("Start Coroutine");
        PV.RPC("ClickProcess", PhotonTargets.All, 0);
        gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "Click";
        yield return new WaitForSeconds(3);
        PV.RPC("ClickProcess", PhotonTargets.All, 1);
        gameObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "Button";


        Debug.Log("End Coroutine");
    }
    void InitModel(int idx)
    {
        int n = PlayerModelsTransform.childCount - 1;
        Debug.Log(n);
        for (int i = 0; i < n; i++)
        {
            if(i != idx)
            {
                PlayerModelsTransform.GetChild(i).gameObject.SetActive(false);
            }
        }
        
    }
    public UnityEngine.Events.UnityAction GetFunctions(string key)
    {
        UnityEngine.Events.UnityAction action = null;
        //Debug.Log(action.Method.Name);
        switch (key)
        {
            case "Left":
                action = ClickLeftBtn;
                break;
            case "Right":
                action = ClickRightBtn;
                break;
            case "Ready":
                action = ClickReadyBtn;
                break;
            case "Back":
                action = BacktoStart;
                break;
            default:
                action = ReplaceNull;
                break;
        }
        return action;
    }
    void ReplaceNull()
    {
        Debug.Log("Empty Action");
    }
    void BacktoStart()
    {
        SetupScript.BackToStart();
    }
}
