using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Custom.Data;

/*
 독립시켜도 될거 같음
 */
public class PhotonImageScript : Photon.PunBehaviour
{
    [SerializeField] RawImage TextureImage;
    [SerializeField] Text PlayerNumberText;
    [SerializeField] GameObject ReadyTextObj;
    GameObject DummyModels;
    PhotonSetupScript SetupScript;
    PhotonView PV;
    [Header("- 디버그용 SerializeField")]
    [SerializeField] int iPlayerNumber;
    [SerializeField] int iModelNumber;
    [SerializeField] bool bReadyflg;
    //int Index;
    public PhotonView IMGPhotonView
    {
        get { return PV; }
    }
    public RawImage Textureimg
    {
        get { return TextureImage; }
    }
    public int PlayerNumber
    {
        get { return iPlayerNumber; }
    }
    public int ModelNumber
    {
        get { return iModelNumber; }
    }
    public bool ReadyFlag
    {
        get { return bReadyflg; }
    }
    // Start is called before the first frame update

    private void Awake()
    {
        SetupScript = GameObject.Find("PhotonNetworkManager").GetComponent<PhotonSetupScript>();
        PV = GetComponent<PhotonView>();
        


    }
    void Start()
    {
        if (!PV.isMine)
        {
            CallFunc_SetObjParent();
            
            Debug.Log("iPlayerNumber : " + iPlayerNumber);
            SetupScript.UpdateImageScript(iPlayerNumber);
            //PV.TransferOwnership(PhotonNetwork.player);
            /*if (imgScript.Textureimg.texture == null)
            {
                Debug.Log("SetDummyTexture");
                imgScript.Textureimg.texture = Render;
            }*/
        }
    }

    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void CallFunc_SetPlayerNumber(int PlayerNumber)
    {
        PV.RPC("SetPlayerNUmber", PhotonTargets.All, PlayerNumber);
        PV.RPC("SetTargetImg", PhotonTargets.All, PlayerNumber);
    }
    [PunRPC]
    void SetPlayerNUmber(int PlayerNumber)
    {
        iPlayerNumber = PlayerNumber;
        PlayerNumberText.text = "P" + iPlayerNumber.ToString();
        name = PlayerNumberText.text;
        
    }
    [PunRPC]
    void SetTargetImg(int index)
    {
        //Index = index;
        if (TextureImage.texture == null)
        {
            TextureImage.texture = SetupScript.RenderImage[index];
            DummyModels = SetupScript.Dummy[index];
            InitModel();
        }
    }

    public void CallFunc_SetObjParent()
    {
        Debug.Log(gameObject.name + " CallFunc_SetObjParent IsMine : " + PV.isMine);
        PV.RPC("SetObjParent", PhotonTargets.All);
    }
    [PunRPC]
    void SetObjParent()
    {
        if (transform.parent == null)
        {
            Debug.Log(gameObject.name + " SetObjParent IsMine : " + PV.isMine);
            transform.SetParent(SetupScript.CharacterViewPanelTransform);
            GetComponent<RectTransform>().localScale = Vector3.one;
        }
    }
    public void CallFunc_SyncSelected()
    {
        object[] parameter = { iModelNumber, 0 };
        PV.RPC("SetOtherClientsDummyModelNum", PhotonTargets.All, parameter);
        PV.RPC("SetReadyFlg", PhotonTargets.All, bReadyflg);
        PV.RPC("SetReadyObj", PhotonTargets.All, bReadyflg);
        CallFunc_SetImageData(iModelNumber);
        CallFunc_SetSelectData();
        Debug.Log("SyncSelected " + "PlayerNum : " + iPlayerNumber + ", ModelNum : " + iModelNumber);
        //SetupScript.UpdateSelectData(iPlayerNumber, iModelNumber, bReadyflg);
    }
    public void CallFunc_SetOtherClientsDummyModelNum(int index, int PrevIndex)
    {
        object[] parameter = { index, PrevIndex };
        PV.RPC("SetOtherClientsDummyModelNum", PhotonTargets.All, parameter);
    }
    [PunRPC]
    void SetOtherClientsDummyModelNum(int index, int PrevIndex)
    {
        if (DummyModels != null)
        {
            DummyModels.transform.GetChild(PrevIndex).gameObject.SetActive(false);
            DummyModels.transform.GetChild(index).gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("Dummy is NULL");
        }
    }
    public void CallFunc_SetImageData(int ModelIndex)
    {
        PV.RPC("SetImageData", PhotonTargets.All, ModelIndex);
    }
    [PunRPC]
    void SetImageData(int ModelIndex)
    {
        Debug.Log("RPC SetImageData");
        Debug.Log("PlayerIndex : " + iPlayerNumber + ", ModelNumber : " + iModelNumber);
        iModelNumber = ModelIndex;
    }
    public void CallFunc_SetSelectData()
    {
        Debug.Log("CallFunc_SetSelectData");
        PV.RPC("SetSelectData", PhotonTargets.All);
    }

    [PunRPC]
    void SetSelectData()
    {
        Debug.Log("RPC SetSelectData");
        SetupScript.UpdateSelectData(iPlayerNumber, iModelNumber, bReadyflg);
    }
    

    public void CallFunc_SetReadyFlg(bool flag = false)
    {
        PV.RPC("SetReadyFlg", PhotonTargets.All, flag);
        PV.RPC("SetReadyObj", PhotonTargets.All, flag);
        //SetupScript.SetSelectData( iPlayerNumber, ModelNumber, flag);

    }
    [PunRPC]
    void SetReadyFlg(bool flag)
    {
        bReadyflg = flag;
        
    }
    
    [PunRPC]
    void SetReadyObj(bool flag)
    {
        ReadyTextObj.SetActive(flag);
    }

    [PunRPC]
    void ClickProcess(int index)
    {
        switch (index)
        {
            case 0:
                TextureImage.color = Color.cyan;
                break;
            case 1:
                TextureImage.color = Color.white;
                break;
        }

    }//MultiButtonScript에서 RPC로 호출

    
    void InitModel(int idx = 0)
    {
        Transform DummyTr = DummyModels.transform;
        int n = DummyTr.childCount - 1;
        Debug.Log(n);
        for (int i = 0; i < n; i++)
        {
            if (i != idx)
            {
                DummyTr.GetChild(i).gameObject.SetActive(false);
            }
        }
    }

}
