using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonSyncScript : Photon.PunBehaviour
{
    byte ClientReadyEvCode = 0;//[0, 199]
    byte AllReadyEvCode = 1;//[0, 199]
    int UserCount;
    int ClientNum;
    bool[] ClientReadyBoolarrOnMaster;
    RaiseEventOptions raiseEventOptionsMasterClient = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient };
    RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
    bool ClientReadyflg;
    //PhotonView PV;
    public bool ALLREADY
    {
        get { return ClientReadyflg; }
    }

    // Start is called before the first frame update
    public void Awake()
    {
        PhotonNetwork.OnEventCall += this.OnEvent;
    }
    public void OnEnable()
    {
        
    }
    void Start()
    {
        //PV = GetComponent<PhotonView>();

        UserCount = GameObject.Find("SceneMgr").GetComponent<SceneManager>().UserCount;
        ClientNum = GameObject.Find("SceneMgr").GetComponent<SceneManager>().PlayerIdx;
        ClientReadyBoolarrOnMaster = new bool[UserCount];
        for (int i = 0; i < UserCount; i++)
        {
            ClientReadyBoolarrOnMaster[i] = false;
        }
        Debug.Log("ClientReadyListOnMaster : " + ClientReadyBoolarrOnMaster.Length);

    }
    public void OnDisable()
    {

    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
    public void ResetSyncScript()
    {
        ClientReadyflg = false;
        for (int i = 0; i < UserCount; i++)
        {
            ClientReadyBoolarrOnMaster[i] = false;
        }
    }
    private void OnEvent(byte eventcode, object content, int senderid)
    {
        if (eventcode == ClientReadyEvCode)
        {
            Debug.Log("ClientReadyEvCode");
            //PhotonPlayer sender = PhotonPlayer.Find(senderid);  // who sent this?
            
            Debug.Log("SenderID : " + senderid);
            //bool[] tmp = (bool[])content;
            //Debug.Log("tmp[senderid - 1] : " + tmp[senderid - 1]);
            ClientReadyBoolarrOnMaster[senderid - 1] = (bool)content;//tmp[senderid - 1];
            if (ClientsAllReady())
            {
                PhotonNetwork.RaiseEvent(AllReadyEvCode, true, true, raiseEventOptions);
            }
            //ClientReadyListOnMaster[(int)content] = true;
        }
        if(eventcode == AllReadyEvCode)
        {
            ClientReadyflg = (bool)content;
        }
    }
    public bool ClientsAllReady()
    {
        for(int i = 0; i < ClientReadyBoolarrOnMaster.Length; i++)
        {
            //Debug.Log("ClientsAllReady.ClientReadyBoolarrOnMaster[" + i + "] : " + ClientReadyBoolarrOnMaster[i]);
            if (!ClientReadyBoolarrOnMaster[i])
            {
                return false;
            }
        }
        return true;
    }

    public void SetClientReadyflg(bool flag = false)
    {
        //Debug.Log("SyncScript.SetClientReadyflg");
        //Debug.Log("ClientNum : " + ClientNum);
        //Debug.Log("ClientReadyBoolarrOnMaster[" + ClientNum + "] : " + ClientReadyBoolarrOnMaster[ClientNum]);
        //ClientReadyBoolarrOnMaster[ClientNum] = flag;
        PhotonNetwork.RaiseEvent(ClientReadyEvCode, flag, true, raiseEventOptionsMasterClient);
    }

    public void DisconnectPhoton()
    {
        PhotonNetwork.Disconnect();
    }
}
