using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autozoom : MonoBehaviour
{
    [Range(0.1f, 1f)]
    [SerializeField]
    float ZoomDist;
    [Range(1f, 20f)]
    [SerializeField]
    float MaxDist;
    [Range(1f, 20f)]
    [SerializeField]
    float MinDist;
    float OrthoSize;
    Camera MainCam;
    Camera UICam;
    PlayersManager PMgr;
    Transform Tr;
    Vector3 BasePos;
    Vector3 CenterPos;
    // Start is called before the first frame update
    void Start()
    {
        MainCam = GetComponent<Camera>();
        //UICam = MainCam.GetComponentInChildren<Camera>();
        //MaxDist = MainCam.orthographicSize;
        Tr = GetComponent<Transform>();
        BasePos = Tr.position;
        //PMgr = GameManager.GM.PMGR;
    }

    // Update is called once per frame
    void Update()
    {
        ChangeDist();
        ChangePos(CenterPos, OrthoSize);
    }
    public void ChangeDist()
    {
        
        if (PMgr != null)
        {
            //Debug.Log("GetLongestDist : "+GetLongestDist(PMgr.PlayerDataList.ToArray()));
            MainCam.orthographicSize = GetLongestDist(PMgr.PlayerDataList.ToArray());
            OrthoSize = MainCam.orthographicSize;
            //UICam.orthographicSize = GetLongestDist(PMgr.PlayerDataList.ToArray());
        }
        else
        {
            PMgr = GameManager.GM.PMGR;
        }
    }
    public Vector3 GetCenter(PlayerSet[] Datas)
    {
        Vector3 center = new Vector3();
        int Cnt = 0;
        for (int n = 0; n < Datas.Length; n++)
        {
            if (Datas[n] != null)
            {
                center += Datas[n].transform.position;
                Cnt++;
            }

        }
        center /= Cnt;
        //Debug.Log("Center : " + center);
        //Debug.Log("CNT : " + Cnt);
        return center;
    }
    public float GetLongestDist(PlayerSet[] Datas)
    {
        float LongestDist = 0;
        if (Datas != null)
        {
            CenterPos = GetCenter(Datas);
            float dist = 0f;
            for (int n = 0; n < Datas.Length; n++)
            {
                if (Datas[n] != null)
                {
                    dist = Vector3.Distance(CenterPos, Datas[n].transform.position);
                    //Debug.Log("dist : " + dist);
                    if (LongestDist < dist)
                    {
                        LongestDist = dist;
                    }

                }
            }
            LongestDist *= ZoomDist;
            //Debug.Log("LongestDist : " + LongestDist);
            if (LongestDist < MinDist)
            {
                LongestDist = MinDist;
            }
            if (LongestDist > MaxDist)
            {
                LongestDist = MaxDist;
            }
        }
        return LongestDist;
    }
    public void ChangePos(Vector3 Center, float OrthoSize)
    {
        Center.y = 12;
        Center.z += -(11f + OrthoSize);
        Tr.position = Vector3.Lerp(Tr.position, Center, 3f);
        //Tr.position = BasePos + Center;
    }
}
