// GENERATED AUTOMATICALLY FROM 'Assets/InputActions/DebugControl.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @DebugControl : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @DebugControl()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""DebugControl"",
    ""maps"": [
        {
            ""name"": ""Select"",
            ""id"": ""0acd7b71-e7a7-46b3-b8ec-317de5fbad56"",
            ""actions"": [
                {
                    ""name"": ""SetSelectMode"",
                    ""type"": ""Button"",
                    ""id"": ""a9e845be-f9ba-4084-ad34-01e1317e425a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""7a4e941f-f167-4435-9ad4-f638cbe48994"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""SetSelectMode"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Select
        m_Select = asset.FindActionMap("Select", throwIfNotFound: true);
        m_Select_SetSelectMode = m_Select.FindAction("SetSelectMode", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Select
    private readonly InputActionMap m_Select;
    private ISelectActions m_SelectActionsCallbackInterface;
    private readonly InputAction m_Select_SetSelectMode;
    public struct SelectActions
    {
        private @DebugControl m_Wrapper;
        public SelectActions(@DebugControl wrapper) { m_Wrapper = wrapper; }
        public InputAction @SetSelectMode => m_Wrapper.m_Select_SetSelectMode;
        public InputActionMap Get() { return m_Wrapper.m_Select; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(SelectActions set) { return set.Get(); }
        public void SetCallbacks(ISelectActions instance)
        {
            if (m_Wrapper.m_SelectActionsCallbackInterface != null)
            {
                @SetSelectMode.started -= m_Wrapper.m_SelectActionsCallbackInterface.OnSetSelectMode;
                @SetSelectMode.performed -= m_Wrapper.m_SelectActionsCallbackInterface.OnSetSelectMode;
                @SetSelectMode.canceled -= m_Wrapper.m_SelectActionsCallbackInterface.OnSetSelectMode;
            }
            m_Wrapper.m_SelectActionsCallbackInterface = instance;
            if (instance != null)
            {
                @SetSelectMode.started += instance.OnSetSelectMode;
                @SetSelectMode.performed += instance.OnSetSelectMode;
                @SetSelectMode.canceled += instance.OnSetSelectMode;
            }
        }
    }
    public SelectActions @Select => new SelectActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    public interface ISelectActions
    {
        void OnSetSelectMode(InputAction.CallbackContext context);
    }
}
