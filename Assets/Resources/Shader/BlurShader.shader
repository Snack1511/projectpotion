Shader "Custom/Blur"
{
    Properties
    {
        [Header(Colors)] [Space]
        //_ColorMode("     Source{Colors}", Float) = 0.0
        _ColorShallow("[_COLORMODE_LINEAR]     Shallow", Color) = (0.35, 0.6, 0.75, 0.8) // Color alpha controls opacity
        //[NoScaleOffset] _ColorGradient("[_COLORMODE_GRADIENT_TEXTURE]     Gradient", 2D) = "white" {}
        _FadeDistance("     Shallow depth", Float) = 0.5
        _WaterDepth("     Gradient size", Float) = 5.0
        _LightContribution("     Light Color Contribution", Range(0, 1)) = 0

        [Space]
        _WaterClearness("     Transparency", Range(0, 1)) = 0.3
        _ShadowStrength("     Shadow strength", Range(0, 1)) = 0.35

        [Space]
        _MultipleCount("MultipleCount", Range(0, 1)) = 0.5

        /*[Header(Crest)][Space]
        _CrestColor("     Color{Crest}", Color) = (1.0, 1.0, 1.0, 0.9)
        _CrestSize("     Size{Crest}", Range(0, 1)) = 0.1
        _CrestSharpness("     Sharp transition{Crest}", Range(0, 1)) = 0.1*/

        /*[Space][Header(Wave geometry)][Space]
        [KeywordEnum(None, Round, Grid, Pointy)] _WaveMode("     Shape{Wave}", Float) = 1.0
        _WaveSpeed("[!_WAVEMODE_NONE]     Speed{Wave}", Float) = 0.5
        _WaveAmplitude("[!_WAVEMODE_NONE]     Amplitude{Wave}", Float) = 0.25
        _WaveFrequency("[!_WAVEMODE_NONE]     Frequency{Wave}", Float) = 1.0
        _WaveDirection("[!_WAVEMODE_NONE]     Direction{Wave}", Range(-1.0, 1.0)) = 0
        _WaveNoise("[!_WAVEMODE_NONE]     Noise{Wave}", Range(0, 1)) = 0.25*/

        /*[Space][Header(Foam)][Space]
        [KeywordEnum(None, Gradient Noise, Texture)] _FoamMode("     Source{Foam}", Float) = 1.0
        [NoScaleOffset] _NoiseMap("[_FOAMMODE_TEXTURE]           Texture{Foam}", 2D) = "white" {}
        _FoamColor("[!_FOAMMODE_NONE]     Color{Foam}", Color) = (1, 1, 1, 1)
        [Space]
        _FoamDepth("[!_FOAMMODE_NONE]     Shore Depth{Foam}", Float) = 0.5
        _FoamNoiseAmount("[!_FOAMMODE_NONE]     Shore Blending{Foam}", Range(0.0, 1.0)) = 1.0
        [Space]
        _FoamAmount("[!_FOAMMODE_NONE]     Amount{Foam}", Range(0, 3)) = 0.25
        [Space]
        _FoamScale("[!_FOAMMODE_NONE]     Scale{Foam}", Range(0, 3)) = 1
        _FoamStretchX("[!_FOAMMODE_NONE]     Stretch X{Foam}", Range(0, 10)) = 1
        _FoamStretchY("[!_FOAMMODE_NONE]     Stretch Y{Foam}", Range(0, 10)) = 1
        [Space]
        _FoamSharpness("[!_FOAMMODE_NONE]     Sharpness{Foam}", Range(0, 1)) = 0.5
        [Space]
        _FoamSpeed("[!_FOAMMODE_NONE]     Speed{Foam}", Float) = 0.1
        _FoamDirection("[!_FOAMMODE_NONE]     Direction{Foam}", Range(-1.0, 1.0)) = 0*/

        /*[Space][Header(Refraction)][Space]
        _RefractionFrequency("     Frequency", Float) = 35
        _RefractionAmplitude("     Amplitude", Range(0, 0.1)) = 0.01
        _RefractionSpeed("     Speed", Float) = 0.1
        _RefractionScale("     Scale", Float) = 1*/

            /*
            [Space][Header(Specular (Experimental))][Space]
            _FresnelAmount("     Amount", Range(0, 1)) = 0.0
            _FresnelSharpness("     Sharpness", Range(0, 1)) = 0.5
            _SpecularColor("     Color", Color) = (1, 1, 1, 0)
            _SunReflection("     Sun Reflection", Range(0, 1)) = 0.0
            */

            [Space][Header(Rendering options)][Space]
            [ToggleOff] _Opaque("     Opaque", Float) = 0.0
            [HideInInspector] _QueueOffset("Queue offset", Float) = 0.0
    }

        SubShader
        {
            Tags
            {
                "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"
            }
            LOD 200
            Blend SrcAlpha OneMinusSrcAlpha
            Lighting Off
            ZWrite[_ZWrite]
            
            Pass
            {
                HLSLPROGRAM
                #pragma prefer_hlslcc gles
                #pragma exclude_renderers d3d11_9x
                #pragma target 2.0

                #pragma shader_feature_local _COLORMODE_LINEAR //_COLORMODE_GRADIENT_TEXTURE
                //#pragma shader_feature_local _FOAMMODE_NONE _FOAMMODE_GRADIENT_NOISE _FOAMMODE_TEXTURE
                //#pragma shader_feature_local _WAVEMODE_NONE _WAVEMODE_ROUND _WAVEMODE_GRID _WAVEMODE_POINTY

                #pragma multi_compile_fog

                // -------------------------------------
                // Universal Render Pipeline keywords
                #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
                #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
                #pragma multi_compile _ _SHADOWS_SOFT

                #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareOpaqueTexture.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
                #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

                #pragma vertex vert
                #pragma fragment frag

                #if defined(_COLORMODE_GRADIENT_TEXTURE)
                TEXTURE2D(_ColorGradient);
                SAMPLER(sampler_ColorGradient);
                float4 _ColorGradient_ST;
                #endif

                #if defined(_COLORMODE_LINEAR)
                half4 _ColorShallow, _ColorDeep;
                #endif

                float _FadeDistance, _WaterDepth, _MultipleCount;

                half _LightContribution;

                half _WaveFrequency, _WaveAmplitude, _WaveSpeed, _WaveDirection, _WaveNoise;
                half _WaterClearness, _CrestSize, _CrestSharpness, _ShadowStrength;

                half4 _CrestColor;
                half4 _FoamColor;
                half _FoamDepth, _FoamAmount, _FoamScale, _FoamSharpness, _FoamStretchX, _FoamStretchY, _FoamSpeed,
                     _FoamDirection, _FoamNoiseAmount, _RefractionFrequency, _RefractionAmplitude, _RefractionSpeed,
                     _RefractionScale, _FresnelAmount, _FresnelSharpness, _SunReflection;

                half4 _SpecularColor;
                half _SpecularStrength;

                TEXTURE2D(_NoiseMap);
                SAMPLER(sampler_NoiseMap);
                float4 _NoiseMap_ST;

                struct VertexInput
                {
                    float4 positionOS : POSITION;
                    float2 texcoord : TEXCOORD0;
                    float3 normalOS : NORMAL;
                    float4 tangentOS : TANGENT;

                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct VertexOutput
                {
                    float4 positionCS : POSITION0;
                    float3 positionWS : POSITION1;
                    float2 uv : TEXCOORD0;
                    float4 screenPosition : TEXCOORD1;

                    float3 normal : TEXCOORD3; // World space.
                    float3 viewDir : TEXCOORD4; // World space.

                    //half fogFactor : TEXCOORD5;

                    UNITY_VERTEX_INPUT_INSTANCE_ID
                    UNITY_VERTEX_OUTPUT_STEREO
                };

                VertexOutput vert(VertexInput i)
                {
                    VertexOutput o = (VertexOutput)0;

                    UNITY_SETUP_INSTANCE_ID(i);
                    UNITY_TRANSFER_INSTANCE_ID(i, o);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                    // Vertex animation.
                    const float3 original_pos_ws = TransformObjectToWorld(i.positionOS.xyz);
                    //const float s = WaveHeight(i.texcoord, original_pos_ws);
                    const float s = 0;
                    o.positionWS = original_pos_ws * _MultipleCount;
                    o.positionWS.y += s * _WaveAmplitude;
                    o.positionCS = TransformWorldToHClip(o.positionWS);
                    const float4 screenPosition = ComputeScreenPos(o.positionCS);
                    o.screenPosition = screenPosition;
                    //o.waveHeight = s;

                    o.uv = i.texcoord;

                    {
                        // Normals.
                        const float3 viewDirWS = GetCameraPositionWS();// - o.positionWS;
                        o.viewDir = viewDirWS;

                        const VertexNormalInputs normalInput = GetVertexNormalInputs(i.normalOS, i.tangentOS);

                        const float sample_distance = 0.01;

                        float3 pos_tangent = original_pos_ws + normalInput.tangentWS * sample_distance;

                        float3 pos_bitangent = original_pos_ws + normalInput.bitangentWS * sample_distance;

                        const float3 modified_tangent = pos_tangent - o.positionWS;
                        const float3 modified_bitangent = pos_bitangent - o.positionWS;
                        const float3 modified_normal = cross(modified_tangent, modified_bitangent);

                        o.normal = normalize(modified_normal);
                    }

                    //half fogFactor = ComputeFogFactor(o.positionCS.z);
                    //o.fogFactor = fogFactor;

                    return o;
                }

                half4 frag(VertexOutput i) : SV_TARGET
                {
                    UNITY_SETUP_INSTANCE_ID(i);
                    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

                    
                    const float2 screen_uv = i.screenPosition.xy / i.screenPosition.w;
                    float2 displaced_uv = screen_uv;
                    const half3 scene_color = SampleSceneColor(displaced_uv);
                    half3 c = scene_color;

                    // Water depth.
                    half4 depth_color;
                    half4 color_shallow;
                    depth_color = _ColorShallow;
                    color_shallow = _ColorShallow;

                    c = lerp(depth_color.rgb, c, _WaterClearness * depth_color.a);

                    

                    return half4(c, 1);
                }
                ENDHLSL
            }

            Pass
            {
                HLSLPROGRAM
                #pragma prefer_hlslcc gles
                #pragma exclude_renderers d3d11_9x
                #pragma target 2.0

                #pragma shader_feature_local _COLORMODE_LINEAR //_COLORMODE_GRADIENT_TEXTURE
                //#pragma shader_feature_local _FOAMMODE_NONE _FOAMMODE_GRADIENT_NOISE _FOAMMODE_TEXTURE
                //#pragma shader_feature_local _WAVEMODE_NONE _WAVEMODE_ROUND _WAVEMODE_GRID _WAVEMODE_POINTY

                #pragma multi_compile_fog

                // -------------------------------------
                // Universal Render Pipeline keywords
                #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
                #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
                #pragma multi_compile _ _SHADOWS_SOFT

                #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareOpaqueTexture.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/DeclareDepthTexture.hlsl"
                #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
                #include "Packages/com.unity.shadergraph/ShaderGraphLibrary/ShaderVariablesFunctions.hlsl"

                #pragma vertex vert
                #pragma fragment frag

                #if defined(_COLORMODE_GRADIENT_TEXTURE)
                TEXTURE2D(_ColorGradient);
                SAMPLER(sampler_ColorGradient);
                float4 _ColorGradient_ST;
                #endif

                #if defined(_COLORMODE_LINEAR)
                half4 _ColorShallow, _ColorDeep;
                #endif

                float _FadeDistance, _WaterDepth, _MultipleCount;

                half _LightContribution;

                half _WaveFrequency, _WaveAmplitude, _WaveSpeed, _WaveDirection, _WaveNoise;
                half _WaterClearness, _CrestSize, _CrestSharpness, _ShadowStrength;

                half4 _CrestColor;
                half4 _FoamColor;
                half _FoamDepth, _FoamAmount, _FoamScale, _FoamSharpness, _FoamStretchX, _FoamStretchY, _FoamSpeed,
                     _FoamDirection, _FoamNoiseAmount, _RefractionFrequency, _RefractionAmplitude, _RefractionSpeed,
                     _RefractionScale, _FresnelAmount, _FresnelSharpness, _SunReflection;

                half4 _SpecularColor;
                half _SpecularStrength;

                TEXTURE2D(_NoiseMap);
                SAMPLER(sampler_NoiseMap);
                float4 _NoiseMap_ST;

                struct VertexInput
                {
                    float4 positionOS : POSITION;
                    float2 texcoord : TEXCOORD0;
                    float3 normalOS : NORMAL;
                    float4 tangentOS : TANGENT;

                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct VertexOutput
                {
                    float4 positionCS : POSITION0;
                    float3 positionWS : POSITION1;
                    float2 uv : TEXCOORD0;
                    float4 screenPosition : TEXCOORD1;

                    float3 normal : TEXCOORD3; // World space.
                    float3 viewDir : TEXCOORD4; // World space.

                    //half fogFactor : TEXCOORD5;

                    UNITY_VERTEX_INPUT_INSTANCE_ID
                    UNITY_VERTEX_OUTPUT_STEREO
                };

                VertexOutput vert(VertexInput i)
                {
                    VertexOutput o = (VertexOutput)0;

                    UNITY_SETUP_INSTANCE_ID(i);
                    UNITY_TRANSFER_INSTANCE_ID(i, o);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                    // Vertex animation.
                    const float3 original_pos_ws = TransformObjectToWorld(i.positionOS.xyz);
                    //const float s = WaveHeight(i.texcoord, original_pos_ws);
                    const float s = 0;
                    o.positionWS = original_pos_ws * -1 * _MultipleCount;
                    o.positionWS.y += s * _WaveAmplitude;
                    o.positionCS = TransformWorldToHClip(o.positionWS);
                    const float4 screenPosition = ComputeScreenPos(o.positionCS);
                    o.screenPosition = screenPosition;
                    //o.waveHeight = s;

                    o.uv = i.texcoord;

                    {
                        // Normals.
                        const float3 viewDirWS = GetCameraPositionWS();// - o.positionWS;
                        o.viewDir = viewDirWS;

                        const VertexNormalInputs normalInput = GetVertexNormalInputs(i.normalOS, i.tangentOS);

                        const float sample_distance = 0.01;

                        float3 pos_tangent = original_pos_ws + normalInput.tangentWS * sample_distance;

                        float3 pos_bitangent = original_pos_ws + normalInput.bitangentWS * sample_distance;

                        const float3 modified_tangent = pos_tangent - o.positionWS;
                        const float3 modified_bitangent = pos_bitangent - o.positionWS;
                        const float3 modified_normal = cross(modified_tangent, modified_bitangent);

                        o.normal = normalize(modified_normal);
                    }

                    //half fogFactor = ComputeFogFactor(o.positionCS.z);
                    //o.fogFactor = fogFactor;

                    return o;
                }

                half4 frag(VertexOutput i) : SV_TARGET
                {
                    UNITY_SETUP_INSTANCE_ID(i);
                    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);


                    const float2 screen_uv = i.screenPosition.xy / i.screenPosition.w;
                    float2 displaced_uv = screen_uv;
                    const half3 scene_color = SampleSceneColor(displaced_uv);
                    half3 c = scene_color;

                    // Water depth.
                    half4 depth_color;
                    half4 color_shallow;
                    depth_color = _ColorShallow;
                    color_shallow = _ColorShallow;

                    c = lerp(depth_color.rgb, c, _WaterClearness * depth_color.a);



                    return half4(c, 1);
                }
                ENDHLSL
            }
        }

            CustomEditor "FlatKitBlurEditor"
}
