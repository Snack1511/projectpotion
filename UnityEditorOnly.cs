using UnityEngine;
using UnityEditor.Animations;

public class UnityEditorOnly
{
    public static RuntimeAnimatorController GetAnimationController(string Address)
    {
        return Resources.Load<AnimatorController>(Address);
    }
}
